# This file is part of EGMWSORT - a tool for data sorting.
#
# Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

ifndef QUIET
QUIET=@
endif

all: bin/egmwsort bin/ebye_gen

########################################################################

# Cloning rules need to be early, such that directories exist when
# automatic rules search for files.

########################################################################
# Drasi git repo cloning.

DRASI_GIT_REPO = https://git.chalmers.se/expsubphys/drasi.git

ifneq (clean,$(MAKECMDGOALS))
ifneq (clean-all,$(MAKECMDGOALS))
DRASI_CLONE := $(shell test -d ../drasi/ || (cd .. ; \
	  git clone $(DRASI_GIT_REPO) --single-branch) )
endif
endif

########################################################################
# UCESB git repo cloning.

UCESB_GIT_REPO = https://git.chalmers.se/expsubphys/ucesb.git

ifneq (clean,$(MAKECMDGOALS))
ifneq (clean-all,$(MAKECMDGOALS))
UCESB_CLONE := $(shell test -d ../ucesb/ || (cd .. ; \
	  git clone $(UCESB_GIT_REPO) --single-branch) )
endif
endif

########################################################################

DTC_ARCH_DIR=../drasi/dtc_arch

########################################################################

CFLAGS += -g -Wall -O3 -I . \
	-I $(DTC_ARCH_DIR) \
	-I ../drasi/lwroc

include $(DTC_ARCH_DIR)/build_rules_inc.mk

CFLAGS += -I ${GENDIR_ARCH}

CXXFLAGS += -g -Wall -O3 -I .

CXXFLAGS += -I ../ucesb/hbook -I ../ucesb/lu_common \
	-I ../ucesb/eventloop -I ../ucesb/common \
	-I ../ucesb/mapcalib -I ../ucesb/threading \
	-I ../ucesb/file_input

LD = $(CXX)

########################################################################

include $(DTC_ARCH_DIR)/acc_auto_def.mk

########################################################################

OBJS = egmwsort.o message.o config.o ebye_record.o ebye_record_stats.o \
	ldf_record.o \
	item.o item_ebye_read.o item_ldf_read.o item_print.o item_process.o \
	item_rteb.o item_hb.o item_ext_data.o \
	input_file.o io_util.o \
	accum_sort.o inc_colourtext.o parse_util.o \
	ebye_record_out.o ldf_record_out.o hit_out.o pulser.o \
	ext_data.o ext_data_srcs.o

OBJS_EBYE_GEN = ebye_gen.o message.o inc_colourtext.o \
	ebye_record_out.o ldf_record_out.o hit_out.o io_util.o

OBJS_ACCUM = accum_sort.o accum_test.o

OBJS_ARCH = $(OBJS:%.o=obj/%.o)

OBJS_EBYE_GEN_ARCH = $(OBJS_EBYE_GEN:%.o=obj/%.o)

OBJS_ACCUM_ARCH = $(OBJS_ACCUM:%.o=obj/%.o)

########################################################################

AUTO_DEPS = $(OBJS_ARCH:%.o=%.d) \
	$(OBJS_EBYE_GEN_ARCH:%.o=%.d) \
	$(OBJS_ACCUM_ARCH:%.o=%.d)

FIXUP_DEPS = sed -e 's,\($(*F)\)\.o[ :]*,obj/$*.o $@ : ,g' \
        -e 's,[^/]acc_auto_def/, gen_$(ARCH_SUFFIX)/acc_auto_def/,g'

ifneq (clean,$(MAKECMDGOALS))
ifneq (clean-all,$(MAKECMDGOALS))
-include $(AUTO_DEPS) # dependency files (.d)
endif
endif

#######################################################################

# Force create the git-describe file by making it from a shell script
GIT_DESCRIBE_FORCE=$(shell ../drasi/lwroc/force_git_describe.sh \
	$(GENDIR_ARCH) $(GENDIR))
ifneq (,$(findstring FAIL,$(GIT_DESCRIBE_FORCE)))
$(error Git describe rule failed - cannot continue!)
endif

########################################################################

bin/egmwsort: $(OBJS_ARCH)
	@echo "   LD    $@"
	@mkdir -p $(dir $@)
	$(QUIET)$(LD) $+ -o $@ $(LDFLAGS) $(LIBS)

obj/%.o: src/%.c
	@echo "   CC    $@"
	@mkdir -p $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -c $< -o $@

obj/%.o: src/%.cc
	@echo "   CXX   $@"
	@mkdir -p $(dir $@)
	$(QUIET)$(CXX) $(CXXFLAGS) -c $< -o $@

obj/%.d: src/%.c
	@echo "  DEPS   $@"
	@mkdir -p $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -MM -MG $< | $(FIXUP_DEPS) > $@

obj/%.d: src/%.cc
	@echo "  DEPS   $@"
	@mkdir -p $(dir $@)
	$(QUIET)$(CXX) $(CXXFLAGS) -MM -MG $< | $(FIXUP_DEPS) > $@

########################################################################

.PHONY: drasi_ucesb_upgrade
drasi_ucesb_upgrade:
	cd ../drasi ; git fetch origin ; git merge --ff-only origin/master
	cd ../ucesb ; git fetch origin ; git merge --ff-only origin/master

########################################################################

EXTWRITE = ../ucesb/hbook/make_external_struct_sender.pl

########################################################################

UCESB_BASE_DIR=../ucesb

-include ../ucesb/makefile_ext_file_writer.inc

########################################################################

gen/ext_data_writer.hh: src/ext_data.h $(EXTWRITE) $(EXT_WRITERS)
	@echo "EXTWRSTR $@"
	@mkdir -p $(dir $@)
	$(QUIET)perl $(EXTWRITE) --struct=ext_data < $< > $@

obj/ext_data.o: gen/ext_data_writer.hh src/ext_data.cc

########################################################################

bin/ebye_gen: $(OBJS_EBYE_GEN_ARCH)
	@echo "   LD    $@"
	@mkdir -p $(dir $@)
	$(QUIET)$(LD) $+ -o $@ $(LDFLAGS) $(LIBS)

########################################################################

bin/accum_test: $(OBJS_ACCUM_ARCH)
	@echo "   LD    $@"
	@mkdir -p $(dir $@)
	$(QUIET)$(LD) $+ -o $@ $(LDFLAGS) $(LIBS)

########################################################################

accum_test.stamp: bin/accum_test
	@echo "  TEST   $@"
	$(QUIET)./$< || ./$< --debug
	$(QUIET)touch $@

test_sh.stamp: test/test.sh bin/egmwsort bin/ebye_gen
	@echo "  TEST   $@"
	$(QUIET)./$< > $@.out 2> $@.err || \
          ( echo "Failure while running: './$<':" ; \
            echo "--- stdout: ---" ; cat $@.out ; \
            echo "--- stderr: ---"; cat $@.err ; \
            echo "---------------" ; false)
	$(QUIET)touch $@

########################################################################

.PHONY: test
test: accum_test.stamp test_sh.stamp

.PHONY: filter
filter:
	$(MAKE) -C filter

.PHONY: root2ebye
root2ebye:
	$(MAKE) -C root2ebye

########################################################################

clean:
	rm -f *.o *.d
	rm -rf gen/ obj/ bin/
	rm -rf ${GENDIR_ARCH}
	rm -f egmwsort accum_test
	rm -f accum_test.stamp test_sh.stamp

########################################################################
