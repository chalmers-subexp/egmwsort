/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>

#include "accum_sort.h"

/* To satisfy debugging code of bad data...  TODO: get rid of this. */
#include "ebye_record.h"
fullbug2023_state _bug2023_info;
int _record_had_backward;
/* End. */

/* Debug printing? */

int _debug = 0;

/* The item to enter into the test array. */

typedef struct accum_item_test_t
{
  lwroc_accum_item_base _base;
  uint32_t              _value;
} accum_item_test;

/* The accumulation buffers. */

#define NUM_ACCUM_IDS  8

lwroc_accum_buffer *_bufs = NULL;

lwroc_accum_many *_mbufs = NULL;

/* Shadow buffers holding state. */

#define NUM_SHADOW_ITEMS  256

typedef struct accum_test_shadow_item_t
{
  uint64_t _ts;
  uint32_t _value;
  uint32_t _insert_count;   /* Just insertation count
			     * (for stable sorting check).
			     */
} accum_test_shadow_item;

typedef struct accum_test_shadow_t {

  accum_test_shadow_item _items[NUM_SHADOW_ITEMS];

  size_t _n;

} accum_test_shadow;

accum_test_shadow _shadow[NUM_ACCUM_IDS];

/* Callback functions. */

uint64_t _last_process_ts;
uint32_t _last_process_insert_count;

void process_item(uint32_t type, uint32_t id,
		  lwroc_accum_item_base *item, void *arg)
{
  accum_test_shadow *s = &_shadow[id];
  accum_item_test *full = (accum_item_test *) item;

  (void) type;
  (void) arg;

  if (_debug)
    fprintf (stderr,
	     "Process ID %d "
	     "%04" PRIx64 "\n",
	     id,
	     item->_ts);

  if (s->_n == 0)
    {
      fprintf (stderr, "Process ID %d: "
	       "has no items in shadow.\n", id);
      exit (1);
    }

  if (item->_ts != s->_items[0]._ts)
    {
      fprintf (stderr, "Process ID %d: "
	       "unexpected ts %04" PRIx64 " != %04" PRIx64 ".\n", id,
	       item->_ts, s->_items[0]._ts);
      exit (1);
    }

  if (item->_insert_count != s->_items[0]._insert_count)
    {
      fprintf (stderr, "Process ID %d: "
	       "unexpected insert count %d != %d.\n", id,
	       item->_insert_count, s->_items[0]._insert_count);
      exit (1);
    }

  if (full->_value != s->_items[0]._value)
    {
      fprintf (stderr, "Process ID %d: "
	       "unexpected value %d != %d.\n", id,
	       full->_value, s->_items[0]._value);
      exit (1);
    }

  if (item->_ts < _last_process_ts)
    {
      fprintf (stderr, "Shadows ID %d: "
	       "out-of-order ts %04" PRIx64 " < last %04" PRIx64 ".\n",
	       id, item->_ts, _last_process_ts);
      exit (1);
    }

  if (item->_ts == _last_process_ts &&
      item->_insert_count < _last_process_insert_count)
    {
      fprintf (stderr, "Shadows ID %d: "
	       "out-of-order insert count %d < last %d.\n",
	       id, item->_insert_count, _last_process_insert_count);
      exit (1);
    }

  _last_process_ts           = item->_ts;
  _last_process_insert_count = item->_insert_count;

  /* Remove this shadow item. */
  s->_n--;
  memmove(&s->_items[0], &s->_items[1],
	  s->_n * sizeof(s->_items[0]));
}

void report_ins_backwards(uint32_t type, uint32_t id,
			  lwroc_accum_item_base *item,
			  uint64_t last_ts)
{
  if (_debug)
    fprintf (stderr,
	     "Insert timestamp backwards for ID %d:%d "
	     "(%04" PRIx64 " - %04" PRIx64 " = %" PRId64 ").\n",
	     type, id,
	     item->_ts, last_ts,
	     (int64_t) (item->_ts - last_ts));
}

void report_pick_backwards(uint32_t type, uint32_t id,
			   lwroc_accum_item_base *item,
			   uint64_t last_ts)
{
  if (_debug)
    fprintf (stderr,
	     "Picked timestamp backwards for ID %d:%d "
	     "(%04" PRIx64 " - %04" PRIx64 " = %" PRId64 ").\n",
	     type, id,
	     item->_ts, last_ts,
	     (int64_t) (item->_ts - last_ts));
}

void report_resorted(uint32_t type, uint32_t id, size_t n)
{
  if (_debug)
    fprintf (stderr, "Resorted for ID %d:%d, %zd items.\n",
	     type, id, n);
}

uint32_t _insert_count = 1;

void insert_shadow(int id, uint64_t ts, uint32_t value)
{
  accum_test_shadow *s = &_shadow[id];

  if (s->_n >= NUM_SHADOW_ITEMS)
    {
      fprintf (stderr, "Test shadow buffer overflow.\n");
      exit(1);
    }

  accum_test_shadow_item *item = &s->_items[s->_n++];

  item->_ts = ts;
  item->_value = value;
  item->_insert_count = _insert_count++;
}

int compare_accum_test_shadow_item(const void *p1, const void *p2)
{
  accum_test_shadow_item *a = (accum_test_shadow_item *) p1;
  accum_test_shadow_item *b = (accum_test_shadow_item *) p2;

  if (a->_ts < b->_ts)
    return -1;
  if (a->_ts > b->_ts)
    return 1;

  if (a->_insert_count < b->_insert_count)
    return -1;
  if (a->_insert_count > b->_insert_count)
    return 1;

  return 0;
}

void sort_shadow()
{
  for (int id = 0; id < NUM_ACCUM_IDS; id++)
    {
      accum_test_shadow *s = &_shadow[id];

      qsort(s->_items, s->_n, sizeof (s->_items[0]),
	    compare_accum_test_shadow_item);
    }
}

void dump_shadow()
{
  for (int id = 0; id < NUM_ACCUM_IDS; id++)
    {
      accum_test_shadow *s = &_shadow[id];

      if (s->_n == 0)
	continue;

      fprintf (stderr, "ID: %d:", id);

      for (int i = 0; i < s->_n; i++)
	{
	  accum_test_shadow_item *item = &s->_items[i];

	  fprintf(stderr,
		  "  %04" PRIx64 " : %3d [%5d]",
		  item->_ts,
		  item->_value,
		  item->_insert_count);
	}
      fprintf (stderr, "\n");
    }
}

#define FATAL_MEMORY(purpose) do {				\
    fprintf (stderr, "Memory allocation error: %s\n", purpose);	\
    exit (1);							\
  } while (0)

int main(int argc, char *argv[])
{
  accum_item_test item;
  int i;
  int ret;

  for (i = 1; i < argc; i++)
    {
      if (strcmp(argv[i], "--debug") == 0)
	_debug = 1;
      else
	{
	  fprintf (stderr, "Unknown option: %s\n", argv[i]);
	  return 1;
	}
    }

  /* TODO: Should try with at least 3 different, and have marks for them. */
  _mbufs = lwroc_accum_many_init(1);

  if (!_mbufs)
    FATAL_MEMORY("Accumulation many-buffer.");

  _bufs = lwroc_accum_init(_mbufs, 0,
			   1 /* type */,
			   NUM_ACCUM_IDS,
			   report_ins_backwards,
			   report_pick_backwards,
			   report_resorted,
			   process_item);

  if (!_bufs)
    FATAL_MEMORY("Accumulation buffer.");

  ret = lwroc_accum_many_post_init(_mbufs);

  if (!ret)
    FATAL_MEMORY("Accumulation many-buffer post-init.");


  srand(1);

  for (int i = 0; i < 10000; i++)
    {
      int id;

      item._base._ts = rand() % 100;
      item._value    = rand() % 100;

      id = rand() % NUM_ACCUM_IDS;

      if (_debug)
	fprintf (stderr, "Insert ID %d %04" PRIx64 ".\n",
		 id, item._base._ts);

      insert_shadow(id, item._base._ts, item._value);

      lwroc_accum_insert_item(_bufs, id, &item._base, sizeof (item));

      if (rand() / (RAND_MAX / 10) == 0)
	{
	  uint64_t ts_until;

	  ts_until = rand() % 100;

	  if (_debug)
	    fprintf (stderr, "Sort until: %04" PRIx64 ".\n",
		     ts_until);

	  sort_shadow();

	  if (_debug)
	    dump_shadow();

	  _last_process_ts = 0;
	  _last_process_insert_count = 0;

	  lwroc_accum_sort_until(_mbufs, ts_until, NULL);

	  if (_debug)
	    dump_shadow();
	}
    }

  item._base._ts = 1;
  item._value    = 2;

  lwroc_accum_insert_item(_bufs, 5, &item._base, sizeof (item));

  return 0;
}
