/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#include "message.h"
#include "io_util.h"

int full_read(int fd, void *buf, size_t count)
{
  for ( ; ; )
    {
      ssize_t n = read(fd, buf, count);

      if (n == -1)
        {
          if (errno == EINTR ||
              errno == EAGAIN)
            continue;

          perror("read");
          break;
        }
      if (n == 0)
        {
	  break;
        }

      count -= (size_t) n;
      buf = (void *) (((char *) buf) + n);

      if (!count)
        return 1;
    }
  return 0;
}

int full_write(int fd, void *buf, size_t count)
{
  for ( ; ; )
    {
      ssize_t n = write(fd, buf, count);

      if (n == -1)
        {
          if (errno == EINTR ||
              errno == EAGAIN)
            continue;

          perror("write");
          break;
        }
      if (n == 0)
        {
	  ERROR("write: closed");
	  break;
        }

      count -= (size_t) n;
      buf = (void *) (((char *) buf) + n);

      if (!count)
        return 1;
    }
  return 0;
}

/* When writing to stdout, we want to make sure that all other
 * data to stdout actually goes to stderr.
 */
int dup_stdout(void)
{
  int fd;

  /* Make a copy of STDOUT_FILENO so we can write to it. */

  if ((fd = dup(STDOUT_FILENO)) == -1)
    {
      perror("dup");
      fprintf(stderr, "Failed to duplicate STDOUT.");
    }

  /* Then duplicate STDERR to also be used by stdout. */

  if (dup2(STDERR_FILENO,STDOUT_FILENO) == -1)
    {
      perror("dup2");
      fprintf(stderr, "Failed to duplicate STDERR to STDOUT.");
    }

  /* Re-evaluate terminals. */
  /* colourtext_prepare(); */

  return fd;
}
