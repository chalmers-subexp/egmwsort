/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#ifndef __INPUT_FILE_H__
#define __INPUT_FILE_H__

#include <stdint.h>
#include <stdio.h>

#include "pd_linked_list.h"
#include "accum_sort.h"

#define INPUT_OPTION_TYPE_FILE   1
#define INPUT_OPTION_TYPE_STDIN  2

typedef struct input_option_t
{
  pd_ll_item _options;

  int _type;
  const char *_name;

} input_option;

extern PD_LL_SENTINEL_ITEM(_input_options);

void sort_input_files(void);

#endif/*__INPUT_FILE_H__*/
