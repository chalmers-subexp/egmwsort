/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#include <stdio.h>
#include <inttypes.h>

#include "message.h"
#include "config.h"
#include "item.h"
#include "item_state.h"
#include "accum_sort.h"
#include "ebye_record_out.h"
#include "hit_out.h"
#include "ext_data.h"
#include "pulser.h"

#include "inc_colourtext.h"

void handle_febex_item(int32_t id, accum_item_febex *febex)
{
  (void) id;
  (void) febex;
}

void handle_asic_item(int32_t id, accum_item_asic *asic)
{
  (void) id;
  (void) asic;
}

void handle_asic_info_item(int32_t id, accum_item_asic_info *asic_info)
{
  uint32_t module = (id >>  4) & 0x3f;
  uint32_t code   = (id      ) & 0x0f;

  if (_egmw_config._track_pulser)
    if (code == 14)
      if (module < PULSER_ID_ASIC_NUM)
	pulser_got(PULSER_ID_ASIC_START + module, asic_info->_base._ts, 0);
}

void handle_caen_item(int32_t id, accum_item_caen *caen)
{
  uint32_t module = (id >> 6) & 0x1f;
  uint32_t ch     = (id     ) & 0x3f;

  if (_egmw_config._track_pulser)
    {
      if (module == 1 && ch == 13)
	{
	  uint16_t t_fine = 0;

	  if (caen->_mask & (1 << 3))
	    t_fine = caen->_adc_data[3];

	  pulser_got(PULSER_ID_CAEN, caen->_base._ts, t_fine);
	}
      if (module == 16 && ch == 13)
	{
	  uint16_t t_fine = 0;

	  if (caen->_mask & (1 << 3))
	    t_fine = caen->_adc_data[3];

	  pulser_got(PULSER_ID_MESY, caen->_base._ts, t_fine);
	}
    }
}

void handle_pixie_item(int32_t id, accum_item_pixie *pixie)
{
  (void) id;
  (void) pixie;
}

uint64_t _prev_rteb_trig_ts = 0;
int      _prev_rteb_trig_fired = 0;

void item_process_common_pre(uint32_t id, lwroc_accum_item_base *item,
			     void *arg)
{
  /*
  printf ("raw: %016" PRIx64 "  d: %10" PRId64 "",
	  item->_ts,
	  item->_ts - _prev_rteb_trig_ts);
  */

  if (item->_ts < _prev_rteb_trig_ts + _egmw_config._rteb_trig_coinc_window)
    {
      /* We consider the first entry to start a trigger window. */

      /* printf ("  trig? %d", !_prev_rteb_trig_fired); */

      if (!_prev_rteb_trig_fired)
	{
	  lwroc_accum_item_base trig;

	  trig._ts  = _prev_rteb_trig_ts - _egmw_config._rteb_window / 2;
	  trig._aux = 0;

	  /* printf (" -> %016" PRIx64 "",
	     trig._ts); */

	  lwroc_accum_insert_item(_accum_rteb_trig,
				  0,
				  &trig,
				  sizeof (trig));

	  /* Do not generate another trigger until we have seen a pause.
	   * event-building will generate blocks anyhow.
	   */
	  _prev_rteb_trig_fired = 1;
	}
    }
  else
    _prev_rteb_trig_fired = 0;

  _prev_rteb_trig_ts = item->_ts;

  /* printf ("\n"); */

  (void) id;
  (void) arg;
}

void item_process_common_post(uint32_t id, lwroc_accum_item_base *item,
			      void *arg)
{
  (void) id;
  (void) item;
  (void) arg;

#ifndef NO_EXT_DATA
  if (_ext_data_filled)
    send_ext_data();
#endif
}

extern uint64_t _prev_ts_mid_hi;

void item_process_febex_item(uint32_t type, uint32_t id,
			     lwroc_accum_item_base *item,
			     void *arg)
{
  accum_item_febex_raw *febex_raw = (accum_item_febex_raw *) item;
  accum_item_febex *febex = &(febex_raw->_febex);

  (void) type;

  if (_egmw_config._print_sort_hit_data)
    print_febex_item(id, febex, PRINT_SORT_HIT_PREFIX);

  handle_febex_item(id, febex);

  item_process_common_pre(id, item, arg);

#ifndef NO_EXT_DATA
  if (_egmw_config._fill_ext_data_raw)
    fill_ext_data_febex(&_ext_data, id, febex);
#endif

  if (_egmw_config._format_out_hits)
    hit_out_febex(id, febex, arg);
  else
    hit_out_raw(&(febex_raw->_raw), arg);

  item_process_common_post(id, item, arg);

  lwroc_accum_insert_item(_accum_rteb_febex,
			  0,
			  &(febex->_base),
			  sizeof (*febex));
}

void item_process_asic_item(uint32_t type, uint32_t id,
			    lwroc_accum_item_base *item,
			    void *arg)
{
  accum_item_asic_raw *asic_raw = (accum_item_asic_raw *) item;
  accum_item_asic *asic = &(asic_raw->_asic);

  (void) type;

  if (_egmw_config._print_sort_hit_data)
    print_asic_item(id, asic, PRINT_SORT_HIT_PREFIX);

  handle_asic_item(id, asic);

  item_process_common_pre(id, item, arg);

#ifndef NO_EXT_DATA
  if (_egmw_config._fill_ext_data_raw)
    fill_ext_data_asic(&_ext_data, id, asic);
#endif

  if (_egmw_config._format_out_hits)
    hit_out_asic(id, asic, arg);
  else
    hit_out_raw(&(asic_raw->_raw), arg);

  item_process_common_post(id, item, arg);

  lwroc_accum_insert_item(_accum_rteb_asic,
			  0,
			  &(asic->_base),
			  sizeof (*asic));

  lwroc_accum_insert_item(_accum_hb_ISIA,
			  0,
			  &(asic->_base),
			  sizeof (*asic));
}

void item_process_asic_info_item(uint32_t type, uint32_t id,
				 lwroc_accum_item_base *item,
				 void *arg)
{
  accum_item_asic_info_raw *asic_info_raw = (accum_item_asic_info_raw *) item;
  accum_item_asic_info *asic_info = &(asic_info_raw->_asic_info);

  (void) type;

  if (_egmw_config._print_sort_hit_data)
    print_asic_info_item(id, asic_info, PRINT_SORT_HIT_PREFIX);

  handle_asic_info_item(id, asic_info);

  item_process_common_pre(id, item, arg);

  if (_egmw_config._format_out_hits)
    hit_out_asic_info(id, asic_info, arg);
  else
    hit_out_raw(&(asic_info_raw->_raw), arg);

  item_process_common_post(id, item, arg);

  lwroc_accum_insert_item(_accum_rteb_asic_info,
			  0,
			  &(asic_info->_base),
			  sizeof (*asic_info));
}

void item_process_caen_item(uint32_t type, uint32_t id,
			    lwroc_accum_item_base *item,
			    void *arg)
{
  accum_item_caen_raw *caen_raw = (accum_item_caen_raw *) item;
  accum_item_caen *caen = &(caen_raw->_caen);

  (void) type;

  if (_egmw_config._print_sort_hit_data)
    print_caen_item(id, caen, PRINT_SORT_HIT_PREFIX);

  handle_caen_item(id, caen);

  item_process_common_pre(id, item, arg);

#ifndef NO_EXT_DATA
  if (_egmw_config._fill_ext_data_raw)
    fill_ext_data_caen(&_ext_data, id, caen);
#endif

  if (_egmw_config._format_out_hits)
    hit_out_caen(id, caen, arg);
  else
    hit_out_raw(&(caen_raw->_raw), arg);

  item_process_common_post(id, item, arg);

  lwroc_accum_insert_item(_accum_rteb_caen,
			  0,
			  &(caen->_base),
			  sizeof (*caen));

  {
    uint32_t module = (id >> 6) & 0x1f;
    uint32_t ch     = (id     ) & 0x3f;

    lwroc_accum_buffer *dest = NULL;
    /* uint32_t dest_id = -1; */

    if      (module == 0 && (ch >= 0 && ch < 7))
      {
	dest    = _accum_hb_REC;
	/* dest_id = ch - 0; */
      }
    else if (module == 1 && (ch >= 0 && ch < 3))
      {
	dest    = _accum_hb_ELUM;
	/* dest_id = ch - 0; */
      }
    else if (module == 1 && (ch >= 6 && ch < 7))
      {
	dest    = _accum_hb_ZD;
	/* dest_id = ch - 6; */
      }
    else if (module == 1 && (ch >= 8 && ch < 11))
      {
	dest    = _accum_hb_MWPC;
	/* dest_id = ch - 8; */
      }

    if (dest)
      {
	lwroc_accum_insert_item(dest,
				0,
				&(caen->_base),
				sizeof (*caen));
      }
  }
}

void item_process_pixie_item(uint32_t type, uint32_t id,
			     lwroc_accum_item_base *item,
			     void *arg)
{
  accum_item_pixie_raw *pixie_raw = (accum_item_pixie_raw *) item;
  accum_item_pixie *pixie = &(pixie_raw->_pixie);

  (void) type;

  if (_egmw_config._print_sort_hit_data)
    print_pixie_item(id, pixie, PRINT_SORT_HIT_PREFIX);

  handle_pixie_item(id, pixie);

  item_process_common_pre(id, item, arg);

#ifndef NO_EXT_DATA
  if (_egmw_config._fill_ext_data_raw)
    fill_ext_data_pixie(&_ext_data, id, pixie);
#endif

  if (_egmw_config._format_out_hits)
    hit_out_pixie(id, pixie, arg);
  else
    hit_out_raw(&(pixie_raw->_raw), arg);

  item_process_common_post(id, item, arg);

  lwroc_accum_insert_item(_accum_rteb_pixie,
			  0,
			  &(pixie->_base),
			  sizeof (*pixie));
}

/* Can be called from accum_insert_item() above. */
void item_report_rteb_ins_backwards(uint32_t type, uint32_t id,
				    lwroc_accum_item_base *item,
				    uint64_t last_ts)
{
  char name_id[256];

  item_name_id(name_id, sizeof (name_id), type | id | item->_aux, -1);

  ERROR("Insert EB timestamp backwards for %s "
	"(%016" PRIx64 " - %016" PRIx64 " = %" PRId64 ")",
	name_id,
	item->_ts, last_ts,
	(int64_t) (item->_ts - last_ts));
}

/* Can be called from accum_insert_item() above. */
void item_report_hb_ins_backwards(uint32_t type, uint32_t id,
				  lwroc_accum_item_base *item,
				  uint64_t last_ts)
{
  char name_id[256];

  item_name_hb_id(name_id, sizeof (name_id), type | id | item->_aux, -1);

  ERROR("Insert HB timestamp backwards for %s "
	"(%016" PRIx64 " - %016" PRIx64 " = %" PRId64 ")",
	name_id,
	item->_ts, last_ts,
	(int64_t) (item->_ts - last_ts));
}

