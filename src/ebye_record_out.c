/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <byteswap.h>
#include <arpa/inet.h>

#include "ebye_record_header.h"
#include "ebye_xfer_header.h"

#include "message.h"
#include "ebye_record_out.h"
#include "io_util.h"

#define BUFFER_LENGTH 0x10000
#define BUFFER_PADDING_RECORD (8+8) /* 8 bytes to allow 24->32 header
				     * growth for network transfers.
				     * Another 8 bytes to always have
				     * some padding bytes (for
				     * iss_sort).
				     */
#define BUFFER_PADDING_XFER (8)     /* Another 8 bytes to always have
				     * some padding bytes (for
				     * iss_sort).
				     */

char out_buffer[BUFFER_LENGTH];
char *out_p = NULL;
char *end_p = NULL;
int _first_record = 1;
int _output_fd = -1;
int _write_ebye_record = 1; /* 0 = xfer header */

void ebye_restart_output_buffer(void);
void ebye_do_write_output(int resize_block);

void ebye_init_output(int fd, int ebye_record)
{
  _output_fd = fd;
  _write_ebye_record = ebye_record;
  ebye_restart_output_buffer();
  if (!_write_ebye_record)
    ebye_do_write_output(1); /* XFER: Write initial (resize) block. */
}

void ebye_restart_output_buffer(void)
{
  if (_write_ebye_record)
    {
      out_p = out_buffer + sizeof (ebye_record_header);
      end_p = out_buffer + BUFFER_LENGTH - BUFFER_PADDING_RECORD;
    }
  else
    {
      out_p = out_buffer + sizeof (ebye_xfer_header);
      end_p = out_buffer + BUFFER_LENGTH - BUFFER_PADDING_XFER;
    }
}

void *ebye_request_output(size_t n, int *new_record, void *arg)
{
  (void) arg;

  *new_record = _first_record;

  _first_record = 0;

  if (out_p + n > end_p)
    {
      *new_record = 1;
      ebye_flush_output();
    }

  /* If still too large, then size cannot be handled. */
  if (out_p + n > end_p)
    FATAL("Too big output request (%zd).", n);

  char *p = out_p;

  return p;
}

void ebye_used_output(size_t n, void *arg)
{
  (void) arg;

  out_p += n;
}

uint32_t _sequence = 1;

void ebye_do_write_output(int resize_block)
{
  uint32_t length = BUFFER_LENGTH;

  if (_write_ebye_record)
    {
      ebye_record_header *record;

      record = (ebye_record_header *) out_buffer;

      memcpy(&record->_id, EBYE_RECORD_HEADER_ID, sizeof (record->_id));

      record->_sequence    = _sequence++;
      record->_tape        = 1;
      record->_stream      = 0;
      record->_endian_tape = 1; /* Byte order of this header. */
      record->_endian_data = 1; /* Byte order of data.        */
      record->_data_length = out_p - (out_buffer + sizeof (ebye_record_header));
    }
  else
    {
      ebye_xfer_header *xfer;

      xfer = (ebye_xfer_header *) out_buffer;

      xfer->_flags        = 2; /* No acq. */
      xfer->_stream       = 0;
      xfer->_endian       = 1; /* Byte order of data. */
      xfer->_id           = 0;
      /* xfer->_sequence */
      xfer->_block_length = BUFFER_LENGTH;
      /* xfer->_data_length */
      xfer->_offset       = 0;
      xfer->_id1          = EBYE_XFER_HEADER_ID1;
      xfer->_id2          = EBYE_XFER_HEADER_ID2;

      if (resize_block)
	{
	  length = 1024; /* Actual sent length. */

	  xfer->_sequence     = (uint32_t) -1;
	  xfer->_data_length  = (uint32_t) -1;
	}
      else
	{
	  xfer->_sequence     = _sequence++;
	  xfer->_data_length  =
	    out_p - (out_buffer + sizeof (ebye_xfer_header));
	}

      xfer->_flags        = htons(xfer->_flags);
      xfer->_stream       = htons(xfer->_stream);
      /* xfer->_endian */
      xfer->_id           = htons(xfer->_id);
      xfer->_sequence     = htonl(xfer->_sequence);
      xfer->_block_length = htonl(xfer->_block_length);
      xfer->_data_length  = htonl(xfer->_data_length);
      xfer->_offset       = htonl(xfer->_offset);
      xfer->_id1          = htonl(xfer->_id1);
      xfer->_id2          = htonl(xfer->_id2);
    }

  memset(out_p, 0x5e, out_buffer + length - out_p);

  if (_output_fd != -1)
    {
      if (!full_write(_output_fd, out_buffer, length))
	FATAL("Write failure.");
    }

  /* Reset write location. */

  ebye_restart_output_buffer();
}

void ebye_flush_output(void)
{
  ebye_do_write_output(0);
}
