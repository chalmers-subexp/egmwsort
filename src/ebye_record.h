/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#ifndef __EBYE_RECORD_H__
#define __EBYE_RECORD_H__

#include <stdint.h>

/* Processing for one buffer / record from file (or similar). */

extern uint32_t _record_sequence;

extern int _record_febex_had_backward;
extern int _records_febex_with_backward;
extern int _records_read;
extern int _record_febex_prev_board;
extern int _record_febex_board_back;

extern int _record_febex_num_sfp_board[4][16];

extern int _record_asic_num_asic_module[64][16];

extern int _record_caen_num_module[32];

/* Process data in one record. */
int ebye_read_record_items(char *p, uint32_t remain);

/* Read (and process) one record from file. */
int ebye_read_record(int fd);

/* All records have been read, process remaining. */
void ebye_records_done(void);

void ebye_records_summary(void);

/* Handle 2023 data rewriting bug after emitting one full record. */

#define BUG2023_RECORD_FULL_LIMIT  65480

typedef struct fullbug2023_state_t
{
  int _next_is_ok;

  int _ok_at_start;

  int _had_sfp_change;
  int _had_board_restart;

  uint32_t _prev_sfp;
  uint32_t _prev_board;

} fullbug2023_state;

extern fullbug2023_state _bug2023_info;

#endif/*__EBYE_RECORD_H__*/
