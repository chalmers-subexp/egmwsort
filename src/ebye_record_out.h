/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#ifndef __EBYE_RECORD_OUT_H__
#define __EBYE_RECORD_OUT_H__

/* Prepare output handling. */
void ebye_init_output(int fd, int ebye_record);

/* To be called to request space for one hit. */
void *ebye_request_output(size_t n, int *new_record, void *arg);

/* To be called after one hit has been filled. */
void ebye_used_output(size_t n, void *arg);

/* Write data accumulated in record. */
void ebye_flush_output(void);

#endif/*__EBYE_RECORD_OUT_H__*/
