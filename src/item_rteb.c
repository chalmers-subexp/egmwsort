/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#include <stdio.h>
#include <inttypes.h>

#include "message.h"
#include "config.h"
#include "item.h"
#include "item_state.h"
#include "accum_sort.h"
#include "ext_data.h"

#include "inc_colourtext.h"

uint64_t _rteb_window_start_ts = 0;

void item_rteb_end_window(void)
{
  /* printf ("close window\n"); */

#ifndef NO_EXT_DATA
  if (_ext_data_filled)
    send_ext_data();
#endif

  _rteb_window_start_ts = 0;
}

void item_process_rteb_common_pre(uint32_t id, lwroc_accum_item_base *item,
				  void *arg)
{
  (void) id;
  (void) item;
  (void) arg;

  /* printf ("pre: %016" PRIx64 "\n", item->_ts); */

  if (_rteb_window_start_ts)
    {
      /* Can we continue to build on this item. */

      if (item->_ts > _rteb_window_start_ts + _egmw_config._rteb_window)
	item_rteb_end_window();
    }

  if (!_rteb_window_start_ts)
    {
      _rteb_window_start_ts = item->_ts;
    }
}

void item_process_rteb_common_post(uint32_t id, lwroc_accum_item_base *item,
				   void *arg)
{
  (void) id;
  (void) item;
  (void) arg;
}

void item_process_rteb_febex_item(uint32_t type, uint32_t id,
				  lwroc_accum_item_base *item,
				  void *arg)
{
  accum_item_febex *febex = (accum_item_febex *) item;

  (void) febex;

  item_process_rteb_common_pre(id, item, arg);

#ifndef NO_EXT_DATA
  if (_egmw_config._fill_ext_data_rteb)
    fill_ext_data_febex(&_ext_data, id, febex);
#endif

  item_process_rteb_common_post(id, item, arg);
}

void item_process_rteb_asic_item(uint32_t type, uint32_t id,
				 lwroc_accum_item_base *item,
				 void *arg)
{
  accum_item_asic *asic = (accum_item_asic *) item;

  (void) asic;

  item_process_rteb_common_pre(id, item, arg);

#ifndef NO_EXT_DATA
  if (_egmw_config._fill_ext_data_rteb)
    fill_ext_data_asic(&_ext_data, id, asic);
#endif

  item_process_rteb_common_post(id, item, arg);
}

void item_process_rteb_asic_info_item(uint32_t type, uint32_t id,
				      lwroc_accum_item_base *item,
				      void *arg)
{
  accum_item_asic_info *asic_info = (accum_item_asic_info *) item;

  (void) asic_info;

  item_process_rteb_common_pre(id, item, arg);

  item_process_rteb_common_post(id, item, arg);
}

void item_process_rteb_caen_item(uint32_t type, uint32_t id,
				 lwroc_accum_item_base *item,
				 void *arg)
{
  accum_item_caen *caen = (accum_item_caen *) item;

  (void) caen;

  item_process_rteb_common_pre(id, item, arg);

#ifndef NO_EXT_DATA
  if (_egmw_config._fill_ext_data_rteb)
    fill_ext_data_caen(&_ext_data, id, caen);
#endif

  item_process_rteb_common_post(id, item, arg);
}

void item_process_rteb_pixie_item(uint32_t type, uint32_t id,
				 lwroc_accum_item_base *item,
				 void *arg)
{
  accum_item_pixie *pixie = (accum_item_pixie *) item;

  (void) pixie;

  item_process_rteb_common_pre(id, item, arg);

#ifndef NO_EXT_DATA
  if (_egmw_config._fill_ext_data_rteb)
    fill_ext_data_pixie(&_ext_data, id, pixie);
#endif

  item_process_rteb_common_post(id, item, arg);
}

void item_process_rteb_trig_item(uint32_t type, uint32_t id,
				 lwroc_accum_item_base *item,
				 void *arg)
{
  lwroc_accum_item_base *trig = item;

  (void) trig;

  /* printf ("trig: %016" PRIx64 "\n", item->_ts); */

  /* Close whatever event-build item was in progress. */
  item_rteb_end_window();

  item_process_rteb_common_pre(id, item, arg);

  item_process_rteb_common_post(id, item, arg);
}
