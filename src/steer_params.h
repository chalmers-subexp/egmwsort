/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#ifndef __STEER_PARAMS_H__
#define __STEER_PARAMS_H__

/* With a 10 Hz pulser, each 64 kiB buffer can hold:
 *
 * CAEN: 1 info + 3 data words = 32 bytes / hit
 * ASIC: 3 info                = 24 bytes / hit
 *
 * 2^16 / (24 / 10 Hz) = 273 s worth of data.
 */

#define TS_SORT_MIN_MARGIN  300 * 1000000000ll  /* 300 s */

/* Number of records with (only) backwards timestamps at which flush
 * of all pending items is performed.  (In order to also get rid of
 * the suspected too-far-future timestamp(s) that are far future.)
 */

#define NUM_BACKWARDS_BEFORE_FLUSH_SORT  5

#endif/*__STEER_PARAMS_H__*/
