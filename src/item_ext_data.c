/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#include <stdio.h>
#include <inttypes.h>
#include <string.h>

#include "message.h"
#include "config.h"
#include "item.h"
#include "item_state.h"
#include "ext_data.h"

#include "ebye_record.h"
#include "hit_out.h"

#ifndef NO_EXT_DATA
void fill_ext_data_febex(struct ext_data *ed,
			 uint32_t id,
			 accum_item_febex *febex)
{
  /* int j; */
  int i;

  i = ed->_febex_n++;

  ed->_febex_id[i] = id | (febex->_base._aux & ~ACCUM_ID_TYPE_MASK);

  ed->_febex_ts_l[i] = (uint32_t)  febex->_base._ts;
  ed->_febex_ts_h[i] = (uint32_t) (febex->_base._ts >> 32);

  /*
  for (j = 0; j < 4; j++)
    if (febex->_mask & (1 << j))
      ed->_febex_adc[i][j] = febex->_adc_data[j];
  */
  if (febex->_mask & (1 << (0))) ed->_febex_adc_a[i]=febex->_adc_data[0];
  if (febex->_mask & (1 << (1))) ed->_febex_adc_b[i]=febex->_adc_data[1];
  if (febex->_mask & (1 << (2))) ed->_febex_adc_c[i]=febex->_adc_data[2];
  if (febex->_mask & (1 << (3))) ed->_febex_adc_d[i]=febex->_adc_data[3];

  _ext_data_filled = 1;
}
#endif

#ifndef NO_EXT_DATA
void fill_ext_data_asic(struct ext_data *ed,
			uint32_t id,
			accum_item_asic *asic)
{
  int i;

  i = ed->_asic_n++;

  ed->_asic_id[i] = id | (asic->_base._aux & ~ACCUM_ID_TYPE_MASK);

  ed->_asic_ts_l[i] = (uint32_t)  asic->_base._ts;
  ed->_asic_ts_h[i] = (uint32_t) (asic->_base._ts >> 32);

  ed->_asic_adc[i] = asic->_adc_data;

  _ext_data_filled = 1;
}
#endif

#ifndef NO_EXT_DATA
void fill_ext_data_caen(struct ext_data *ed,
			uint32_t id,
			accum_item_caen *caen)
{
  /* int j; */
  int i;

  i = ed->_caen_n++;

  /* printf ("%4d: %5d\n", id, caen->_adc_data[3]); */

  ed->_caen_id[i] = id | (caen->_base._aux & ~ACCUM_ID_TYPE_MASK);

  ed->_caen_ts_l[i] = (uint32_t)  caen->_base._ts;
  ed->_caen_ts_h[i] = (uint32_t) (caen->_base._ts >> 32);

  /*
  for (j = 0; j < 4; j++)
    if (caen->_mask & (1 << j))
      ed->_caen_adc[i][j] = caen->_adc_data[j];
  */
  if (caen->_mask & (1 << (0))) ed->_caen_adc_a[i] = caen->_adc_data[0];
  if (caen->_mask & (1 << (1))) ed->_caen_adc_b[i] = caen->_adc_data[1];
  if (caen->_mask & (1 << (2))) ed->_caen_adc_c[i] = caen->_adc_data[2];
  if (caen->_mask & (1 << (3))) ed->_caen_adc_d[i] = caen->_adc_data[3];

  _ext_data_filled = 1;
}
#endif

#ifndef NO_EXT_DATA
void fill_ext_data_pixie(struct ext_data *ed,
			 uint32_t id,
			 accum_item_pixie *pixie)
{
  int i;

  i = ed->_pixie_n++;

  /* printf ("%4d: %5d\n", id, pixie->_adc_data[3]); */

  ed->_pixie_id[i] = id | (pixie->_base._aux & ~ACCUM_ID_TYPE_MASK);

  ed->_pixie_ts_l[i] = (uint32_t)  pixie->_base._ts;
  ed->_pixie_ts_h[i] = (uint32_t) (pixie->_base._ts >> 32);

  ed->_pixie_adc[i] = pixie->_adc_data;

  _ext_data_filled = 1;
}
#endif

