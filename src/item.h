/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#ifndef __ITEM_H__
#define __ITEM_H__

#include <stdint.h>

#include "accum_sort.h"

#define MAX_RAW_WORDS             12

/* This is not suitable for trace data. */
typedef struct accum_item_raw_t
{
  uint32_t              _raw_words;
  uint32_t              _raw[MAX_RAW_WORDS];

} accum_item_raw;

/* Hold internal representation of ebye hits.
 */

typedef struct accum_item_febex_t
{
  lwroc_accum_item_base _base;        /* Timestamp. */
  uint32_t              _mask;        /* Valid data-words. */
  uint32_t              _adc_data[4];

} accum_item_febex;

typedef struct accum_item_asic_t
{
  lwroc_accum_item_base _base;
  uint32_t              _adc_data;

} accum_item_asic;

typedef struct accum_item_asic_info_t
{
  lwroc_accum_item_base _base;

  /* What kind of info item? (type) */

} accum_item_asic_info;

typedef struct accum_item_caen_t
{
  lwroc_accum_item_base _base;
  uint32_t              _mask;
  uint32_t              _adc_data[4];

} accum_item_caen;

typedef struct accum_item_pixie_t
{
  lwroc_accum_item_base _base;
  uint32_t              _adc_data;
  uint32_t              _ts_frac;

} accum_item_pixie;

typedef struct accum_item_febex_raw_t
{
  accum_item_febex      _febex;
  accum_item_raw        _raw;

} accum_item_febex_raw;

typedef struct accum_item_asic_raw_t
{
  accum_item_asic       _asic;
  accum_item_raw        _raw;

} accum_item_asic_raw;

typedef struct accum_item_asic_info_raw_t
{
  accum_item_asic_info  _asic_info;
  accum_item_raw        _raw;

} accum_item_asic_info_raw;

typedef struct accum_item_caen_raw_t
{
  accum_item_caen       _caen;
  accum_item_raw        _raw;

} accum_item_caen_raw;

typedef struct accum_item_pixie_raw_t
{
  accum_item_pixie      _pixie;
  accum_item_raw        _raw;

} accum_item_pixie_raw;

/* Item accumulation (time sorting) from input data blocks to
 * time-sorted signals.
 */

extern lwroc_accum_buffer *_accum_febex;
extern lwroc_accum_buffer *_accum_asic;
extern lwroc_accum_buffer *_accum_asic_info;
extern lwroc_accum_buffer *_accum_caen;
extern lwroc_accum_buffer *_accum_pixie;

/* Item accumulation (second time sorting) of time-sorted signals
 * together with a signal-based software trigger.
 *
 * Raw trigger event builder.
 */

extern lwroc_accum_buffer *_accum_rteb_febex;
extern lwroc_accum_buffer *_accum_rteb_asic;
extern lwroc_accum_buffer *_accum_rteb_asic_info;
extern lwroc_accum_buffer *_accum_rteb_caen;
extern lwroc_accum_buffer *_accum_rteb_pixie;

extern lwroc_accum_buffer *_accum_rteb_trig;

/* Detector-assigned item accumulation (second time sorting) of
 * time-sorted signals.  Used for signal (DAM channel) to hit
 * conversions.
 *
 * Hit builder.
 *
 * TODO: should not insert into lwroc_accum_buffer but into pipes to
 * separate threads for processing.  For now - get something working.
 */

extern lwroc_accum_buffer *_accum_hb_ELUM;
extern lwroc_accum_buffer *_accum_hb_LUME;
extern lwroc_accum_buffer *_accum_hb_ISIA;
extern lwroc_accum_buffer *_accum_hb_CD;
extern lwroc_accum_buffer *_accum_hb_REC;
extern lwroc_accum_buffer *_accum_hb_ZD;
extern lwroc_accum_buffer *_accum_hb_MWPC;

/* */

/* Use when reading items, to print item offset. */
extern char *_record_buffer;

/* */

void item_init_state(void);

void item_expect_spec(const char *spec);

/* Handle one 2-word piece of data. */
int item_ebye_read(uint32_t *p, uint32_t remain);

/* Call after reading all pieces of data in a block/record. */
void item_ebye_read_end(void);

/* Handle one hit. */
int item_ldf_read(uint32_t *p, uint32_t remain);

/* Accumulated item is complete.
 * (Or rather, new has started.  So handle previous.)
 */
void item_got_accum_item(void);

/* Format name and index of an item. */
void item_name_id(char *name, size_t n,
		  uint32_t id, uint32_t extra_id);

void item_name_hb_id(char *name, size_t n,
		     uint32_t id, uint32_t extra_id);

/* Sort and pass along old enough items.
 * Old enough = no older are expected any longer.
 */
void item_process_items(void);

/* Final sort and pass along of all remaining items. */
void item_process_remaining(void);

void item_summary(void);

/* Printing of items. */

void print_febex_item(int32_t id, accum_item_febex *febex,
		      const char *prefix);
void print_asic_info_item(int32_t id, accum_item_asic_info *asic_info,
			  const char *prefix);
void print_asic_item(int32_t id, accum_item_asic *asic,
		     const char *prefix);
void print_caen_item(int32_t id, accum_item_caen *caen,
		     const char *prefix);
void print_pixie_item(int32_t id, accum_item_pixie *pixie,
		      const char *prefix);

/* Processing of items (after sort), first-stage. */

void item_process_febex_item(uint32_t type, uint32_t id,
			     lwroc_accum_item_base *item,
			     void *arg);
void item_process_asic_item(uint32_t type, uint32_t id,
			    lwroc_accum_item_base *item,
			    void *arg);
void item_process_asic_info_item(uint32_t type, uint32_t id,
				 lwroc_accum_item_base *item,
				 void *arg);
void item_process_caen_item(uint32_t type, uint32_t id,
			    lwroc_accum_item_base *item,
			    void *arg);
void item_process_pixie_item(uint32_t type, uint32_t id,
			     lwroc_accum_item_base *item,
			     void *arg);

/* Processing of items (after sort), second-stage. */

void item_process_rteb_febex_item(uint32_t type, uint32_t id,
				  lwroc_accum_item_base *item,
				  void *arg);
void item_process_rteb_asic_item(uint32_t type, uint32_t id,
				 lwroc_accum_item_base *item,
				 void *arg);
void item_process_rteb_asic_info_item(uint32_t type, uint32_t id,
				      lwroc_accum_item_base *item,
				      void *arg);
void item_process_rteb_caen_item(uint32_t type, uint32_t id,
				 lwroc_accum_item_base *item,
				 void *arg);
void item_process_rteb_pixie_item(uint32_t type, uint32_t id,
				  lwroc_accum_item_base *item,
				  void *arg);
void item_process_rteb_trig_item(uint32_t type, uint32_t id,
				 lwroc_accum_item_base *item,
				 void *arg);

/* Processing of items for hit-building (after sort), second-stage. */

void item_process_hb_LUME_item(uint32_t type, uint32_t id,
			       lwroc_accum_item_base *item,
			       void *arg);
void item_process_hb_ELUM_item(uint32_t type, uint32_t id,
			       lwroc_accum_item_base *item,
			       void *arg);
void item_process_hb_ISIA_item(uint32_t type, uint32_t id,
			       lwroc_accum_item_base *item,
			       void *arg);
void item_process_hb_CD_item(uint32_t type, uint32_t id,
			     lwroc_accum_item_base *item,
			     void *arg);
void item_process_hb_REC_item(uint32_t type, uint32_t id,
			      lwroc_accum_item_base *item,
			      void *arg);
void item_process_hb_ZD_item(uint32_t type, uint32_t id,
			     lwroc_accum_item_base *item,
			     void *arg);
void item_process_hb_MWPC_item(uint32_t type, uint32_t id,
			       lwroc_accum_item_base *item,
			       void *arg);

/* Filling ext_data structure. */

struct ext_data;

void fill_ext_data_febex(struct ext_data *ed,
			 uint32_t id,
			 accum_item_febex *febex);
void fill_ext_data_asic(struct ext_data *ed,
			uint32_t id,
			accum_item_asic *asic);
void fill_ext_data_caen(struct ext_data *ed,
			uint32_t id,
			accum_item_caen *caen);
void fill_ext_data_pixie(struct ext_data *ed,
			 uint32_t id,
			 accum_item_pixie *pixie);

#endif/*__ITEM_H__*/
