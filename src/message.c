/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


/* Must be early, or stdio may miss vs(n)printf. */
#include <stdarg.h>

#include <stdio.h>
#include <stdlib.h>

#include "message.h"

#include "inc_colourtext.h"

void message_colourstr(int level, int outerr,
		       const char **ctext,
		       const char **ctextback);

int _message_rate_limit = 0;
int _message_seen = 0;
int _message_suppressed[3] = { 0, 0, 0 }; /* Errors, warnings, lower. */
int _message_printed = 0;

void message_rate_limit_begin(void)
{
  _message_rate_limit = 1;
  _message_seen = 0;
}

void message_rate_limit_end(void)
{
  _message_rate_limit = 0;

  if (_message_suppressed[0] ||
      _message_suppressed[1] ||
      _message_suppressed[2])
    {
      INFO("Messages suppressed: %d errors, %d warnings, %d <= info.",
	   _message_suppressed[0],
	   _message_suppressed[1],
	   _message_suppressed[2]);

      memset(_message_suppressed, 0, sizeof (_message_suppressed));
    }
}

void do_message_internal(int level,
			 const char *file, int line,
			 const char *fmt, va_list ap)
{
  char msg[1024];
  const char *ctext;
  const char *ctextback;

  if (_message_rate_limit)
    {
      _message_seen++;

      if (_message_seen >= 5)
	{
	  int kind;

	  if (level <= MSGLVL_ERROR)
	    kind = 0;
	  else if (level <= MSGLVL_WARNING)
	    kind = 1;
	  else
	    kind = 2;

	  _message_suppressed[kind]++;

	  return;
	}
    }

  vsnprintf(msg, sizeof (msg), fmt, ap);

  message_colourstr(level, 1, &ctext, &ctextback);

  fflush(stdout); /* To avoid mixing of lines. */

  fprintf (stderr, "%s%s%s%s\n",
	   ctext, msg, ctextback, CT_ERR(CLR_EOL));

  _message_printed++;

  if (level == MSGLVL_SIGNAL ||
      level == MSGLVL_BUG ||
      level == MSGLVL_FATAL ||
      level == MSGLVL_BADCFG)
    exit(1);
}

void message_internal(int level,
		      const char *file, int line,
		      const char *fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
  do_message_internal(level, file, line, fmt, ap);
  va_end(ap);
}

void message_colourstr(int level, int outerr,
		       const char **ctext,
		       const char **ctextback)
{
  *ctext = "";
  *ctextback = "";

  (void) outerr; /* In case not used in CT_OUTERR below. */

  switch (level)
    {
    case MSGLVL_SIGNAL:
      *ctext = CT_OUTERR(outerr,YELLOW_BG_MAGENTA);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case MSGLVL_BUG:
      *ctext = CT_OUTERR(outerr,RED_BG_GREEN);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case MSGLVL_FATAL:
      *ctext = CT_OUTERR(outerr,WHITE_BG_MAGENTA);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case MSGLVL_BADCFG:
      *ctext = CT_OUTERR(outerr,YELLOW_BG_MAGENTA);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case MSGLVL_ERROR:
      *ctext = CT_OUTERR(outerr,WHITE_BG_RED);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
      /*
    case MSGLVL_ATTACH:
      *ctext = CT_OUTERR(outerr,WHITE_BG_GREEN);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case MSGLVL_DETACH:
      *ctext = CT_OUTERR(outerr,YELLOW_BG_RED);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
      */
    case MSGLVL_WARNING:
      *ctext = CT_OUTERR(outerr,BLACK_BG_YELLOW);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
      /*
    case MSGLVL_ACTION:
      *ctext = CT_OUTERR(outerr,YELLOW_BG_BLUE);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
      */
    case MSGLVL_INFO:
      *ctext = CT_OUTERR(outerr,GREEN);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case MSGLVL_LOG:
      /* Normal terminal colour. */
      break;
    case MSGLVL_DEBUG:
      *ctext = CT_OUTERR(outerr,MAGENTA);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case MSGLVL_SPAM:
      *ctext = CT_OUTERR(outerr,BLUE);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    }
}
