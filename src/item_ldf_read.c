/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#include <stdio.h>
#include <inttypes.h>
#include <string.h>

#include "message.h"
#include "config.h"
#include "item.h"
#include "item_state.h"

#include "ldf_record.h"

int item_ldf_read(uint32_t *p, uint32_t remain)
{
  uint32_t w0_ev_len;

  /* All items are to have at least four words, check that. */
  if (remain < 4 * sizeof (uint32_t))
    {
      ERROR("Non-zero (%d) left, but less than minimum item size.", remain);
      return 0;
    }

  w0_ev_len = (p[0] >> 17) & 0x3fff;

  if (remain < w0_ev_len * sizeof (uint32_t))
    {
      ERROR("Non-zero (%d) left, but less than item size (%d * %d).",
	    remain, w0_ev_len, (int) sizeof (uint32_t));
      return 0;
    }

  uint32_t w0_finish     = (p[0] >> 31) & 1;
  uint32_t w0_hdr_len    = (p[0] >> 12) & 0x1f;
  uint32_t w0_crate      = (p[0] >>  8) & 0x0f;
  uint32_t w0_slot       = (p[0] >>  4) & 0x0f;
  uint32_t w0_ch         = (p[0]      ) & 0x0f;

  uint32_t w1_ts_lo      =  p[1];

  uint32_t w2_cfd_forced = (p[2] >> 31) & 1;
  uint32_t w2_cfd_trig   = (p[2] >> 30) & 1;
  uint32_t w2_ts_frac    = (p[2] >> 16) & 0x3fff;
  uint32_t w2_ts_hi      = (p[2]      ) & 0xffff;

  uint32_t w3_trace_oor  = (p[3] >> 31) & 1;
  uint32_t w3_trace_len  = (p[3] >> 16) & 0x7fff;
  uint32_t w3_adc        = (p[3]      ) & 0xffff;

  /* printf ("ev: %d (%08x)\n", w0_ev_len, p[0]); */

  uint64_t ts =
    (((uint64_t) w2_ts_hi)   << 36) |
    (((uint64_t) w1_ts_lo)   <<  4) |
    (((uint64_t) w2_ts_frac) >> 12);

  if (_egmw_config._print_raw_data)
    fprintf (stdout,
	     "%05zd "
	     "PIXIE: %02d/%02d/%02d 0x%04x .%03x     @ %016" PRIx64 "\n",
	     (((char *) p) - _record_buffer) / 4,
	     w0_crate, w0_slot, w0_ch,
	     w3_adc, w2_ts_frac & 0xfff,
	     ts);

  uint32_t accum_id = (ACCUM_ID_TYPE_PIXIE |
		       (w0_crate << 8) | (w0_slot << 4) | w0_ch);
  /*
  CHECK_TS_BACKWARDS(ts, _item_state._ts_last._pixie[w0_module],
		     TS_FAR_BACKWARDS_PIXIE,
		     item_name_id, accum_id, w0_data_idx);

  CHECK_TS_BACKWARDS(ts, _item_state._ts_last._pixie_grp[w0_module / 16],
		     TS_FAR_BACKWARDS_PIXIE,
		     item_name_id, accum_id, w0_data_idx);
  */
  _record_pixie_num_crate_slot[w0_crate][w0_slot]++;

  accum_item_pixie_raw pixie_raw;

  pixie_raw._raw._raw_words   = 0;

  pixie_raw._pixie._base._ts  = ts;
  pixie_raw._pixie._base._aux = accum_id;
  pixie_raw._pixie._adc_data  = w3_adc;
  pixie_raw._pixie._ts_frac   = w2_ts_frac & 0xfff;

  (void) w0_finish;
  (void) w0_hdr_len;
  (void) w2_cfd_forced;
  (void) w2_cfd_trig;
  (void) w3_trace_oor;
  (void) w3_trace_len;

  /* We can insert directly. */
  {
    /* COPY_RAW_DATA(&pixie_raw._raw); */

    if (_egmw_config._print_hit_data)
      print_pixie_item(accum_id & ~ACCUM_ID_TYPE_MASK,
		      &pixie_raw._pixie,
		      PRINT_HIT_PREFIX);
    if (_egmw_config._accum_sort)
      lwroc_accum_insert_item(_accum_pixie,
			      accum_id & ~ACCUM_ID_TYPE_MASK,
			      &pixie_raw._pixie._base,
			      sizeof (pixie_raw));
  }

  return w0_ev_len * sizeof (uint32_t);
}
