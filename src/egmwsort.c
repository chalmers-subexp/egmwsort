/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>

#include "message.h"
#include "config.h"
#include "ebye_record.h"
#include "ldf_record.h"
#include "item.h"
#include "ebye_record_out.h"
#include "io_util.h"
#include "input_file.h"
#include "pulser.h"

#include "ext_data.h"

#include "inc_colourtext.h"
#include "parse_util.h"

#include "git-describe-include.h"

void main_usage(char *cmdname)
{
  printf ("Miniball (FEBEX) / ISS (ASIC/CAEN) unpacker (%s).\n",
          GIT_DESCRIBE_STRING);

  printf ("\n");
  printf ("Usage: %s <options> INPUT...\n", cmdname);
  printf ("\n");
  printf ("  INPUT                    Input filename(s), or - for stdin.\n");
  printf ("\n");
  /* Input options: */
  printf ("  --ldf                    Read LDF format data (instead of EBYE).\n");
  printf ("  --expect=SPEC            Subsystems expected, e.g. 'c01a012', or 'help'.\n");
  printf ("  --synthetic-check        Check synthetic data (anti-loss/dup).\n");
  printf ("\n");
  /* Event building. */
  printf ("  --trig-coinc=N           Trigger coincidence window (ns).\n");
  printf ("  --eb-window=N            Event building window (ns).\n");
  printf ("\n");
  /* Printing options: */
  printf ("  --print-raw              Print raw data items.\n");
  printf ("  --print-hit              Print hit data items (combined raw).\n");
  printf ("  --print-sort-hit         Print hit data items (when time-sorted).\n");
  printf ("  --print-record           Print record info.\n");
  printf ("\n");
  /* Analysis options: */
  printf ("  --track-pulser           Analyse pulser correlations.\n");
  printf ("\n");
  /* Output options: */
  printf ("  --output=FILENAME        Output filename, or - for stdout.\n");
  printf ("  --format-out-hits        Regenerate output hits from unpacked (test code).\n");
  printf ("  --dump=FILENAME          Debug dump filename.\n");
  printf ("  --ext-data-raw           Ext data (root/struct) raw instead of event-built.\n");
  usage_ext_data();
  printf ("\n");
}

PD_LL_SENTINEL(_input_options);

void add_input(int type, const char *name)
{
  input_option *item;

  item = malloc(sizeof (input_option));

  if (!item)
    FATAL_MEMORY("input item");

  memset (item, 0, sizeof (input_option));

  item->_type = type;
  item->_name = strdup(name);

  PD_LL_ADD_BEFORE(&_input_options, &item->_options);
}

int add_input_try_follow_link(const char *path)
{
  struct stat buf;

  if (strcmp(path, "-") == 0)
    {
      add_input(INPUT_OPTION_TYPE_STDIN, path);
      return 1;
    }

  /* Test if it is a file name.
   */

  if (stat(path,&buf) == 0)
    {
      add_input(INPUT_OPTION_TYPE_FILE, path);
      return 1;
    }

  return 0;
}

int main(int argc,char *argv[])
{
  int i;

  FILE *in_fid = NULL;
  FILE *out_fid = NULL;

  int ext_data_raw = 0;

  int ldf = 0;

  pd_ll_item *iter;

  memset(&_egmw_config, 0, sizeof (_egmw_config));

  _egmw_config._rteb_trig_coinc_window =  10000;
  _egmw_config._rteb_window            = 100000;

  colourtext_init();

  _stderr_isatty = isatty(STDERR_FILENO);

  for (i = 1; i < argc; i++)
    {
      char *request = argv[i];
      char *post;

      if (LWROC_MATCH_C_ARG("--help")) {
        main_usage(argv[0]);
        exit(0);
      } else if (LWROC_MATCH_C_ARG("--ldf")) {
	ldf = 1;
      } else if (LWROC_MATCH_C_ARG("--print-raw")) {
	_egmw_config._print_raw_data = 1;
      } else if (LWROC_MATCH_C_ARG("--print-hit")) {
	_egmw_config._print_hit_data = 1;
      } else if (LWROC_MATCH_C_ARG("--print-sort-hit")) {
	_egmw_config._print_sort_hit_data = 1;
      } else if (LWROC_MATCH_C_ARG("--print-record")) {
	_egmw_config._print_record = 1;
      } else if (LWROC_MATCH_C_ARG("--format-out-hits")) {
	_egmw_config._format_out_hits = 1;
      } else if (LWROC_MATCH_C_PREFIX("--input=",post)) {
	BADCFG("--input=FILE option deprecated.  Just use FILE.");
      } else if (LWROC_MATCH_C_PREFIX("--output=",post)) {
	_out_filename = post;
      } else if (LWROC_MATCH_C_PREFIX("--dump=",post)) {
	_dump_filename = post;
      } else if (LWROC_MATCH_C_ARG("--synthetic-check")) {
	_egmw_config._synthetic_data_check = 1;
      } else if (LWROC_MATCH_C_ARG("--track-pulser")) {
	_egmw_config._track_pulser = 1;
      } else if (LWROC_MATCH_C_PREFIX("--track-pulser=",post)) {
	_egmw_config._track_pulser_cfg = post;
	_egmw_config._track_pulser = 1;
      } else if (LWROC_MATCH_C_PREFIX("--expect=",post)) {
	_egmw_config._expect_subsystem_spec = post;
      } else if (LWROC_MATCH_C_PREFIX("--trig-coinc=",post)) {
	_egmw_config._rteb_trig_coinc_window = atoi(post);
      } else if (LWROC_MATCH_C_PREFIX("--eb-window=",post)) {
	_egmw_config._rteb_window = atoi(post);
      } else if (LWROC_MATCH_C_ARG("--ext-data-raw")) {
	ext_data_raw = 1;
      }	else if (match_ext_data_arg(request)) {
	_egmw_config._fill_ext_data_rteb = 1;
      } else {

	if (add_input_try_follow_link(request))
	  ;
	else
	  {
	    BADCFG("Unrecognized or invalid option "
		   "(also not a file): '%s'.",
		   request);
	  }
      }
    }

  if (ext_data_raw)
    {
      if (!_egmw_config._fill_ext_data_rteb)
	BADCFG("--ext-data-raw requires ext data destination (root/struct).");

      _egmw_config._fill_ext_data_rteb = 0;
      _egmw_config._fill_ext_data_raw = 1;
    }

  if (PD_LL_IS_EMPTY(&_input_options))
    {
      if (argc <= 1)
	{
	  main_usage(argv[0]);
	  exit(1);
	}
      else
	BADCFG("No input option specified.");
    }

  if (_dump_filename)
    {
      _dump_fid = fopen(_dump_filename, "wt");
      if (_dump_fid == NULL)
	{
	  perror("fopen");
	  FATAL("Failed to open dump file: %s",
		_dump_filename);
	}
    }

  if (_out_filename)
    {
      if (strcmp(_out_filename,"-") == 0)
	{
	  out_fid = fdopen(dup_stdout(), "wt");
	}
      else
	{
	  out_fid = fopen(_out_filename, "wt");
	  if (out_fid == NULL)
	    {
	      perror("fopen");
	      FATAL("Failed to open output file: %s",
		    _out_filename);
	    }
	}
    }

  /* Options that require the sorting to be performed. */
  if (out_fid ||
      _egmw_config._print_sort_hit_data ||
      _egmw_config._track_pulser ||
      _egmw_config._fill_ext_data_raw ||
      _egmw_config._fill_ext_data_rteb)
    _egmw_config._accum_sort = 1;

  sort_input_files();

  item_init_state();

  item_expect_spec(_egmw_config._expect_subsystem_spec);

  if (_egmw_config._track_pulser)
    pulser_setup(_egmw_config._track_pulser_cfg);

  ebye_init_output(out_fid ? fileno(out_fid) : -1, 1 /* record headers */);

  init_ext_data(argv[0]);

  PD_LL_FOREACH(_input_options, iter)
    {
      input_option *item =
	PD_LL_ITEM(iter, input_option, _options);

      switch (item->_type)
	{
	case INPUT_OPTION_TYPE_STDIN:
	  in_fid = stdin;
	  break;
	case INPUT_OPTION_TYPE_FILE:
	{
	  char *gz = NULL;
	  char *xz = NULL;

	  if ((gz = strstr(item->_name, ".gz")) ||
	      (xz = strstr(item->_name, ".xz")))
	    {
	      size_t sz = strlen(item->_name) + 32;
	      char *cmd = (char *) malloc(sz);
	      if (!cmd)
		FATAL_MEMORY("popen command");
	      if (gz)
		snprintf (cmd, sz, "gunzip -c %s", item->_name);
	      else
		snprintf (cmd, sz, "unxz -d -c %s", item->_name);
	      (void) xz;
	      in_fid = popen(cmd, "r");
	      free(cmd);
	    }
	  else
	    in_fid = fopen(item->_name, "rt");
	  if (in_fid == NULL)
	    {
	      perror("fopen");
	      FATAL("Failed to open input file: %s", item->_name);
	    }
	  break;
	}
	default:
	  BUG("Internal error, input type %d unknown.", item->_type);
	  break;
	}

      INFO("Opened for input: '%s'.", item->_name);

      while ((!ldf && ebye_read_record(fileno(in_fid))) ||
	     ( ldf && ldf_read_record(fileno(in_fid))))
	{
	  /* TODO: Processing is done in read_record().
	   * Move it out of there?
	   */
	}

      /* TODO? - pclose for popen... */
      /* fclose(in_fid); */
    }

  /* Final processing. */
  if (!ldf)
    ebye_records_done();
  else
    ldf_records_done();

  /* Write final data. */
  ebye_flush_output();

  close_ext_data();

  if (_dump_fid)
    fclose(_dump_fid);

  if (_egmw_config._track_pulser)
    pulser_summary();

  if (!ldf)
    ebye_records_summary();
  else
    ldf_records_summary();

  item_summary();
}
