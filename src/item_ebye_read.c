/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#include <stdio.h>
#include <inttypes.h>
#include <string.h>

#include "message.h"
#include "config.h"
#include "item.h"
#include "item_state.h"

#include "ebye_record.h"
#include "hit_out.h"

#define RECENT_INFO_1(code1, high1) \
  (((high1) << 4) | ((code1)))
#define RECENT_INFO_2(code1, high1, code2, high2) \
  ((RECENT_INFO_1(code1, high1) << 8) | \
   (RECENT_INFO_1(code2, high2)     ))

/* This is not the strict checking.  This is checking such that we do
 * not have big jumps, which would mean that despite time sorting, we
 * end up wrong.
 *
 * First check is against what has already been sorted, so somethings
 * actually ended up out-of-order.
 *
 * Second check is a warning that something is (far) out-of-order
 * within a module (or equivalent).
 */
#define CHECK_TS_BACKWARDS(ts, ts_last, ts_far_limit,		\
			   name_fmt, id, extra_id) do {		\
    if (0) {							\
      char name_id[256];					\
      name_fmt(name_id, sizeof (name_id), id, extra_id);	\
      INFO("%s %016" PRIx64 " , %016" PRIx64 "",		\
	   name_id, ts, ts_last._ts);				\
    }								\
    if (ts > ts_last._ts) {					\
      ts_last._ts = ts;						\
      ts_last._id = id;						\
      ts_last._extra_id = extra_id;				\
    }								\
    if (ts < _item_state._ts_sorted_until) {			\
      char name_id[256];					\
      name_fmt(name_id, sizeof (name_id), id, extra_id);	\
      ERROR("%s ts before sorted edge: "			\
	    "%016" PRIx64 " < %016" PRIx64 " : "		\
	    "%" PRId64 "",					\
	    name_id, ts, _item_state._ts_sorted_until,		\
	    (int64_t) (ts - _item_state._ts_sorted_until));	\
    }								\
    else if ((int64_t) ts <					\
	     (int64_t) (ts_last._ts - (ts_far_limit))) {	\
      char name_id[256];					\
      char name_id_last[256];					\
      name_fmt(name_id, sizeof (name_id), id, extra_id);	\
      name_fmt(name_id_last, sizeof (name_id_last),		\
	       ts_last._id, ts_last._extra_id);			\
      ERROR("%s ts far backwards: "				\
	    "%016" PRIx64 " < %016" PRIx64 " (%s) : "		\
	    "%" PRId64 "",					\
	    name_id, ts, ts_last._ts, name_id_last,		\
	    (int64_t) (ts - ts_last._ts));			\
    }								\
  } while (0)

#define STORE_RAW_WORDS(name, w0, w1) do {			\
    if (_item_state._raw_words > MAX_RAW_WORDS - 2) {		\
      ERROR("Accumulated too many raw words at '%s', "		\
	    "already have %d.", name, _item_state._raw_words);	\
    } else {							\
      _item_state._raw[_item_state._raw_words++] = (w0);	\
      _item_state._raw[_item_state._raw_words++] = (w1);	\
    }								\
  } while (0)

#define COPY_RAW_DATA(dest) do {				\
    (dest)->_raw_words = _item_state._raw_words;		\
    memcpy((dest)->_raw, _item_state._raw,			\
	   _item_state._raw_words * sizeof (uint32_t));		\
  } while (0)


int item_read_info(uint32_t *p, uint32_t remain)
{
  uint32_t w0_module = (p[0] >> 24) & 0x3f;
  uint32_t w0_code   = (p[0] >> 20) & 0x0f;
  uint32_t w0_field  = (p[0]      ) & 0x000fffff;

  uint32_t w1_ts     = (p[1]      ) & 0x0fffffff;
  uint32_t w1_high   = (p[1] >> 28) & 0x0f;

  int no_w1_high_check = 0;

  (void) w0_module;

  if (_egmw_config._print_raw_data)
    fprintf (stdout,
	     "%04zd "
	     "INFO:  c%02d:h%02d m%02d 0x%05x @ %07" PRIx32 "\n",
	     (((char *) p) - _record_buffer) / 40,
	     w0_code, w1_high, w0_module, w0_field,
	     w1_ts);

  _item_state._recent_info <<= 8;
  _item_state._recent_info |= ((w1_high << 4) | w0_code);

  if      (w0_code == 5 && w1_high == 1) /* R3B-ASIC */
    {
      _item_state._ts_5_1_hi  = ((uint64_t) w0_field) << 48;
      no_w1_high_check = 1;
    }
  else if ((w0_code == 4 ||
	    w0_code == 14 ||
	    w0_code == 15) && w1_high == 1) /* R3B-ASIC */
    {
      _item_state._ts_x_1_mid = ((uint64_t) w0_field) << 28;
      no_w1_high_check = 1;

      uint64_t ts = _item_state._ts_5_1_hi | _item_state._ts_x_1_mid | w1_ts;

      const char *kind[] = {
	NULL,      NULL,      NULL,      NULL,
	"SYNC100", NULL,      NULL,      NULL,
	NULL,      NULL,      NULL,      NULL,
	NULL,      NULL,      "INFO14",  "INFO15",
      };

      if (_egmw_config._print_raw_data)
	fprintf (stdout,
		 "%04zd "
		 "%s:  %02d             @ %016" PRIx64 ""
		 "\n",
		 (((char *) p) - _record_buffer) / 40,
		 kind[w0_code],
		 w0_module, ts);

      uint32_t accum_id   = (ACCUM_ID_TYPE_ASIC_INFO |
			     (w0_module << 4) | w0_code);

      CHECK_TS_BACKWARDS(ts, _item_state._ts_last._asic_info[w0_module],
			 TS_FAR_BACKWARDS_ASIC_MI,
			 item_name_id, accum_id, -1);

      CHECK_TS_BACKWARDS(ts, _item_state._ts_last._asic[w0_module],
			 TS_FAR_BACKWARDS_ASIC,
			 item_name_id, accum_id, -1);

      /* Consume whatever we parsed so far (before this item). */
      item_got_accum_item();

      /* Keep track of this item. */
      _item_state._accum_id                                        = accum_id;
      _item_state._accum_item._asic_info_raw._asic_info._base._ts  = ts;
      _item_state._accum_item._asic_info_raw._asic_info._base._aux = accum_id;
      _item_state._accum_is_ok                                     = 1;
    }
  else if (w0_code == 7 && w1_high == 1) /* R3B-ASIC */
    {
      _item_state._ts_7_1_mid = ((uint64_t) w0_field) << 28;
      no_w1_high_check = 1;
    }
  else if (w0_code == 7) /* FEBEX */
    {
      _item_state._ts_7_mid = ((uint64_t) w0_field) << 28;
    }
  else if (w0_code == 8) /* FEBEX */
    {
      _item_state._ts_8_hi  = ((uint64_t) w0_field) << 48;
    }
  else if (w0_code == 4) /* CAEN */
    {
      _item_state._ts_4_mid = ((uint64_t) w0_field) << 28;
    }
  else
    {
      ERROR("Unhandled info code (%d) in %08x %08x.",
	    w0_code, p[0], p[1]);

      return 0;
    }

  if (!no_w1_high_check &&
      w1_high)
    {
      ERROR("Unhandled high bits (c%d,h%d) in info w1 in %08x %08x.",
	    w0_code, w1_high, p[0], p[1]);
      return 0;
    }

  STORE_RAW_WORDS("febex", p[0], p[1]);

  /* fprintf (stdout, "Info: %08x %08x\n", p[0], p[1]); */

  (void) w1_ts;

  return 2 * sizeof (uint32_t);
}

int item_read_febex(uint32_t *p, uint32_t remain)
{
  uint32_t w0_pileup   = (p[0] >> 29) & 1;
  uint32_t w0_clip     = (p[0] >> 28) & 1;
  uint32_t w0_sfp      = (p[0] >> 26) & 0x03;
  uint32_t w0_board    = (p[0] >> 22) & 0x0f;
  uint32_t w0_data_idx = (p[0] >> 20) & 0x03;
  uint32_t w0_ch       = (p[0] >> 16) & 0x0f;
  uint32_t w0_adc_data = (p[0]      ) & 0xffff;

  uint32_t w1_ts     = (p[1]      ) & 0x0fffffff;
  uint32_t w1_high   = (p[1] >> 28) & 0x0f;

  uint64_t ts = _item_state._ts_8_hi | _item_state._ts_7_mid | w1_ts;

  if (w1_high != 0)
    {
      ERROR("Unhandled high bits in febex w1 %08x:%08x.",
	    p[0], p[1]);
      return 0;
    }

  if (_egmw_config._print_raw_data)
    fprintf (stdout,
	     "%04zd "
	     "FEBEX: %d/%02d/%02d:%d 0x%04x p%d c%d @ %016" PRIx64 "\n",
	     (((char *) p) - _record_buffer) / 40,
	     w0_sfp, w0_board, w0_ch, w0_data_idx,
	     w0_adc_data, w0_pileup, w0_clip,
	     ts);

  STORE_RAW_WORDS("febex", p[0], p[1]);

  uint32_t accum_id   = (ACCUM_ID_TYPE_FEBEX |
			 (w0_sfp << 8) | (w0_board << 4) | w0_ch);
  uint32_t accum_mask = (1 << w0_data_idx);

  CHECK_TS_BACKWARDS(ts, _item_state._ts_last._febex[w0_sfp * 4 + w0_board],
		     TS_FAR_BACKWARDS_FEBEX,
		     item_name_id, accum_id, w0_data_idx);

#if BUG2023_HANDLING
  /****************************************************************/
  /* Start 2023 bug handling.                                     */
  if (w0_sfp != _bug2023_info._prev_sfp &&
      _bug2023_info._prev_sfp != (uint32_t) -1)
    {
      _bug2023_info._had_sfp_change = 1;
      /* Case b.ii: we are ok from now. */
      if (!_bug2023_info._is_ok)
	WARNING("SFP change -> ok from here");
      _bug2023_info._is_ok = 1;
    }
  if (w0_board < _bug2023_info._prev_board)
    _bug2023_info._had_board_restart = 1;

  _bug2023_info._prev_sfp   = w0_sfp;
  _bug2023_info._prev_board = w0_board;
  /* End 2023 bug handling.                                       */
  /****************************************************************/
#endif

  _record_febex_num_sfp_board[w0_sfp][w0_board]++;

  int sfp_board = (w0_sfp << 8) | (w0_board << 4);

  if (sfp_board < _record_febex_prev_board)
    _record_febex_board_back = 1;

  _record_febex_prev_board = sfp_board;

  if (accum_id == _item_state._accum_id &&
      ts       == _item_state._accum_item._febex_raw._febex._base._ts &&
      !!(accum_mask & _item_state._accum_item._febex_raw._febex._mask))
    {
      ERROR("Same accum id/ts/mask again! "
	    "(0x%03x/%016" PRIx64 "/%x)",
	    accum_id, ts, accum_mask);
      /* return 0; */
    }

  if (accum_id != _item_state._accum_id ||
      ts       != _item_state._accum_item._febex_raw._febex._base._ts ||
      !!(accum_mask & _item_state._accum_item._febex_raw._febex._mask))
    {
      if (accum_id < _item_state._accum_id &&
	  _first_in_record)
	{
	  int j;

	  /*
	    fprintf (stderr, "First in record: @ %016" PRIx64 " (diff %" PRId64 ")\n",
	    ts, ts - _old_first_ts[0]);
	  */

	  for (j = 0; j < 7; j++)
	    _old_first_ts[j] = _old_first_ts[j+1];
	  _old_first_ts[7] = ts;
	}

      /* Consume whatever we parsed so far (before this item). */
      item_got_accum_item();

      _first_in_record = 0;
    }

  /* Keep track of this item. */
  _item_state._accum_id                                = accum_id;
  _item_state._accum_item._febex_raw._febex._base._ts  = ts;
  _item_state._accum_item._febex_raw._febex._base._aux = accum_id;
  _item_state._accum_item._febex_raw._febex._mask     |= accum_mask;
  _item_state._accum_item._febex_raw._febex._adc_data[w0_data_idx] =
    (w0_pileup << 17) | (w0_clip << 16) | w0_adc_data;
#if BUG2023_HANDLING
  _item_state._accum_is_ok                            &= _bug2023_info._is_ok;
#endif

  return 2 * sizeof (uint32_t);
}

int item_read_asic(uint32_t *p, uint32_t remain)
{
  uint32_t w0_pileup   = (p[0] >> 29) & 1;
  uint32_t w0_module   = (p[0] >> 23) & 0x3f;
  uint32_t w0_asic     = (p[0] >> 19) & 0x0f;
  uint32_t w0_ch       = (p[0] >> 12) & 0x7f;
  uint32_t w0_adc_data = (p[0]      ) & 0xfff;

  uint32_t w1_ts     = (p[1]      ) & 0x0fffffff;
  uint32_t w1_high   = (p[1] >> 28) & 0x0f;

  uint64_t ts = _item_state._ts_5_1_hi | _item_state._ts_7_1_mid | w1_ts;

  if (w1_high != 1)
    {
      ERROR("Unhandled high bits in asic w1 %08x:%08x.",
	    p[0], p[1]);
      return 0;
    }

  if (_egmw_config._print_raw_data)
    fprintf (stdout,
	     "%04zd "
	     "ASIC: %02d/%02d/%03d 0x%03x p%d @ %016" PRIx64 ""
	     "\n",
	     (((char *) p) - _record_buffer) / 40,
	     w0_module, w0_asic, w0_ch,
	     w0_adc_data, w0_pileup,
	     ts);

  STORE_RAW_WORDS("asic", p[0], p[1]);

  uint32_t accum_id   = (ACCUM_ID_TYPE_ASIC |
			 (w0_module << 11) | (w0_asic << 7) | w0_ch);

  CHECK_TS_BACKWARDS(ts,
		     _item_state._ts_last._asic_mod[w0_module * 16 + w0_asic],
		     TS_FAR_BACKWARDS_ASIC_MI,
		     item_name_id, accum_id, 0);

  CHECK_TS_BACKWARDS(ts,
		     _item_state._ts_last._asic[w0_module],
		     TS_FAR_BACKWARDS_ASIC,
		     item_name_id, accum_id, 0);

  _record_asic_num_asic_module[w0_module][w0_asic]++;

  /*
  int module_asic = (w0_asic << 8) | (w0_module << 4);

  if (sfp_board < _record_asic_prev_module)
    _record_asic_board_back = 1;

  _record_asic_prev_module = sfp_board;
  */

  /* We do not have multiple data items, so always is a new one. */

  /* Consume whatever we parsed so far (before this item). */
  item_got_accum_item();

  /* Keep track of this item. */
  _item_state._accum_id                              = accum_id;
  _item_state._accum_item._asic_raw._asic._base._ts  = ts;
  _item_state._accum_item._asic_raw._asic._base._aux = accum_id;
  _item_state._accum_item._asic_raw._asic._adc_data  =
    (w0_pileup << 17) | w0_adc_data;
  _item_state._accum_is_ok                           = 1;

  return 2 * sizeof (uint32_t);
}

int item_read_caen(uint32_t *p, uint32_t remain)
{
  uint32_t w0_pileup   = (p[0] >> 29) & 1;
  uint32_t w0_module   = (p[0] >> 24) & 0x1f;
  uint32_t w0_data_idx = (p[0] >> 22) & 0x03;
  uint32_t w0_ch       = (p[0] >> 16) & 0x3f;
  uint32_t w0_adc_data = (p[0]      ) & 0xffff;

  uint32_t w1_ts     = (p[1]      ) & 0x0fffffff;
  uint32_t w1_high   = (p[1] >> 28) & 0x0f;

  uint64_t ts = _item_state._ts_4_mid | w1_ts;

  if (w1_high != 0)
    {
      ERROR("Unhandled high bits in caen w1 %08x:%08x.",
	    p[0], p[1]);
      return 0;
    }

  if (_egmw_config._synthetic_data_check)
    {
      /* Per channel (and data_idx) we keep knowledge of a checksum of
       * the previous hit.  This is encoded in the low 2 bits of the
       * present hit.
       */

      uint8_t *p_check = &_synthetic_caen_check[w0_module][w0_ch][w0_data_idx];

      if ((w0_adc_data & 0x0003) != *p_check)
	{
	  ERROR("%04zd "
		"CAEN: %02d/%02d:%d 0x%04x p%d @ %016" PRIx64 "  "
		"check: got %d != expect %d",
		(((char *) p) - _record_buffer) / 40,
		w0_module, w0_ch, w0_data_idx,
		w0_adc_data, w0_pileup,
		ts,
		w0_adc_data & 0x0003, *p_check);

	  _synthetic_data_check_bad++;
	}

      _synthetic_data_check_count++;

      /* Since the checksum value is part of the data, we can just
       * calculate a new one...
       */

      *p_check = synth_check_caen(ts, w0_adc_data, w0_pileup);

      if (_synthetic_data_check_count % 1000 == 0)
	{
	  fprintf(stdout,
		  "Synthetic checks: bad %" PRIu64 " / %" PRIu64 "   \r",
		  _synthetic_data_check_bad,
		  _synthetic_data_check_count);
	  fflush(stdout);
	}
    }

  /* CAEN item timestamps are in units of 4 ns.
   * TODO: 1730 in units of 2 ns.
   */

  ts *= 4;
  /*
  if (ts > _item_state._ts_last._caen[w0_module]._ts)
    _item_state._ts_last._caen[w0_module]._ts = ts;
  */
  if (_egmw_config._print_raw_data)
    fprintf (stdout,
	     "%04zd "
	     "CAEN: %02d/%02d:%d 0x%04x p%d @ %016" PRIx64 "\n",
	     (((char *) p) - _record_buffer) / 40,
	     w0_module, w0_ch, w0_data_idx,
	     w0_adc_data, w0_pileup,
	     ts);

  STORE_RAW_WORDS("caen", p[0], p[1]);

  uint32_t accum_id   = (ACCUM_ID_TYPE_CAEN |
			 (w0_module << 6) | w0_ch);
  uint32_t accum_mask = (1 << w0_data_idx);

  CHECK_TS_BACKWARDS(ts, _item_state._ts_last._caen[w0_module],
		     TS_FAR_BACKWARDS_CAEN,
		     item_name_id, accum_id, w0_data_idx);

  CHECK_TS_BACKWARDS(ts, _item_state._ts_last._caen_grp[w0_module / 16],
		     TS_FAR_BACKWARDS_CAEN,
		     item_name_id, accum_id, w0_data_idx);

  _record_caen_num_module[w0_module]++;
  /*
  int sfp_board = (w0_sfp << 8) | (w0_board << 4);

  if (sfp_board < _record_febex_prev_board)
    _record_febex_board_back = 1;

  _record_febex_prev_board = sfp_board;
  */

  if (accum_id == _item_state._accum_id &&
      ts       == _item_state._accum_item._caen_raw._caen._base._ts &&
      !!(accum_mask & _item_state._accum_item._caen_raw._caen._mask))
    {
      ERROR("Same accum id/ts/mask again! "
	    "(0x%03x/%016" PRIx64 "/%x)",
	    accum_id, ts, accum_mask);
      /* return 0; */
    }

  if (accum_id != _item_state._accum_id ||
      ts       != _item_state._accum_item._caen_raw._caen._base._ts ||
      !!(accum_mask & _item_state._accum_item._caen_raw._caen._mask))
    {
      /* Consume whatever we parsed so far (before this item). */
      item_got_accum_item();

      _first_in_record = 0;
    }

  /* Keep track of this item. */
  _item_state._accum_id                              = accum_id;
  _item_state._accum_item._caen_raw._caen._base._ts  = ts;
  _item_state._accum_item._caen_raw._caen._base._aux = accum_id;
  _item_state._accum_item._caen_raw._caen._mask     |= accum_mask;
  _item_state._accum_item._caen_raw._caen._adc_data[w0_data_idx] =
    (w0_pileup << 17) | w0_adc_data;
  _item_state._accum_is_ok                           = 1;

  return 2 * sizeof (uint32_t);
}

int item_read_caen_trace(uint32_t *p, uint32_t remain)
{
  uint32_t w0_ch       = (p[0] >> 16) & 0xfff;
  uint32_t w0_length   = (p[0]      ) & 0xffff;

  uint32_t w1_ts     = (p[1]      ) & 0x0fffffff;
  uint32_t w1_high   = (p[1] >> 28) & 0x0f;

  uint64_t ts = _item_state._ts_4_mid | w1_ts;

  uint32_t use;

  if (w1_high != 0)
    {
      ERROR("Unhandled high bits in caen trace w1 %08x:%08x.",
	    p[0], p[1]);
      return 0;
    }

  /* CAEN item timestamps are in units of 4 ns.
   * TODO: 1730 in units of 2 ns.
   */

  ts *= 4;

  if (_egmw_config._print_raw_data)
    fprintf (stdout,
	     "%04zd "
	     "CAEN trace: %04d len:%5d\n",
	     (((char *) p) - _record_buffer) / 40,
	     w0_ch, w0_length);

  if (w0_length & 0x03)
    {
      ERROR("Trace length 0x%04x=%5d not a multiple of 4.",
	    w0_length, w0_length);
      return 0;
    }

  /* TODO: All handling to retain data!! */

  (void) ts;

  use = 2 * sizeof (uint32_t) + ((w0_length + 3) / 4) * 2 * sizeof (uint32_t);

  if (remain < use)
    {
      ERROR("Trace uses more data (%d) than left (%d).",
	    use, remain);
      return 0;
    }

  return use;
}

int item_ebye_read(uint32_t *p, uint32_t remain)
{
  uint32_t w0_type;
  uint32_t w0_type4;
  uint32_t prev_type;
  uint32_t recent_info;

  /* All items are to have two words, so we check that already here. */
  if (remain < 2 * sizeof (uint32_t))
    {
      ERROR("Non-zero (%d), but less than minimum item size left.", remain);
      return 0;
    }

  w0_type  = (p[0] >> 30) & 0x03;
  w0_type4 = (p[0] >> 28) & 0x0f;

  /* Make a copy to use. */
  prev_type = _item_state._prev_type;
  /* Current item is not an info item, so reset recent info state. */
  _item_state._prev_type = w0_type;

  if (w0_type == 0x2) /* info */
    {
      /* Whenever we get an info item after something else,
       * we close what we got.
       */
      if (prev_type != 0x2)
	item_got_accum_item();

      return item_read_info(p, remain);
    }

  /* Make a copy to use. */
  recent_info = _item_state._recent_info;
  /* Current item is not an info item, so reset recent info state. */
  _item_state._recent_info = 0;

  if (w0_type == 0x3) /* adc (febex) */
    {
      if (_egmw_config._print_raw_data && 0)
	fprintf (stdout,
		 "%04zd "
		 "INFO-recent: %d:%d %d:%d\n",
		 (((char *) p) - _record_buffer) / 40,
		 (recent_info >>  8) & 0x0f,
		 (recent_info >> 12) & 0x0f,
		 (recent_info >>  0) & 0x0f,
		 (recent_info >>  4) & 0x0f);

      if ((recent_info & 0xffff) == RECENT_INFO_2(7, 0, 8, 0))
	{
	  _item_state._recent_info = recent_info;
	  return item_read_febex(p, remain);
	}

      if ((recent_info & 0xffff) == RECENT_INFO_2(5, 1, 7, 1))
	{
	  _item_state._recent_info = recent_info;
	  return item_read_asic(p, remain);
	}

      if ((recent_info & 0xff) == RECENT_INFO_1(4, 0))
	{
	  _item_state._recent_info = recent_info;
	  return item_read_caen(p, remain);
	}
    }

  if (w0_type4 == 0x4) /* trace, type 0100 */
    {
      if ((recent_info & 0xff) == RECENT_INFO_1(4, 0))
	{
	  _item_state._recent_info = recent_info;
	  return item_read_caen_trace(p, remain);
	}
    }

  ERROR("Unhandled item type %d in %08x with recent info "
	"(c%d,h%d, c%d,h%d).",
	w0_type, p[0],
	(recent_info >>  8) & 0x0f,
	(recent_info >> 12) & 0x0f,
	(recent_info >>  0) & 0x0f,
	(recent_info >>  4) & 0x0f);
  return 0;
}

void item_ebye_read_end(void)
{
  /* Consume whatever we parsed until now. */
  item_got_accum_item();
}

/* Can be called from accum_insert_item(). */
void item_report_ins_backwards(uint32_t type, uint32_t id,
			       lwroc_accum_item_base *item,
			       uint64_t last_ts)
{
  char name_id[256];

  item_name_id(name_id, sizeof (name_id), type | id, -1);

  ERROR("Insert timestamp backwards for %s "
	"(%016" PRIx64 " - %016" PRIx64 " = %" PRId64 ")  "
	"%s",
	name_id,
	item->_ts, last_ts,
	(int64_t) (item->_ts - last_ts),
#if BUG2023_HANDLING
	_bug2023_info._is_ok ? "CUR-OK-wrong!" : "not-ok"
#else
	""
#endif
	);

  _record_febex_had_backward = 1;
}

void item_got_accum_item(void)
{
  if (_item_state._accum_id == (uint32_t) -1)
    return;

  if (_item_state._accum_item._base._ts > _item_state._ts_max_seen)
    _item_state._ts_max_seen = _item_state._accum_item._base._ts;

  if (_dump_fid)
    fprintf(_dump_fid,
	    "%4d %d/%02d/%02d %016" PRIx64 "\n",
	    _item_state._accum_id,
	    (_item_state._accum_id >> 8) & 0x03,
	    (_item_state._accum_id >> 4) & 0x0f,
	    (_item_state._accum_id     ) & 0x0f,
	    _item_state._accum_item._febex_raw._febex._base._ts);

  /* Only add non-broken items. */
  if (_item_state._accum_is_ok)
    {
      switch (_item_state._accum_id & ACCUM_ID_TYPE_MASK)
	{
	case ACCUM_ID_TYPE_FEBEX:
	  COPY_RAW_DATA(&_item_state._accum_item._febex_raw._raw);
	  if (_egmw_config._print_hit_data)
	    print_febex_item(_item_state._accum_id & ~ACCUM_ID_TYPE_MASK,
			     &_item_state._accum_item._febex_raw._febex,
			     PRINT_HIT_PREFIX);
	  if (_egmw_config._accum_sort)
	    lwroc_accum_insert_item(_accum_febex,
				    _item_state._accum_id & ~ACCUM_ID_TYPE_MASK,
				    &_item_state._accum_item._febex_raw._febex._base,
				    sizeof (_item_state._accum_item._febex_raw));
	  break;
	case ACCUM_ID_TYPE_ASIC:
	  COPY_RAW_DATA(&_item_state._accum_item._asic_raw._raw);
	  if (_egmw_config._print_hit_data)
	    print_asic_item(_item_state._accum_id & ~ACCUM_ID_TYPE_MASK,
			    &_item_state._accum_item._asic_raw._asic,
			    PRINT_HIT_PREFIX);
	  if (_egmw_config._accum_sort)
	    lwroc_accum_insert_item(_accum_asic,
				    _item_state._accum_id & ~ACCUM_ID_TYPE_MASK,
				    &_item_state._accum_item._asic_raw._asic._base,
				    sizeof (_item_state._accum_item._asic_raw));
	  break;
	case ACCUM_ID_TYPE_ASIC_INFO:
	  COPY_RAW_DATA(&_item_state._accum_item._asic_info_raw._raw);
	  if (_egmw_config._print_hit_data)
	    print_asic_info_item(_item_state._accum_id & ~ACCUM_ID_TYPE_MASK,
				 &_item_state._accum_item._asic_info_raw._asic_info,
				 PRINT_HIT_PREFIX);
	  if (_egmw_config._accum_sort)
	    lwroc_accum_insert_item(_accum_asic_info,
				    _item_state._accum_id & ~ACCUM_ID_TYPE_MASK,
				    &_item_state._accum_item._asic_info_raw._asic_info._base,
				    sizeof (_item_state._accum_item._asic_info_raw));
	  break;
	case ACCUM_ID_TYPE_CAEN:
	  COPY_RAW_DATA(&_item_state._accum_item._caen_raw._raw);
	  if (_egmw_config._print_hit_data)
	    print_caen_item(_item_state._accum_id & ~ACCUM_ID_TYPE_MASK,
			    &_item_state._accum_item._caen_raw._caen,
			    PRINT_HIT_PREFIX);
	  if (_egmw_config._accum_sort)
	    lwroc_accum_insert_item(_accum_caen,
				    _item_state._accum_id & ~ACCUM_ID_TYPE_MASK,
				    &_item_state._accum_item._caen_raw._caen._base,
				    sizeof (_item_state._accum_item._caen_raw));
	  break;
	default:
	  ERROR("Unhandled accum_id: %08x",
		_item_state._accum_id);
	  break;
	}
    }

  /* item_handled: */
  _item_state._accum_id                    = (uint32_t) -1;
  memset(&_item_state._accum_item, 0, sizeof (_item_state._accum_item));
  _item_state._accum_is_ok                 = 1;

  _item_state._raw_words = 0;
}
