/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#include <stdio.h>
#include <inttypes.h>

#include "message.h"
#include "config.h"
#include "item.h"
#include "item_state.h"
#include "accum_sort.h"
#include "ext_data.h"

#include "inc_colourtext.h"

void item_process_hb_common_pre(uint32_t id, lwroc_accum_item_base *item,
				void *arg)
{
  (void) id;
  (void) item;
  (void) arg;

  /* printf ("pre: %016" PRIx64 "\n", item->_ts); */
}

void item_process_hb_common_post(uint32_t id, lwroc_accum_item_base *item,
				 void *arg)
{
  (void) id;
  (void) item;
  (void) arg;
}

void item_process_hb_ELUM_item(uint32_t type, uint32_t id,
			       lwroc_accum_item_base *item,
			       void *arg)
{
  item_process_hb_common_pre(id, item, arg);

  item_process_hb_common_post(id, item, arg);
}

void item_process_hb_LUME_item(uint32_t type, uint32_t id,
			       lwroc_accum_item_base *item,
			       void *arg)
{
  item_process_hb_common_pre(id, item, arg);

  item_process_hb_common_post(id, item, arg);
}

void item_process_hb_ISIA_item(uint32_t type, uint32_t id,
			       lwroc_accum_item_base *item,
			       void *arg)
{
  item_process_hb_common_pre(id, item, arg);

  item_process_hb_common_post(id, item, arg);
}

void item_process_hb_CD_item(uint32_t type, uint32_t id,
			     lwroc_accum_item_base *item,
			     void *arg)
{
  item_process_hb_common_pre(id, item, arg);

  item_process_hb_common_post(id, item, arg);
}

void item_process_hb_REC_item(uint32_t type, uint32_t id,
			      lwroc_accum_item_base *item,
			      void *arg)
{
  item_process_hb_common_pre(id, item, arg);

  item_process_hb_common_post(id, item, arg);
}

void item_process_hb_ZD_item(uint32_t type, uint32_t id,
			     lwroc_accum_item_base *item,
			     void *arg)
{
  item_process_hb_common_pre(id, item, arg);

  item_process_hb_common_post(id, item, arg);
}

void item_process_hb_MWPC_item(uint32_t type, uint32_t id,
			       lwroc_accum_item_base *item,
			       void *arg)
{
  item_process_hb_common_pre(id, item, arg);

  item_process_hb_common_post(id, item, arg);
}
