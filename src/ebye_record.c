/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <byteswap.h>
#include <arpa/inet.h>

#include "ebye_record_header.h"
#include "ebye_xfer_header.h"

#include "message.h"
#include "config.h"
#include "ebye_record.h"
#include "item.h"
#include "io_util.h"

#define BUFFER_LENGTH 0x10000

extern int _first_in_record;

int _prev_record_not_full = 0;

/* Broken record data identification:
 *
 * A record is ok:
 *
 * a) If the previous record was not full.
 *
 * b) If the previous record contained data for different SFPs.
 *    (Data inside a record after SFP change is ok.)
 *
 * c) If the previous record was full, but itself 'ok' and contained
 *    board number jumps backward.
 *
 * Notes:
 *
 * Case a) occurs since this is the original error itself: only data
 * that follows a full record can be/is broken.
 *
 * Case b) occurs since the tape server (data writing; a separate
 * process after the readout=rewriter) will merge records that are
 * small enough such that the combined data fits in one record.  Since
 * two records were merged, each of them was not full, and thus the
 * error cannot have occured.
 *
 * Note: case b) relies on the fact that SFP numbers are correct even
 * if the remaining data may be corrupt.
 *
 * Case c) is a special case of b), but with the same SFP involved.
 * In this case, it can be detected that originally two records were
 * involved by the fact that the board numbers have restarted.  This
 * must however only be considered if the record itself is good, since
 * broken records contain unreliable data, e.g. board numbers that
 * could jump more arbitrarily.
 */

/* Broken record tracking:
 *
 * - Start out by assuming the (first) record to not be ok.
 *
 * - Keep a variable that holds the current 'ok' status.
 *
 * - At the end of a record which is not full (case a): set as => ok.
 *
 * - At the end of a record that is full:
 *
 *   - Contained SFP changes (case b): set as => ok.
 *
 *   - Is itself ok and had SFP restart (= board number jumping back)
 *     (case c): set as => ok.
 *
 *   - Else: set as => NOT ok.
 *
 * - During record processing:
 *
 *   - In case of SFP change (case b.ii): set as => ok.
 *     (Applies to remaining data in record.)
 */

fullbug2023_state _bug2023_info =
  {
    0, /* Start in NOT ok state. */
    0, /* Will be initialised. */
    0, /* Will be initialised. */
    0, /* Will be initialised. */
    0, /* Will be initialised. */
    0, /* Will be initialised. */
  };

int ebye_read_record_items(char *p, uint32_t remain)
{
  int ret = 1;

  _record_febex_had_backward = 0;
  _record_febex_prev_board = -1;
  _record_febex_board_back = 0;

  _first_in_record = 1;

  _record_buffer = p;

  memset(_record_febex_num_sfp_board, 0,
	 sizeof (_record_febex_num_sfp_board));
  memset(_record_asic_num_asic_module, 0,
	 sizeof (_record_asic_num_asic_module));
  memset(_record_caen_num_module, 0,
	 sizeof (_record_caen_num_module));

  while (remain)
    {
      uint32_t used = item_ebye_read((uint32_t *) p, remain);

      if (used == 0)
	{
	  /* Error occured. */
	  ret = 0;
	  break;
	}

      if (used > remain)
	{
	  FATAL("Internal item_read error, used (%d) > remain (%d).",
		used, remain);
	}

      p      += used;
      remain -= used;
    }

  item_ebye_read_end();

  return ret;
}

typedef struct record_xfer_info_t
{
  uint32_t _header_length;
  uint32_t _block_length;
  uint32_t _data_length;

  uint32_t _sequence;

  int      _swap_data;
} record_xfer_info;

int handle_ebye_record_header(ebye_record_header *record,
			      record_xfer_info *info)
{
  if (record->_endian_tape != 0x0001)
    {
      if (record->_endian_tape != 0x0100)
	{
	  ERROR("Unknown record header (tape) endianess: %04x.",
		record->_endian_tape);
	  return 0;
	}

      record->_sequence    = __bswap_32(record->_sequence);
      record->_tape        = __bswap_16(record->_tape);
      record->_stream      = __bswap_16(record->_stream);
      record->_data_length = __bswap_32(record->_data_length);
    }

  if (record->_endian_data != 0x0001)
    {
      if (record->_endian_data != 0x0100)
        {
          ERROR("Unknown record header (data) endianess: %04x.",
		record->_endian_data);
          return 0;
        }

      info->_swap_data = 1;
    }

  if (record->_data_length + sizeof (*record) > BUFFER_LENGTH)
    {
      ERROR("Unhandled data length (too long): %d > %d.",
	    record->_data_length, BUFFER_LENGTH);
      return 0;
    }

  info->_header_length = sizeof (*record);
  info->_block_length  = BUFFER_LENGTH;
  info->_data_length   = record->_data_length;

  info->_sequence      = record->_sequence;

  return 1;
}

/* The resize messages implicity uses the previously set block length. */
/* For the first, it is 1024 bytes. */
uint32_t _prev_block_length = 1024;

int handle_xfer_header(ebye_xfer_header *xfer,
		       record_xfer_info *info)
{
  uint32_t block_length;

  if (xfer->_endian != 0x0001)
    {
      if (xfer->_endian != 0x0100)
	{
	  ERROR("Unknown (xfer) header endianess: %04x.",
		xfer->_endian);
	  return 0;
	}
    }

  block_length       = ntohl(xfer->_block_length);
  info->_data_length = ntohl(xfer->_data_length);

  if (block_length < 1024 ||
      block_length > BUFFER_LENGTH)
    {
      ERROR("Unhandled block length: "
	    "%d outside [%d,%d].",
	    block_length,
	    1024, BUFFER_LENGTH);
      return 0;
    }

  if (info->_data_length == (uint32_t) -1)
    {
      /* A (re)start buffer.  Used to tell the block_length of
       * following buffers.  (We do not care; are adaptive).
       */
      info->_data_length  = 0;
      info->_block_length = _prev_block_length;

      _prev_block_length = block_length;
    }
  else
    {
      info->_block_length = block_length;

      if (info->_block_length != _prev_block_length)
	{
	  /* This is just a warning, but most likely we will be out of
	   * sync after this block.
	   */
	  WARNING("xfer block length changed in header: "
		  "%d != %d.",
		  info->_block_length,
		  _prev_block_length);
	}
    }

  if (info->_data_length + sizeof (*xfer) > info->_block_length)
    {
      ERROR("Unhandled xfer data length (longer than block): "
	    "%d > %d.",
	    info->_data_length,
	    info->_block_length);
      return 0;
    }

  if (xfer->_endian != 0x0001)
    {
      if (xfer->_endian != 0x0100)
        {
          ERROR("Unknown xfer data endianess: %04x.",
		xfer->_endian);
          return 0;
        }

      info->_swap_data = 1;
    }

  info->_header_length = sizeof (*xfer);

  info->_sequence      = ntohl(xfer->_sequence);

  return 1;
}

typedef union ebye_union_header_t
{
  ebye_record_header record;
  ebye_xfer_header   xfer;
} ebye_union_header;

int ebye_read_record(int fd)
{
  ebye_union_header h;
  record_xfer_info   info;
  char buffer[BUFFER_LENGTH];
  char *p;
  uint32_t remain;

  memset(&info, 0, sizeof (info));

  if (!full_read(fd, &h.record, sizeof (h.record)))
    {
      /* ERROR("Failed to read record header."); */
      return 0;
    }

  if (memcmp(&h.record._id, EBYE_RECORD_HEADER_ID, sizeof (h.record._id)) == 0)
    {
      if (!handle_ebye_record_header(&h.record, &info))
	return 0;
    }
  /* Else: see if it is an XFER header.
   *
   * They of course had to put those magic numbers after the end
   * of the record header...  So we first need to read.
   *
   * Plain acceptance of id1/2 as just 0 is too weak!  Therefore also
   * checking that the endianess marker at least is sane.
   */
  else if (full_read(fd, &h.xfer._id1, 2 * sizeof (h.xfer._id1)) &&
	   ((ntohl(h.xfer._id1) == EBYE_XFER_HEADER_ID1 &&
	     ntohl(h.xfer._id2) == EBYE_XFER_HEADER_ID2) ||
	    (ntohl(h.xfer._id1) == 0 &&
	     ntohl(h.xfer._id2) == 0)) &&
	   (h.xfer._endian == 0x0001 ||
	    h.xfer._endian == 0x0100))
    {
      /* INFO("Found XFER header."); */

      if (!handle_xfer_header(&h.xfer, &info))
	return 0;
    }
  else
    {
      ERROR("Bad record ID, expected 'EBYEDATA' or XFER ID1,ID2.");
      return 0;
    }

  if (info._sequence != _record_sequence + 1 &&
      _record_sequence != (uint32_t) -1 &&
      info._sequence != 0) /* SHM apparently does give seq 0 all the time. */
    {
      WARNING("Unexpected sequence number, "
	      "got %d, expected %d (jump %d).",
	      info._sequence,
	      _record_sequence + 1,
	      info._sequence - (_record_sequence + 1));
    }

  _record_sequence = info._sequence;

  if (!full_read(fd, &buffer, info._block_length - info._header_length))
    {
      ERROR("Failed to read record data: %d.",
	    (int) (info._block_length - info._header_length));
      return 0;
    }

  if (info._swap_data)
    {

      p = buffer;
      remain = info._data_length;

      while (remain >= sizeof (uint32_t))
	{
	  uint32_t *p32 = (uint32_t *) p;

	  *p32 = __bswap_32(*p32);

	  p      += sizeof (uint32_t);
	  remain -= sizeof (uint32_t);
	}

      /*
      for (size_t i = 0; i < record._data_length; i += 4)
	{
	  INFO("%08x", buffer[i]);
	}
      */
    }

  /* Handling of bug, reset record-internal tracking. */
  _bug2023_info._ok_at_start       = _bug2023_info._next_is_ok;
  _bug2023_info._had_sfp_change    = 0;
  _bug2023_info._had_board_restart = 0;
  _bug2023_info._prev_sfp   = (uint32_t) -1;
  _bug2023_info._prev_board = 0;

  /* Set here, in case unpacking is aborted. */
  _bug2023_info._next_is_ok = 0;

  if (_egmw_config._print_record)
    {
      fprintf (stderr, "==================================================\n");

      if (_egmw_config._print_record)
	fprintf (stderr,
		 "Record: %10d  %6d"
		 /*"  %5d %5d  %3d %3d"*/
		 "               %11s"
		 "\n",
		 info._sequence,
		 info._data_length/*,
		 record._tape,
		 record._stream,
		 record._endian_tape,
		 record._endian_data*/,
		 _bug2023_info._ok_at_start ? "ok-at-start" : "NOT-OK");
    }

  if (_dump_fid)
    fprintf(_dump_fid,
	    "==== %10d  %6d\n",
	    info._sequence,
	    info._data_length);

  message_rate_limit_begin();

  if (!ebye_read_record_items(buffer, info._data_length))
    {
      ERROR("Failed to read record items.");
      return 0;
    }

  item_process_items();

  message_rate_limit_end();

  _records_febex_with_backward += _record_febex_had_backward;
  _records_read++;

  if (_egmw_config._print_record)
    {
      int sfp, board, asic, module;

      for (sfp = 0; sfp < 4; sfp++)
	{
	  for (board = 0; board < 16; board++)
	    if (_record_febex_num_sfp_board[sfp][board])
	      goto has_febex_sfp;

	  continue;
	has_febex_sfp:
	  fprintf (stderr, "SFP: %2d :", sfp);
	  for (board = 0; board < 16; board++)
	    fprintf (stderr, " %3d", _record_febex_num_sfp_board[sfp][board]);
	  fprintf (stderr, "\n");
	}

      for (asic = 0; asic < 64; asic++)
	{
	  for (module = 0; module < 16; module++)
	    if (_record_asic_num_asic_module[asic][module])
	      goto has_asic_module;

	  continue;
	has_asic_module:
	  fprintf (stderr, "asic: %2d :", asic);
	  for (module = 0; module < 16; module++)
	    fprintf (stderr, " %3d", _record_asic_num_asic_module[asic][module]);
	  fprintf (stderr, "\n");
	}

      do
	{
	  for (module = 0; module < 32; module++)
	    if (_record_caen_num_module[module])
	      goto has_caen_module;

	  break;
	has_caen_module:
	  fprintf (stderr, "caen: ");
	  for (module = 0; module < 32; module++)
	    fprintf (stderr, " %3d", _record_caen_num_module[module]);
	  fprintf (stderr, "\n");
	}
      while (0);

      fprintf (stderr,
	       "Record: %10d  %7s  %10s  %4s  %4s\n",
	       info._sequence,
	       _prev_record_not_full ? "ok" : "suspect",
	       _record_febex_board_back ? "board-back" : "-",
	       _bug2023_info._had_sfp_change ? "cSFP" : "-",
	       _bug2023_info._had_board_restart ? "<brd" : "-");
    }

  /* Handling of bug, evaluate status for use at start of next record. */
  if (info._data_length < BUG2023_RECORD_FULL_LIMIT)
    {
      _bug2023_info._next_is_ok = 1; /* Case a. */
    }
  else
    {
      if (_bug2023_info._had_sfp_change)
	_bug2023_info._next_is_ok = 1; /* Case b. */
      else if (_bug2023_info._ok_at_start &&
	       _bug2023_info._had_board_restart)
	_bug2023_info._next_is_ok = 1; /* Case c. */
      else
	_bug2023_info._next_is_ok = 0; /* Already set above. */
    }

  _prev_record_not_full = (info._data_length < BUG2023_RECORD_FULL_LIMIT);

  return 1;
}

void ebye_records_done(void)
{
  message_rate_limit_begin();

  item_process_remaining();

  message_rate_limit_end();
}

void ebye_records_summary(void)
{
  INFO("Backwards: %d / %d = %.1f %%",
       _records_febex_with_backward, _records_read,
       (_records_febex_with_backward * 100.) / _records_read);
}
