/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#ifndef __EXT_DATA_H__
#define __EXT_DATA_H__

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef EXT_STRUCT_CTRL
# define EXT_STRUCT_CTRL(x)
#endif

/* The arrays are used when event-building has been done.
 * They thus tell how many hits we handle within one event.
 *
 * Suggestion: 2x number of channels becomes excessive for the ASIC, but
 * is otherwise acceptable.
 */
#define MAX_EXT_FEBEX_HITS  1024
#define MAX_EXT_ASIC_HITS   1024
#define MAX_EXT_CAEN_HITS   1024
#define MAX_EXT_PIXIE_HITS  1024

/* Todo: script that parses ext_data structure, and ext_data interface
 * should be made able to handle uint64 entries, such that the _l and _h
 * suffixes can be removed.
 *
 * Todo: improve the parser such that the _a _b _c _d suffixes can be
 * turned back into an (outer level) array.  Introduced in order to
 * handle multiple hits.
 */
struct ext_data
{
  uint32_t _febex_n;
  uint32_t _febex_id   [MAX_EXT_FEBEX_HITS  EXT_STRUCT_CTRL(_febex_n)];
  uint32_t _febex_ts_h [MAX_EXT_FEBEX_HITS  EXT_STRUCT_CTRL(_febex_n)];
  uint32_t _febex_ts_l [MAX_EXT_FEBEX_HITS  EXT_STRUCT_CTRL(_febex_n)];
  uint32_t _febex_adc_a[MAX_EXT_FEBEX_HITS  EXT_STRUCT_CTRL(_febex_n)];
  uint32_t _febex_adc_b[MAX_EXT_FEBEX_HITS  EXT_STRUCT_CTRL(_febex_n)];
  uint32_t _febex_adc_c[MAX_EXT_FEBEX_HITS  EXT_STRUCT_CTRL(_febex_n)];
  uint32_t _febex_adc_d[MAX_EXT_FEBEX_HITS  EXT_STRUCT_CTRL(_febex_n)];

  uint32_t _asic_n;
  uint32_t _asic_id  [MAX_EXT_ASIC_HITS  EXT_STRUCT_CTRL(_asic_n)];
  uint32_t _asic_ts_h[MAX_EXT_ASIC_HITS  EXT_STRUCT_CTRL(_asic_n)];
  uint32_t _asic_ts_l[MAX_EXT_ASIC_HITS  EXT_STRUCT_CTRL(_asic_n)];
  uint32_t _asic_adc [MAX_EXT_ASIC_HITS  EXT_STRUCT_CTRL(_asic_n)];

  uint32_t _caen_n;
  uint32_t _caen_id   [MAX_EXT_CAEN_HITS  EXT_STRUCT_CTRL(_caen_n)];
  uint32_t _caen_ts_h [MAX_EXT_CAEN_HITS  EXT_STRUCT_CTRL(_caen_n)];
  uint32_t _caen_ts_l [MAX_EXT_CAEN_HITS  EXT_STRUCT_CTRL(_caen_n)];
  uint32_t _caen_adc_a[MAX_EXT_CAEN_HITS  EXT_STRUCT_CTRL(_caen_n)];
  uint32_t _caen_adc_b[MAX_EXT_CAEN_HITS  EXT_STRUCT_CTRL(_caen_n)];
  uint32_t _caen_adc_c[MAX_EXT_CAEN_HITS  EXT_STRUCT_CTRL(_caen_n)];
  uint32_t _caen_adc_d[MAX_EXT_CAEN_HITS  EXT_STRUCT_CTRL(_caen_n)];

  uint32_t _pixie_n;
  uint32_t _pixie_id  [MAX_EXT_PIXIE_HITS  EXT_STRUCT_CTRL(_pixie_n)];
  uint32_t _pixie_ts_h[MAX_EXT_PIXIE_HITS  EXT_STRUCT_CTRL(_pixie_n)];
  uint32_t _pixie_ts_l[MAX_EXT_PIXIE_HITS  EXT_STRUCT_CTRL(_pixie_n)];
  uint32_t _pixie_adc [MAX_EXT_PIXIE_HITS  EXT_STRUCT_CTRL(_pixie_n)];
  uint32_t _pixie_tfr [MAX_EXT_PIXIE_HITS  EXT_STRUCT_CTRL(_pixie_n)];
};

/* Structure holding one hit/event.
 *
 * Filled by this code.
 * Transmitted by send_ext_data().
 */
extern struct ext_data _ext_data;

extern int _ext_data_filled;

void usage_ext_data(void);

int match_ext_data_arg(const char *arg);

void init_ext_data(const char *argv0);

void send_ext_data(void);

void close_ext_data(void);

#ifdef __cplusplus
}
#endif

#endif/*__EXT_DATA_H__*/
