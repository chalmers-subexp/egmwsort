/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <byteswap.h>
#include <arpa/inet.h>

#include "ldf_record_header.h"

#include "message.h"
#include "ldf_record_out.h"
#include "io_util.h"

#define LDF_BUFFER_LENGTH              0x8000
#define LDF_BUFFER_LENGTH_INCL_HEADER  (LDF_BUFFER_LENGTH + 8)

#define LDF_BUFFER_MAX_CHUNK           (LDF_BUFFER_LENGTH - 16)

#define LDF_VSN_BUFFER_LENGTH          0x1000
#define LDF_NUM_VSN                    0x100

#define LDF_SPILL_BUFFER_LENGTH        (LDF_NUM_VSN * \
					(LDF_VSN_BUFFER_LENGTH + 8) + 8)

struct ldf_out_vsn
{
  char   _buffer[LDF_VSN_BUFFER_LENGTH];
  size_t _used;
};

struct ldf_out_vsn _ldf_out_vsn[LDF_NUM_VSN];

char _ldf_spill_buffer[LDF_SPILL_BUFFER_LENGTH];

char _ldf_record_buffer[LDF_BUFFER_LENGTH_INCL_HEADER];
char *_ldf_record_buffer_p = NULL;

int _ldf_output_fd = -1;

void ldf_format_spill();

void ldf_init_output(int fd)
{
  int vsn;

  for (vsn = 0; vsn < LDF_NUM_VSN; vsn++)
    _ldf_out_vsn[vsn]._used = 0;

  _ldf_output_fd = fd;

  _ldf_record_buffer_p = _ldf_record_buffer + sizeof (ldf_record_header);
}

void *ldf_request_output(int vsn, size_t n, void *arg)
{
  struct ldf_out_vsn *out_vsn = &_ldf_out_vsn[vsn];

  (void) arg;

  if (out_vsn->_used + n > LDF_VSN_BUFFER_LENGTH)
    {
      ldf_format_spill();
    }

  /* If still too large, then size cannot be handled. */
  if (out_vsn->_used + n > LDF_VSN_BUFFER_LENGTH)
    FATAL("Too big LDF output request (%zd).", n);

  char *p = out_vsn->_buffer + out_vsn->_used;

  return p;
}

void ldf_used_output(int vsn, size_t n, void *arg)
{
  (void) arg;

  _ldf_out_vsn[vsn]._used += n;
}

void ldf_write_record()
{
  {
    ldf_record_header *ldf_header =
      (ldf_record_header *) _ldf_record_buffer;

    memcpy(&ldf_header->_type, LDF_BUFFER_TYPE_DATA,
	   sizeof (ldf_header->_type));
    ldf_header->_words = LDF_BUFFER_LENGTH / sizeof (uint32_t);
  }

  memset(_ldf_record_buffer_p, 0xff,
	 _ldf_record_buffer + LDF_BUFFER_LENGTH_INCL_HEADER -
	 _ldf_record_buffer_p);

  if (_ldf_output_fd != -1)
    {
      if (!full_write(_ldf_output_fd, _ldf_record_buffer,
		      LDF_BUFFER_LENGTH_INCL_HEADER))
	FATAL("Write failure.");
    }

  /* Reset record writing. */
  _ldf_record_buffer_p = _ldf_record_buffer + sizeof (ldf_record_header);
}

void ldf_format_spill()
{
  uint32_t spill_len;
  char *p = _ldf_spill_buffer;
  int vsn;
  uint32_t first_payload;
  uint32_t seqs, seq;

  /* Collect all the vsn data into a spill buffer. */

  for (vsn = 0; vsn < LDF_NUM_VSN; vsn++)
    {
      struct ldf_out_vsn *out_vsn = &_ldf_out_vsn[vsn];

      if (out_vsn->_used)
	{
	  ldf_spill_vsn_header *vsn_header = (ldf_spill_vsn_header *) p;

	  vsn_header->_words = out_vsn->_used / sizeof (uint32_t) + 2;
	  vsn_header->_vsn   = vsn;

	  p = (char *) (vsn_header + 1);

	  memcpy(p, out_vsn->_buffer, out_vsn->_used);

	  p += out_vsn->_used;
	}

      out_vsn->_used = 0;
    }

  /* Add the end vsn. */

  {
    ldf_spill_vsn_header *vsn_header = (ldf_spill_vsn_header *) p;

    vsn_header->_words = 2;
    vsn_header->_vsn   = 9999;

    p = (char *) (vsn_header + 1);
  }

  spill_len = p - _ldf_spill_buffer;

  /* Then the spill should be ejected into records, as chunks. */

  /* A chunk requires a 3-word header and there should be at least one
   * 0xffffffff filler word at the end.  If this matches exactly, there
   * is no payload at all.  Flush.
   */
  if (_ldf_record_buffer_p + 4 * sizeof (uint32_t) >=
      _ldf_record_buffer + LDF_BUFFER_LENGTH_INCL_HEADER)
    ldf_write_record();

  /* Calculate how much will be stored in the remaining part of the
   * first (current) record.
   */
  first_payload =
    LDF_BUFFER_LENGTH_INCL_HEADER - 4 * sizeof (uint32_t) -
    (uint32_t) (_ldf_record_buffer_p - _ldf_record_buffer);
  seqs = 1;

  if (first_payload < spill_len)
    {
      uint32_t remain = spill_len - first_payload;
      uint32_t full_seqs = remain / LDF_BUFFER_MAX_CHUNK;

      seqs += full_seqs;

      if (remain > full_seqs * LDF_BUFFER_MAX_CHUNK)
	seqs++;

      /*
      fprintf (stderr, "Spill: %d = %d + %d * %d + [%d]\n",
	       spill_len, first_payload, full_seqs, LDF_BUFFER_MAX_CHUNK,
	       remain > full_seqs * LDF_BUFFER_MAX_CHUNK);
      */
    }

  /*
  fprintf (stderr, "Spill: %d : #%d\n",
	   spill_len, seqs);
  */

  p = _ldf_spill_buffer;

  seq = 0;

  while (spill_len > 0)
    {
      uint32_t payload = spill_len;
      uint32_t remain =
	LDF_BUFFER_LENGTH_INCL_HEADER - 4 * sizeof (uint32_t) -
	(uint32_t) (_ldf_record_buffer_p - _ldf_record_buffer);

      if (payload > remain)
	payload = remain;

      {
	ldf_chunk_header *chunk_header =
	  (ldf_chunk_header *) _ldf_record_buffer_p;

	chunk_header->_bytes     = payload + 2 * sizeof (uint32_t);
	chunk_header->_seq_total = seqs;
	chunk_header->_seq_cur   = seq;

	_ldf_record_buffer_p = (char *) (chunk_header + 1);
      }

      memcpy (_ldf_record_buffer_p, p, payload);

      _ldf_record_buffer_p += payload;

      /*
      fprintf (stderr, "Spill: %d : #%d/#%d %d : %d\n",
	       spill_len, seq, seqs, payload, spill_len - payload);
      */

      p         += payload;
      spill_len -= payload;
      seq++;

      if (spill_len)
	ldf_write_record();
    }

  /*
  fprintf (stderr, "Spill: #%d of #%d\n",
	   seq, seqs);
  */

  if (seq != seqs)
    BUG("Calculation of LDF spill chunks failed #%d != #%d.",
	seq, seqs);
}

void ldf_flush_output(void)
{
  ldf_format_spill();
  if (_ldf_record_buffer_p != _ldf_record_buffer + sizeof (ldf_record_header))
    ldf_write_record();
}
