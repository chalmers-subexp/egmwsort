/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#ifndef __PULSER_H__
#define __PULSER_H__

#define PULSER_ID_CAEN            0
#define PULSER_ID_MESY            (PULSER_ID_CAEN + 1)
#define PULSER_ID_MESY_MOD_START  (PULSER_ID_MESY + 1)
#define PULSER_ID_MESY_MOD_NUM    16
#define PULSER_ID_MESY_MOD_END    (PULSER_ID_MESY_MOD_START + \
				   PULSER_ID_MESY_MOD_NUM)
#define PULSER_ID_ASIC_START      (PULSER_ID_MESY_MOD_END + 1)
#define PULSER_ID_ASIC_NUM        4
#define PULSER_ID_ASIC_END        (PULSER_ID_ASIC_START + \
				   PULSER_ID_ASIC_NUM)
#define PULSER_ID_NUM             (PULSER_ID_ASIC_END)

void pulser_setup(const char *cfg);

void pulser_expect(int id);

void pulser_got(int id, uint64_t ts, uint16_t t_fine);

void pulser_summary(void);

#endif/*__PULSER_H__*/

