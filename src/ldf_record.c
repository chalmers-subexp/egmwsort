/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>

#include <byteswap.h>
#include <arpa/inet.h>

#include "ldf_record_header.h"

#include "message.h"
#include "config.h"
#include "ldf_record.h"
#include "item.h"
#include "io_util.h"

#define BUFFER_LENGTH 0x10000

/* LDF files are misaligned vs. 16-byte boundaries, due to data blocks
 * being 2^n size, but headers not included.
 *
 * Alternative to get 'hexdump -C'-like output but with 8-byte lines:
 *
 * xxd -e -c 8 FILENAME.ldf
 */

int data_buffers = 0;
int data_chunks = 0;
int data_spills = 0;
int data_modules = 0;
int data_hits = 0;

char *ldf_spill = NULL;
size_t ldf_spill_len = 0;
size_t ldf_spill_alloc = 0;

int ldf_read_spill_items(char *p, uint32_t remain)
{
  int ret = 1;

  _record_buffer = p;

  while (remain)
    {
      uint32_t used = item_ldf_read((uint32_t *) p, remain);

      if (used == 0)
	{
	  /* Error occured. */
	  ret = 0;
	  break;
	}

      if (used > remain)
	{
	  FATAL("Internal item_read error, used (%d) > remain (%d).",
		used, remain);
	}

      data_hits++;

      p      += used;
      remain -= used;
    }

  return ret;
}

int ldf_read_record_spill(char *p, uint32_t remain)
{
  data_spills++;

  memset(_record_pixie_num_crate_slot, 0,
         sizeof (_record_pixie_num_crate_slot));

  if (_egmw_config._print_record)
    {
      fprintf (stderr,
	       "========================================"
	       "===========================\n");

      if (_egmw_config._print_record)
	fprintf (stderr,
		 "Spill:\n");
      /* Could print data_length. */
    }

  while (remain)
    {
      uint32_t words, bytes, bytes_data;
      uint32_t vsn;

      if (remain < sizeof (uint32_t))
	{
	  ERROR("LDF spill data, no space for length (4 bytes) > remain (%d).",
		remain);
	  return 0;
	}

      words = *((uint32_t *) &p[0]);

      if (words == 0xffffffff)
	{
	  p      += sizeof (uint32_t);
	  remain -= sizeof (uint32_t);
	  continue;
	}

      if (remain < 2 * sizeof (uint32_t))
	{
	  ERROR("LDF spill data, no space for module (8 bytes) > remain (%d).",
		remain);
	  return 0;
	}

      vsn   = *((uint32_t *) &p[4]);

      bytes = words * sizeof (uint32_t);

      /* printf ("%05x*4 = %06x = %7d : %4d\n", words, bytes, bytes, vsn); */

      if (bytes == 0xffffffff)
	{
	  /* TODO: check that remaining buffer also if 0xff. */
	  /* printf ("%08x\n", bytes); */
	  return 1;
	}

      if (bytes < 2 * sizeof (uint32_t))
	{
	  ERROR("LDF data spill module too small for header, "
		"(%d < 2*%d bytes).",
		bytes, (int) sizeof (uint32_t));
	  return 0;
	}

      if (remain < bytes)
	{
	  ERROR("LDF data spill module larger than remaining block "
		"(%d > remain %d).",
		bytes, remain);
	  return 0;
	}

      data_modules++;

      bytes_data = bytes - 2 * sizeof (uint32_t);

      /* Read the data in this chunk. */

      (void) vsn;

      message_rate_limit_begin();

      if (!ldf_read_spill_items(p + 2 * sizeof (uint32_t),
				bytes_data))
	{
	  ERROR("Failed to read LDF chunk items.");
	  return 0;
	}

      message_rate_limit_end();

      p += bytes;
      remain -= bytes;
    }

  message_rate_limit_begin();

  item_process_items();

  message_rate_limit_end();

  if (_egmw_config._print_record)
    {
      int crate, slot;

      for (crate = 0; crate < 16; crate++)
	{
	  for (slot = 0; slot < 16; slot++)
	    if (_record_pixie_num_crate_slot[crate][slot])
	      goto has_pixie_crate;

	  continue;
	has_pixie_crate:
	  fprintf (stderr, "PIXIE: %2d :", crate);
	  for (slot = 0; slot < 16; slot++)
	    fprintf (stderr, " %6d%s",
		     _record_pixie_num_crate_slot[crate][slot],
		     slot == 7 ? "\n           " : "");
	  fprintf (stderr, "\n");
	}

      fprintf (stderr,
	       "Spill.\n");
    }

  return 1;
}

/* The sequence number continuity checks does are a bit complex and
 * does not catch all errors.  It would have been much easier if a
 * spill number would have been part of the data as well.
 */

uint32_t _spill_collect_total    = (uint32_t) -1;
uint32_t _spill_collect_seq_prev = (uint32_t) -1;
int      _spill_collect_good     = 0;

int ldf_read_record_data(char *p, uint32_t remain)
{
  data_buffers++;

  while (remain)
    {
      uint32_t bytes, bytes_data;
      uint32_t seq_total, seq_cur;

      if (remain < sizeof (uint32_t))
	{
	  ERROR("LDF read data, no space for chunk (4 bytes) > remain (%d).",
		remain);
	  return 0;
	}

      bytes = *((uint32_t *) p);
      p += sizeof (uint32_t); remain -= sizeof (uint32_t);

      if (bytes == 0xffffffff)
	{
	  /* TODO: check that remaining buffer also if 0xff. */
	  /* printf ("%08x\n", bytes); */
	  return 1;
	}

      if (bytes < 2 * sizeof (uint32_t))
	{
	  ERROR("LDF data chunk too small for sequence numbers, "
		"(%d < 2*%d bytes).",
		bytes, (int) sizeof (uint32_t));
	  return 0;
	}

      if (remain < bytes)
	{
	  ERROR("LDF data chunk larger than remaining block (%d > remain %d).",
		bytes, remain);
	  return 0;
	}

      data_chunks++;

      seq_total = *((uint32_t *) &p[0]);
      seq_cur   = *((uint32_t *) &p[4]);

      /* printf ("%5d %2d/%d\n", bytes, seq_cur, seq_total); */

      if (seq_cur == 0)
	{
	  /* First chunk in sequence.
	   * We can reset the number of chunks to expect in total.
	   */
	  _spill_collect_total    = seq_total;
	  _spill_collect_seq_prev = (uint32_t) -1;
	  _spill_collect_good     = 1;
	}

      if (!_spill_collect_good)
	goto end_chunk; /* Only report errors once. */

      if (seq_total != _spill_collect_total)
	{
	  /* Mismatch in total number of chunks expected for this
	   * spill.  Cannot build or use spill.
	   */
	  ERROR("LDF data chunk sequence #%d total %d mismatch prev total %d.",
		seq_cur, seq_total, _spill_collect_total);
	  _spill_collect_good = 0;
	  goto end_chunk;
	}

      if (seq_cur != _spill_collect_seq_prev + 1)
	{
	  /* Sequence number mismatch, at least one lost. */
	  ERROR("LDF data chunk sequence #%d/%d not increasing from prev #%d.",
		seq_cur, seq_total, _spill_collect_seq_prev);
	  _spill_collect_good = 0;
	  goto end_chunk;
	}

      /* Copy the data in this chunk to the spill. */

      bytes_data = bytes - 2 * sizeof (uint32_t);

      if (ldf_spill_len + bytes_data > ldf_spill_alloc)
	{
	  if (!ldf_spill_alloc)
	    ldf_spill_alloc = 1024;

	  while (ldf_spill_alloc < ldf_spill_len + bytes_data)
	    ldf_spill_alloc *= 2;

	  ldf_spill = (char *) realloc(ldf_spill, ldf_spill_alloc);

	  if (!ldf_spill)
	    {
	      FATAL_MEMORY("LDF spill buffer.");
	    }
	}

      memcpy (ldf_spill + ldf_spill_len, p + 2 * sizeof (uint32_t),
	      bytes_data);

      ldf_spill_len += bytes_data;

      _spill_collect_seq_prev = seq_cur;

      /* Last chunk in sequence, use the spill. */
      if (seq_cur == seq_total - 1)
	{
	  /* printf ("spill %zd\n", ldf_spill_len); */

	  if (!ldf_read_record_spill(ldf_spill, ldf_spill_len))
	    {
	      ERROR("Failed to read LDF spill.");
	      return 0;
	    }

	  ldf_spill_len = 0;
	}

    end_chunk:
      p += bytes;
      remain -= bytes;
    }

  return 1;
}

#define LDF_TYPE_DIR   1
#define LDF_TYPE_HEAD  2
#define LDF_TYPE_DATA  3
#define LDF_TYPE_EOF   4

typedef struct record_ldf_info_t
{
  uint32_t _header_length;
  uint32_t _data_length;

  int      _type;

} record_ldf_info;

void to_print_char(char *dest, char *src, size_t n)
{
  size_t i;

  for (i = 0; i < n; i++)
    {
      if (isprint(*src))
	*dest = *src;
      else
	*dest = '.';

      dest++;
      src++;
    }

  *dest = 0;
}

int handle_ldf_record_header(ldf_record_header *record,
			     record_ldf_info *info)
{
  uint8_t hex_ffffffff_ffffffff[8] =
    { 0xff, 0xff, 0xff, 0xff,  0xff, 0xff, 0xff, 0xff };

  if (memcmp(&record->_type, LDF_BUFFER_TYPE_DIR,
	     sizeof (record->_type)) == 0)
    info->_type = LDF_TYPE_DIR;
  else if (memcmp(&record->_type, LDF_BUFFER_TYPE_HEAD,
		  sizeof (record->_type)) == 0)
    info->_type = LDF_TYPE_HEAD;
  else if (memcmp(&record->_type, LDF_BUFFER_TYPE_DATA,
		  sizeof (record->_type)) == 0)
    info->_type = LDF_TYPE_DATA;
  else if (memcmp(&record->_type, LDF_BUFFER_TYPE_EOF,
		  sizeof (record->_type)) == 0)
    info->_type = LDF_TYPE_EOF;
  else if (memcmp(record, hex_ffffffff_ffffffff,
		  sizeof (*record)) == 0)
    {
      /* Empty record that we skip. */
      /* This occurs due to HEAD records having length 0x40 words,
       * and then these fillers. */

      /* This is *NOT* efficient reading due to reads of 8 bytes at a
       * time in parent function, but works.
       */

      info->_header_length = sizeof (*record);
      info->_data_length   = 0;
      info->_type          = 0;

      return 1;
    }
  else
    {
      char str[5];

      to_print_char(str, (char*) &record->_type, 4);

      ERROR("Bad LDF record type, got '%4.4s'=0x%08x "
	    "(expected DIR, HEAD or DATA).",
	    str, record->_type);
      return 0;
    }

  if (record->_words * sizeof (uint32_t) > BUFFER_LENGTH)
    {
      ERROR("Unhandled LDF data length (too long): %d * %d-byte words > %d.",
	    record->_words, (int) sizeof (uint32_t), BUFFER_LENGTH);
      return 0;
    }

  info->_header_length = sizeof (*record);
  info->_data_length   = record->_words * sizeof (uint32_t);

  return 1;
}

int ldf_read_record(int fd)
{
  ldf_record_header h;
  record_ldf_info   info;
  char buffer[BUFFER_LENGTH];

  memset(&info, 0, sizeof (info));

  /* Each record begins with a two word type-length header. */

  if (!full_read(fd, &h, sizeof (h)))
    {
      /* ERROR("Failed to read record header."); */
      return 0;
    }

  if (!handle_ldf_record_header(&h, &info))
    return 0;

  if (info._type == 0)
    return 1;

  if (!full_read(fd, &buffer, info._data_length))
    {
      ERROR("Failed to read LDF record data: %d.",
	    (int) info._data_length);
      return 0;
    }

  /*
  fprintf (stderr,
	   "read: %d + %d\n", (int) sizeof (h), info._data_length);
  */

  switch (info._type)
    {
    case LDF_TYPE_DIR:
    case LDF_TYPE_HEAD:
    case LDF_TYPE_EOF:
      break;
    case LDF_TYPE_DATA:
      if (!ldf_read_record_data(buffer, info._data_length))
	return 0;
      break;
    }

  if (_egmw_config._print_record && 0)
    {
      fprintf (stderr, "==================================================\n");

      if (_egmw_config._print_record)
	fprintf (stderr,
		 "Record:\n");
    }

  return 1;
}

void ldf_records_done(void)
{
  message_rate_limit_begin();

  item_process_remaining();

  message_rate_limit_end();
}

void ldf_records_summary(void)
{
  INFO("LDF buf: %d chunk: %d spill: %d mod: %d hits: %d",
       data_buffers, data_chunks, data_spills, data_modules, data_hits);
}
