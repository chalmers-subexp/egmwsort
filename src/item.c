/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#include <stdio.h>
#include <inttypes.h>

#include "message.h"
#include "config.h"
#include "item.h"
#include "item_state.h"
#include "ebye_record.h"
#include "ext_data.h"
#include "pulser.h"
#include "steer_params.h"

#include "inc_colourtext.h"

item_state _item_state;

#define EXPECT_BIT(i)  (((uint64_t) 1) << (i))

typedef struct item_config_t
{
  uint64_t _expect_febex;
  uint64_t _expect_asic;
  uint64_t _expect_caen;
  uint64_t _expect_caen_grp;
  uint64_t _expect_mesytec;
} item_config;

item_config _item_config;

void item_expect_spec(const char *spec);

char *_record_buffer;

int _first_in_record = 0;
uint64_t _old_first_ts[8];

uint64_t _synthetic_data_check_count = 0;
uint64_t _synthetic_data_check_bad   = 0;

uint8_t _synthetic_caen_check[32 /* mod */][64 /*ch */ ][4 /* data_idx */];

lwroc_accum_buffer *_accum_febex     = NULL;
lwroc_accum_buffer *_accum_asic      = NULL;
lwroc_accum_buffer *_accum_asic_info = NULL;
lwroc_accum_buffer *_accum_caen      = NULL;
lwroc_accum_buffer *_accum_pixie     = NULL;

lwroc_accum_many *_accum_many = NULL;

lwroc_accum_buffer *_accum_rteb_febex     = NULL;
lwroc_accum_buffer *_accum_rteb_asic      = NULL;
lwroc_accum_buffer *_accum_rteb_asic_info = NULL;
lwroc_accum_buffer *_accum_rteb_caen      = NULL;
lwroc_accum_buffer *_accum_rteb_pixie     = NULL;

lwroc_accum_buffer *_accum_rteb_trig      = NULL;

lwroc_accum_many *_accum_rteb_many = NULL;

lwroc_accum_buffer *_accum_hb_ELUM = NULL;
lwroc_accum_buffer *_accum_hb_LUME = NULL;
lwroc_accum_buffer *_accum_hb_ISIA = NULL;
lwroc_accum_buffer *_accum_hb_CD   = NULL;
lwroc_accum_buffer *_accum_hb_REC  = NULL;
lwroc_accum_buffer *_accum_hb_ZD   = NULL;
lwroc_accum_buffer *_accum_hb_MWPC = NULL;

lwroc_accum_many *_accum_hb_many = NULL;

void item_report_ins_backwards(uint32_t type, uint32_t id,
			       lwroc_accum_item_base *item,
			       uint64_t last_ts);

void item_report_pick_backwards(uint32_t type, uint32_t id,
				lwroc_accum_item_base *item,
				uint64_t last_ts);

void item_report_resorted(uint32_t type, uint32_t id, size_t n);

void item_report_rteb_ins_backwards(uint32_t type, uint32_t id,
				    lwroc_accum_item_base *item,
				    uint64_t last_ts);

void item_report_rteb_pick_backwards(uint32_t type, uint32_t id,
				     lwroc_accum_item_base *item,
				     uint64_t last_ts);

void item_report_rteb_resorted(uint32_t type, uint32_t id, size_t n);

void item_report_hb_ins_backwards(uint32_t type, uint32_t id,
				  lwroc_accum_item_base *item,
				  uint64_t last_ts);

void item_report_hb_pick_backwards(uint32_t type, uint32_t id,
				   lwroc_accum_item_base *item,
				   uint64_t last_ts);

void item_report_hb_resorted(uint32_t type, uint32_t id, size_t n);

void item_init_state(void)
{
  int ret;

  memset(&_item_state, 0, sizeof (_item_state));

  memset(&_item_config, 0, sizeof (_item_config));

  _item_state._accum_id = (uint32_t) -1;
  _item_state._accum_is_ok = 1;

  memset(_old_first_ts, 0, sizeof (_old_first_ts));

  memset(_synthetic_caen_check, 0, sizeof (_synthetic_caen_check));

  /********************************************************************/
  /* First-stage accumulation buffers.                                */
  /* From input, each item individually, as not yet sorted.           */

  _accum_many = lwroc_accum_many_init(5);

  if (!_accum_many)
    FATAL_MEMORY("Accumulation many-buffer.");

  _accum_febex =
    lwroc_accum_init(_accum_many, 0,
		     ACCUM_ID_TYPE_FEBEX,
		     NUM_ACCUM_IDS_FEBEX,
		     item_report_ins_backwards,
		     item_report_pick_backwards,
		     item_report_resorted,
		     item_process_febex_item);

  _accum_asic =
    lwroc_accum_init(_accum_many, 1,
		     ACCUM_ID_TYPE_ASIC,
		     NUM_ACCUM_IDS_ASIC,
		     item_report_ins_backwards,
		     item_report_pick_backwards,
		     item_report_resorted,
		     item_process_asic_item);

  _accum_asic_info =
    lwroc_accum_init(_accum_many, 2,
		     ACCUM_ID_TYPE_ASIC_INFO,
		     NUM_ACCUM_IDS_ASIC_INFO,
		     item_report_ins_backwards,
		     item_report_pick_backwards,
		     item_report_resorted,
		     item_process_asic_info_item);

  _accum_caen =
    lwroc_accum_init(_accum_many, 3,
		     ACCUM_ID_TYPE_CAEN,
		     NUM_ACCUM_IDS_CAEN,
		     item_report_ins_backwards,
		     item_report_pick_backwards,
		     item_report_resorted,
		     item_process_caen_item);

  _accum_pixie =
    lwroc_accum_init(_accum_many, 4,
		     ACCUM_ID_TYPE_PIXIE,
		     NUM_ACCUM_IDS_PIXIE,
		     item_report_ins_backwards,
		     item_report_pick_backwards,
		     item_report_resorted,
		     item_process_pixie_item);

  if (!_accum_febex ||
      !_accum_asic ||
      !_accum_asic_info ||
      !_accum_caen ||
      !_accum_pixie)
    FATAL_MEMORY("Accumulation buffer.");

  ret = lwroc_accum_many_post_init(_accum_many);

  if (!ret)
    FATAL_MEMORY("Accumulation many-buffer post-init.");

  /********************************************************************/
  /* Second-stage accumulation buffers - raw trigger event building.  */
  /* Items already sorted, so only one queue per type.                */

  _accum_rteb_many = lwroc_accum_many_init(5+1);

  if (!_accum_rteb_many)
    FATAL_MEMORY("Accumulation RTEB many-buffer.");

  _accum_rteb_febex =
    lwroc_accum_init(_accum_rteb_many, 0,
		     ACCUM_ID_TYPE_FEBEX,
		     1,
		     item_report_rteb_ins_backwards,
		     item_report_rteb_pick_backwards,
		     item_report_rteb_resorted,
		     item_process_rteb_febex_item);

  _accum_rteb_asic =
    lwroc_accum_init(_accum_rteb_many, 1,
		     ACCUM_ID_TYPE_ASIC,
		     1,
		     item_report_rteb_ins_backwards,
		     item_report_rteb_pick_backwards,
		     item_report_rteb_resorted,
		     item_process_rteb_asic_item);

  _accum_rteb_asic_info =
    lwroc_accum_init(_accum_rteb_many, 2,
		     ACCUM_ID_TYPE_ASIC_INFO,
		     1,
		     item_report_rteb_ins_backwards,
		     item_report_rteb_pick_backwards,
		     item_report_rteb_resorted,
		     item_process_rteb_asic_info_item);

  _accum_rteb_caen =
    lwroc_accum_init(_accum_rteb_many, 3,
		     ACCUM_ID_TYPE_CAEN,
		     1,
		     item_report_rteb_ins_backwards,
		     item_report_rteb_pick_backwards,
		     item_report_rteb_resorted,
		     item_process_rteb_caen_item);

  _accum_rteb_pixie =
    lwroc_accum_init(_accum_rteb_many, 4,
		     ACCUM_ID_TYPE_PIXIE,
		     1,
		     item_report_rteb_ins_backwards,
		     item_report_rteb_pick_backwards,
		     item_report_rteb_resorted,
		     item_process_rteb_pixie_item);

  _accum_rteb_trig =
    lwroc_accum_init(_accum_rteb_many, 5,
		     ACCUM_ID_TYPE_TRIG,
		     1,
		     item_report_rteb_ins_backwards,
		     item_report_rteb_pick_backwards,
		     item_report_rteb_resorted,
		     item_process_rteb_trig_item);

  if (!_accum_rteb_febex ||
      !_accum_rteb_asic ||
      !_accum_rteb_asic_info ||
      !_accum_rteb_caen ||
      !_accum_rteb_pixie ||
      !_accum_rteb_trig)
    FATAL_MEMORY("Accumulation RTEB buffer.");

  ret = lwroc_accum_many_post_init(_accum_rteb_many);

  if (!ret)
    FATAL_MEMORY("Accumulation RTEB many-buffer post-init.");

  /********************************************************************/
  /* Second-stage accumulation buffers - hit building.                */
  /* Items already sorted, so only one queue per type.                */

  _accum_hb_many = lwroc_accum_many_init(4+1);

  if (!_accum_hb_many)
    FATAL_MEMORY("Accumulation HB many-buffer.");

  _accum_hb_ELUM =
    lwroc_accum_init(_accum_hb_many, 0,
		     ACCUM_ID_TYPE_ELUM,
		     1,
		     item_report_hb_ins_backwards,
		     item_report_hb_pick_backwards,
		     item_report_hb_resorted,
		     item_process_hb_ELUM_item);

  _accum_hb_LUME =
    lwroc_accum_init(_accum_hb_many, 1,
		     ACCUM_ID_TYPE_LUME,
		     1,
		     item_report_hb_ins_backwards,
		     item_report_hb_pick_backwards,
		     item_report_hb_resorted,
		     item_process_hb_LUME_item);

  _accum_hb_ISIA =
    lwroc_accum_init(_accum_hb_many, 2,
		     ACCUM_ID_TYPE_ISIA,
		     1,
		     item_report_hb_ins_backwards,
		     item_report_hb_pick_backwards,
		     item_report_hb_resorted,
		     item_process_hb_ISIA_item);

  _accum_hb_CD =
    lwroc_accum_init(_accum_hb_many, 3,
		     ACCUM_ID_TYPE_CD,
		     1,
		     item_report_hb_ins_backwards,
		     item_report_hb_pick_backwards,
		     item_report_hb_resorted,
		     item_process_hb_CD_item);

  _accum_hb_REC =
    lwroc_accum_init(_accum_hb_many, 4,
		     ACCUM_ID_TYPE_REC,
		     1,
		     item_report_hb_ins_backwards,
		     item_report_hb_pick_backwards,
		     item_report_hb_resorted,
		     item_process_hb_REC_item);

  _accum_hb_ZD =
    lwroc_accum_init(_accum_hb_many, 5,
		     ACCUM_ID_TYPE_ZD,
		     1,
		     item_report_hb_ins_backwards,
		     item_report_hb_pick_backwards,
		     item_report_hb_resorted,
		     item_process_hb_ZD_item);

  _accum_hb_MWPC =
    lwroc_accum_init(_accum_hb_many, 6,
		     ACCUM_ID_TYPE_MWPC,
		     1,
		     item_report_hb_ins_backwards,
		     item_report_hb_pick_backwards,
		     item_report_hb_resorted,
		     item_process_hb_MWPC_item);

  if (!_accum_hb_ELUM ||
      !_accum_hb_LUME ||
      !_accum_hb_ISIA ||
      !_accum_hb_CD ||
      !_accum_hb_REC)
    FATAL_MEMORY("Accumulation HB buffer.");

  ret = lwroc_accum_many_post_init(_accum_hb_many);

  if (!ret)
    FATAL_MEMORY("Accumulation HB many-buffer post-init.");

  /********************************************************************/
}

void item_expect_spec(const char *spec)
{
  const char *p;
  uint64_t *mask = NULL;
  int i;

  if (!spec)
    {
      /* We have no idea what to expect.  Allow all.
       * Also mean that time-sorting must use maximum delay.
       */

      _item_config._expect_febex    = (uint64_t) -1;
      _item_config._expect_asic     = (uint64_t) -1;
      _item_config._expect_caen     = (uint64_t) -1;
      _item_config._expect_caen_grp = (uint64_t) -1;
      _item_config._expect_mesytec  = (uint64_t) -1;

      return;
    }

  if (strcmp(spec, "help") == 0)
    {
      printf ("  --expect=SPEC\n");
      printf ("\n");
      printf ("  SPEC: c01a012 = expect CAEN modules 0,1 and ASICs 0,1,2.\n");
      exit(0);
    }

  for (p = spec; *p; p++)
    {
      if (*p == 'f')
	mask = &_item_config._expect_febex;
      else if (*p == 'a')
	mask = &_item_config._expect_asic;
      else if (*p == 'c')
	mask = &_item_config._expect_caen;
      else if (*p == 'm')
	mask = &_item_config._expect_mesytec;
      else if ((*p >= '0' && *p <= '9') ||
	       (*p >= 'A' && *p <= 'F'))
	{
	  if ((*p >= '0' && *p <= '9'))
	    i = *p - '0';
	  else
	    i = *p - 'A';

	  if (!mask)
	    BADCFG("Expect specification cannot start with number.");

	  *mask |= EXPECT_BIT(i);
	}
      else
	{
	  BADCFG("Unhandled character '%c' in expect specification.", *p);
	}
    }

  for (i = 0; i < NUM_MODULES_CAEN; i++)
    {
      if (_item_config._expect_caen & EXPECT_BIT(i))
	_item_config._expect_caen_grp |= EXPECT_BIT(i / 16);
    }

  if (_item_config._expect_caen_grp & (1 << 0))
    pulser_expect(PULSER_ID_CAEN);

  for (i = 0; i < PULSER_ID_MESY_MOD_NUM; i++)
    if (_item_config._expect_mesytec & (1 << i))
      pulser_expect(PULSER_ID_MESY_MOD_START + i);

  for (i = 0; i < PULSER_ID_ASIC_NUM; i++)
    if (_item_config._expect_asic & (1 << i))
      pulser_expect(PULSER_ID_ASIC_START + i);

  INFO("Expect: "
       "febex 0x%" PRIx64", "
       "asic 0x%" PRIx64", "
       "caen 0x%" PRIx64", "
       "caen_grp 0x%" PRIx64", "
       "mesytec 0x%" PRIx64".",
       _item_config._expect_febex,
       _item_config._expect_asic,
       _item_config._expect_caen,
       _item_config._expect_caen_grp,
       _item_config._expect_mesytec);
}

/* Can be called from accum_sort_until(). */
void item_report_pick_backwards(uint32_t type, uint32_t id,
				lwroc_accum_item_base *item,
				uint64_t last_ts)
{
  char name_id[256];

  item_name_id(name_id, sizeof (name_id), type | id, -1);

  ERROR("Picked timestamp backwards for %s "
	"(%016" PRIx64 " - %016" PRIx64 " = %" PRId64 ")",
	name_id,
	item->_ts, last_ts,
	(int64_t) (item->_ts - last_ts));

  _record_febex_had_backward = 1;
}

/* Can be called from accum_sort_until(). */
void item_report_resorted(uint32_t type, uint32_t id, size_t n)
{
  char name_id[256];

  item_name_id(name_id, sizeof (name_id), type | id, -1);

  ERROR("Resorted for %s, %zd items.",
	name_id, n);
}

/* Can be called from accum_sort_until(). */
void item_report_rteb_pick_backwards(uint32_t type, uint32_t id,
				     lwroc_accum_item_base *item,
				     uint64_t last_ts)
{
  char name_id[256];

  item_name_id(name_id, sizeof (name_id), type | id | item->_aux, -1);

  ERROR("Picked EB timestamp backwards for %s "
	"(%016" PRIx64 " - %016" PRIx64 " = %" PRId64 ")",
	name_id,
	item->_ts, last_ts,
	(int64_t) (item->_ts - last_ts));
}

/* Can be called from accum_sort_until(). */
void item_report_rteb_resorted(uint32_t type, uint32_t id, size_t n)
{
  char name_id[256];

  item_name_id(name_id, sizeof (name_id), type | id, -1);

  ERROR("Resorted EB for %s, %zd items.",
	name_id, n);
}

/* Can be called from accum_sort_until(). */
void item_report_hb_pick_backwards(uint32_t type, uint32_t id,
				   lwroc_accum_item_base *item,
				   uint64_t last_ts)
{
  char name_id[256];

  item_name_hb_id(name_id, sizeof (name_id), type | id | item->_aux, -1);

  ERROR("Picked HB timestamp backwards for %s "
	"(%016" PRIx64 " - %016" PRIx64 " = %" PRId64 ")",
	name_id,
	item->_ts, last_ts,
	(int64_t) (item->_ts - last_ts));
}

/* Can be called from accum_sort_until(). */
void item_report_hb_resorted(uint32_t type, uint32_t id, size_t n)
{
  char name_id[256];

  item_name_hb_id(name_id, sizeof (name_id), type | id, -1);

  ERROR("Resorted HB for %s, %zd items.",
	name_id, n);
}

#define FIND_TS_LAST(ts_last, ts_array, limit) do {	\
    int __i;						\
    for (__i = 0; __i < (limit); __i++)			\
      if (ts_array[__i]._ts > ts_last)			\
	ts_last = ts_array[__i]._ts;			\
  } while (0);

#define FIND_TS_SORT(ts_sort_until_max, ts_array, expect_array, limit) do { \
    int __i;								\
    for (__i = 0; __i < (limit); __i++)					\
      if (expect_array & EXPECT_BIT(__i)) {				\
	if (ts_array[__i]._ts < ts_sort_until_max)			\
	  ts_sort_until_max = ts_array[__i]._ts;			\
	/* printf(#ts_array "%d  %" PRId64 "\n",			\
	   __i, ts_sort_until_max); */					\
      }									\
  } while (0);

void item_process_items(void)
{
  uint64_t ts_last = 0;
  uint64_t ts_sort_until;
  uint64_t ts_sort_until_min;
  uint64_t ts_sort_until_max = (uint64_t) -1;

  /*
  fprintf (stderr,
	   "%016" PRIx64 " %016" PRIx64 " %016" PRIx64 "  \n"
	   "%016" PRIx64 " %016" PRIx64 " %016" PRIx64 "  \n"
	   "%016" PRIx64 " %016" PRIx64 " %016" PRIx64 "  \n"
	   "%016" PRIx64 " %016" PRIx64 "\n",
	   _item_state._ts_last._asic[0]._ts,
	   _item_state._ts_last._asic[1]._ts,
	   _item_state._ts_last._asic[2]._ts,
	   _item_state._ts_last._asic_mod[0]._ts,
	   _item_state._ts_last._asic_mod[1]._ts,
	   _item_state._ts_last._asic_mod[2]._ts,
	   _item_state._ts_last._asic_info[0]._ts,
	   _item_state._ts_last._asic_info[1]._ts,
	   _item_state._ts_last._asic_info[2]._ts,
	   _item_state._ts_last._caen[0]._ts,
	   _item_state._ts_last._caen[1]._ts);
  */
  /*
  fprintf (stderr,
	   "%" PRId64 " %" PRId64 " %" PRId64 "  \n"
	   "%" PRId64 " %" PRId64 " %" PRId64 "  \n"
	   "%" PRId64 " %" PRId64 " %" PRId64 "  \n"
	   "%" PRId64 " %" PRId64 "\n",
	   _item_state._ts_last._asic[0]._ts / 1000000000,
	   _item_state._ts_last._asic[1]._ts / 1000000000,
	   _item_state._ts_last._asic[2]._ts / 1000000000,
	   _item_state._ts_last._asic_mod[0]._ts / 1000000000,
	   _item_state._ts_last._asic_mod[1]._ts / 1000000000,
	   _item_state._ts_last._asic_mod[2]._ts / 1000000000,
	   _item_state._ts_last._asic_info[0]._ts / 1000000000,
	   _item_state._ts_last._asic_info[1]._ts / 1000000000,
	   _item_state._ts_last._asic_info[2]._ts / 1000000000,
	   _item_state._ts_last._caen[0]._ts / 1000000000,
	   _item_state._ts_last._caen[1]._ts / 1000000000);
  */

  FIND_TS_LAST(ts_last, _item_state._ts_last._febex,     NUM_BOARDS_FEBEX);
  FIND_TS_LAST(ts_last, _item_state._ts_last._asic,      NUM_MODULES_ASIC);
  FIND_TS_LAST(ts_last, _item_state._ts_last._asic_mod,  NUM_MODULES_ASIC_MOD);
  FIND_TS_LAST(ts_last, _item_state._ts_last._asic_info, NUM_MODULES_ASIC_INFO);
  FIND_TS_LAST(ts_last, _item_state._ts_last._caen,      NUM_MODULES_CAEN);
  FIND_TS_LAST(ts_last, _item_state._ts_last._caen_grp,  NUM_MODULES_CAEN_GRP);

  {
    static uint64_t ts_last_prev = 0;

    /* Search for things not working correctly! */
    /* Should not happen!!! */

    if (ts_last < ts_last_prev)
      ERROR("Last: out-of-order.");
    ts_last_prev = ts_last;
  }

  ts_sort_until_min = ts_last - TS_SORT_MIN_MARGIN;

  /* If we have a set of expected modules, then we only need to
   * consider those.
   */

  ts_sort_until_max = ts_last;

  FIND_TS_SORT(ts_sort_until_max,
	       _item_state._ts_last._febex,
	       _item_config._expect_febex,
	       NUM_BOARDS_FEBEX);
  FIND_TS_SORT(ts_sort_until_max,
	       _item_state._ts_last._asic,
	       _item_config._expect_asic,
	       NUM_MODULES_ASIC);
  FIND_TS_SORT(ts_sort_until_max,
	       _item_state._ts_last._caen_grp,
	       _item_config._expect_caen_grp,
	       NUM_MODULES_CAEN_GRP);

  /* Only sort until the point where we have gotten the oldest data
   * for any module (/ group of modules) that are expected.
   */
  ts_sort_until = ts_sort_until_max - TS_FAR_BACKWARDS_ASIC;
  /* But if that is waaaay too old, then use a clamp.
   * That is relative to the newest data we have gotten.
   */
  if (ts_sort_until_min > ts_sort_until)
    ts_sort_until = ts_sort_until_min;

#define TIMEDIFF_NS_AS_S(x) (((int64_t) (x)) / 1000000000)

  if (_stderr_isatty)
    fprintf (stderr,
	     "Last: %6" PRId64 " s, "
	     "%5" PRId64 " s "
	     "%4" PRId64 " s "
	     "(%4" PRId64 " s, %4" PRId64 " s, %4" PRId64 " s, %4" PRId64 " s)"
	     "%s\r",
	     TIMEDIFF_NS_AS_S(ts_last),
	     TIMEDIFF_NS_AS_S(ts_sort_until - ts_last),
	     TIMEDIFF_NS_AS_S(ts_sort_until_max - ts_last),
	     TIMEDIFF_NS_AS_S(_item_state._ts_last._asic[0]._ts - ts_last),
	     TIMEDIFF_NS_AS_S(_item_state._ts_last._asic[1]._ts - ts_last),
	     TIMEDIFF_NS_AS_S(_item_state._ts_last._asic[2]._ts - ts_last),
	     TIMEDIFF_NS_AS_S(_item_state._ts_last._caen_grp[0]._ts - ts_last),
	     CT_ERR(CLR_EOL));

  /*
  fprintf (stderr,
	   "%" PRId64 " ? %" PRId64 "    \n",
	   _item_state._ts_max_seen / 1000000000,
	   ts_sort_until / 1000000000);
  */

  /* Only use value if it did not wrap around negative 0. */
  if (ts_sort_until < ts_last)
    {
      if (_item_state._ts_max_seen + TS_SORT_MIN_MARGIN < ts_sort_until)
	{
	  /* Timestamps seen in last record (since last time we were
	   * called), are much smaller than our sorting range.
	   *
	   * Under that circumstance we do not sort.
	   *
	   * But if it persists for 5 times, then we sort _everything_ in
	   * order to flush the buffers.
	   */
	  _item_state._ts_max_seen_backwards++;

	  if (_item_state._ts_max_seen_backwards >=
	      NUM_BACKWARDS_BEFORE_FLUSH_SORT)
	    {
	      WARNING("New data is repeatedly earlier than buffered data "
		      "(current sorting front).  "
		      "Sorting all to flush.  "
		      "(%016" PRIx64 " - %016" PRIx64 " = %" PRId64 ")",
		      _item_state._ts_max_seen, ts_sort_until,
		      (int64_t) (_item_state._ts_max_seen - ts_sort_until));

	      item_process_remaining();

	      _item_state._ts_sorted_until = 0;
	      memset(&_item_state._ts_last, 0, sizeof (_item_state._ts_last));
	    }
	  else
	    WARNING("New data is earlier than buffered data "
		    "(current sorting front).  "
		    "Not sorting (#%d/#%d).  "
		    "(%016" PRIx64 " - %016" PRIx64 " = %" PRId64 ")",
		    _item_state._ts_max_seen_backwards,
		    NUM_BACKWARDS_BEFORE_FLUSH_SORT,
		    _item_state._ts_max_seen, ts_sort_until,
		    (int64_t) (_item_state._ts_max_seen - ts_sort_until));
	}
      else
	{
	  uint64_t eb_sort_until;
	  uint64_t max_rteb_back;

	  lwroc_accum_sort_until(_accum_many, ts_sort_until, NULL);

	  eb_sort_until = 0;

	  max_rteb_back =
	    _egmw_config._rteb_trig_coinc_window +
	    _egmw_config._rteb_window / 2;

	  if (ts_sort_until > max_rteb_back)
	    eb_sort_until = ts_sort_until - max_rteb_back;

	  lwroc_accum_sort_until(_accum_rteb_many, eb_sort_until, NULL);

	  _item_state._ts_sorted_until = ts_sort_until;

	  _item_state._ts_max_seen_backwards = 0;
	}
    }

  _item_state._ts_max_seen = 0;

  /* printf ("%" PRId64 "\r", ts_last / 1000000000); fflush(stdout); */
}

void item_process_remaining(void)
{
  lwroc_accum_sort_until(_accum_many, (uint64_t) -1, NULL);

  lwroc_accum_sort_until(_accum_rteb_many, (uint64_t) -1, NULL);

#ifndef NO_EXT_DATA
  if (_ext_data_filled)
    send_ext_data();
#endif
}

void item_summary(void)
{
  if (_egmw_config._synthetic_data_check)
    {
      fprintf(stdout,
	      "Synthetic checks: bad %" PRIu64 " / %" PRIu64 "\n",
	      _synthetic_data_check_bad,
	      _synthetic_data_check_count);
    }
}
