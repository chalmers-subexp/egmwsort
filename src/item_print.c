/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#include <stdio.h>
#include <inttypes.h>
#include <string.h>

#include "message.h"
#include "config.h"
#include "item.h"
#include "item_state.h"

#include "ebye_record.h"
#include "hit_out.h"

/* First and second level. */

void item_febex_name_id(char *name, size_t n,
			uint32_t id, uint32_t extra_id)
{
  uint32_t sfp   = (id >> 8) & 0x03;
  uint32_t board = (id >> 4) & 0x0f;
  uint32_t ch    = (id     ) & 0x0f;
  uint32_t idx   = extra_id;

  snprintf (name, n, "FEBEX %d/%02d/%02d:%d", sfp, board, ch, idx);
}

void item_asic_name_id(char *name, size_t n,
		       uint32_t id, uint32_t extra_id)
{
  uint32_t module = (id >> 11) & 0x3f;
  uint32_t asic_i = (id >>  7) & 0x0f;
  uint32_t ch     = (id      ) & 0x7f;
  (void) extra_id;

  snprintf (name, n, "ASIC mod %02d/%02d/%03d", module, asic_i, ch);
}

void item_asic_info_name_id(char *name, size_t n,
			    uint32_t id, uint32_t extra_id)
{
  uint32_t module = (id >>  4) & 0x3f;
  (void) extra_id;

  snprintf (name, n, "ASIC info %02d", module);
}

void item_caen_name_id(char *name, size_t n,
		       uint32_t id, uint32_t extra_id)
{
  uint32_t module = (id >> 6) & 0x1f;
  uint32_t ch     = (id     ) & 0x3f;
  uint32_t idx    = extra_id;

  if (extra_id == (uint32_t) -1)
    snprintf (name, n, "CAEN %02d/%02d", module, ch);
  else
    snprintf (name, n, "CAEN %02d/%02d:%d", module, ch, idx);
}

void item_trig_id(char *name, size_t n,
		  uint32_t id, uint32_t extra_id)
{
  (void) id;
  (void) extra_id;

  snprintf (name, n, "TRIG");
}

void item_pixie_name_id(char *name, size_t n,
			uint32_t id, uint32_t extra_id)
{
  uint32_t crate  = (id >> 8) & 0xf;
  uint32_t slot   = (id >> 4) & 0xf;
  uint32_t ch     = (id     ) & 0xf;
  (void) extra_id;

  snprintf (name, n, "PIXIE %02d/%02d/%02d", crate, slot, ch);
}

void item_name_id(char *name, size_t n,
		  uint32_t id, uint32_t extra_id)
{
  switch (id & ACCUM_ID_TYPE_MASK)
    {
    case ACCUM_ID_TYPE_FEBEX:
      item_febex_name_id(name, n, id, extra_id);
      break;
    case ACCUM_ID_TYPE_ASIC:
      item_asic_name_id(name, n, id, extra_id);
      break;
    case ACCUM_ID_TYPE_ASIC_INFO:
      item_asic_info_name_id(name, n, id, extra_id);
      break;
    case ACCUM_ID_TYPE_CAEN:
      item_caen_name_id(name, n, id, extra_id);
      break;
    case ACCUM_ID_TYPE_TRIG:
      item_trig_id(name, n, id, extra_id);
      break;
    case ACCUM_ID_TYPE_PIXIE:
      item_pixie_name_id(name, n, id, extra_id);
      break;
    default:
      snprintf (name, n, "?");
      FATAL("Unhandled id: %08x", id);
      break;
    }
}

/* Second level. */

void item_name_hb_id(char *name, size_t n,
		     uint32_t id, uint32_t extra_id)
{
  switch (id & ACCUM_ID_TYPE_MASK)
    {
    case ACCUM_ID_TYPE_ELUM:
      /* item_febex_name_id(name, n, id, extra_id); */
      break;
    case ACCUM_ID_TYPE_LUME:
      /* item_asic_name_id(name, n, id, extra_id); */
      break;
    case ACCUM_ID_TYPE_ISIA:
      /* item_asic_info_name_id(name, n, id, extra_id); */
      break;
    case ACCUM_ID_TYPE_CD:
      /* item_caen_name_id(name, n, id, extra_id); */
      break;
    case ACCUM_ID_TYPE_REC:
      /* item_caen_trig_id(name, n, id, extra_id); */
      break;
    default:
      snprintf (name, n, "?");
      FATAL("Unhandled HB id: %08x", id);
      break;
    }
}

/* */

void print_febex_item(int32_t id, accum_item_febex *febex,
		      const char *prefix)
{
  uint32_t sfp   = (id >> 8) & 0x03;
  uint32_t board = (id >> 4) & 0x0f;
  uint32_t ch    = (id     ) & 0x0f;
  int j;

  fprintf (stdout,
	   "%sFEBEX: %d/%02d/%02d ",
	   prefix,
	   sfp, board, ch);
  for (j = 0; j < 4; j++)
    if (!(febex->_mask & (1 << j)))
      fprintf (stdout, "    -");
    else
      fprintf (stdout,
	       " %04x ",
	       febex->_adc_data[j] & 0xffff);
  fprintf (stdout,
	   "  @ %016" PRIx64 "\n",
	   febex->_base._ts);
}

void print_asic_info_item(int32_t id, accum_item_asic_info *asic_info,
			  const char *prefix)
{
  uint32_t module = (id >>  4) & 0x3f;
  uint32_t code   = (id      ) & 0x0f;

  fprintf (stdout,
	   "%sASIC info: %02d  %02d                 @ %016" PRIx64 "\n",
	   prefix,
	   module,
	   code,
	   asic_info->_base._ts);
}

void print_asic_item(int32_t id, accum_item_asic *asic,
		     const char *prefix)
{
  uint32_t module = (id >> 11) & 0x3f;
  uint32_t asic_i = (id >>  7) & 0x0f;
  uint32_t ch     = (id      ) & 0x7f;

  fprintf (stdout,
	   "%sASIC: %02d/%02d/%03d  %04x             @ %016" PRIx64 "\n",
	   prefix,
	   module, asic_i, ch,
	   asic->_adc_data & 0xffff,
	   asic->_base._ts);
}

uint64_t _prev_heimtime     = 0;
int16_t _prev_heimtime_fine = 0;

/* FIne times [0,0x400[, for a scale [0,4[ */

void print_caen_item(int32_t id, accum_item_caen *caen,
		     const char *prefix)
{
  uint32_t module = (id >> 6) & 0x1f;
  uint32_t ch     = (id     ) & 0x3f;
  int j;

  fprintf (stdout,
	   "%sCAEN: %02d/%02d ",
	   prefix,
	   module, ch);
  for (j = 0; j < 4; j++)
    if (!(caen->_mask & (1 << j)))
      fprintf (stdout, "    -");
    else
      fprintf (stdout,
	       " %04x",
	       caen->_adc_data[j] & 0xffff);
  fprintf (stdout,
	   "  @ %016" PRIx64 "\n",
	   caen->_base._ts);

#if 0
  if (module == 1 && ch == 10)
    {
      uint64_t t;
      int16_t t_fine;

      t      = caen->_base._ts;
      t_fine = caen->_adc_data[3] & 0xffff;

      fprintf (stdout,
	       "heimtime: @ %20" PRId64 " %20" PRId64 " f%4d : %20" PRId64 " %20.1f\n",
	       t,
	       (t - _prev_heimtime),
	       (t_fine - _prev_heimtime_fine),
	       (t - _prev_heimtime) * 0x100 + (t_fine - _prev_heimtime_fine),
	       (double) ((t - _prev_heimtime) * 0x100 +
			 (t_fine - _prev_heimtime_fine)) / 0x100);

      _prev_heimtime      = t;
      _prev_heimtime_fine = t_fine;
    }
#endif
}

void print_pixie_item(int32_t id, accum_item_pixie *pixie,
		      const char *prefix)
{
  uint32_t crate  = (id >> 8) & 0xf;
  uint32_t slot   = (id >> 4) & 0xf;
  uint32_t ch     = (id     ) & 0xf;

  fprintf (stdout,
	   "%sPIXIE: %02d/%02d/%03d  %04x .%03x        @ %016" PRIx64 "\n",
	   prefix,
	   crate, slot, ch,
	   pixie->_adc_data & 0xffff,
	   pixie->_ts_frac,
	   pixie->_base._ts);
}
