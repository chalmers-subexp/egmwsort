/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include <math.h>
#include <stdlib.h>

#include "pulser.h"
#include "message.h"

#include "inc_colourtext.h"
#include "parse_util.h"

#define MW_AVG_FRAC_NEW  0.02
#define MW_AVG_FRAC_OLD  (1 - MW_AVG_FRAC_NEW)

#define MW_AVG_GO_GOOD   0.99

typedef struct pulser_item_t
{
  uint64_t _ts;
  uint64_t _ts_prev;

  uint16_t _fine;
  uint16_t _fine_prev;

  struct selfcorr
  {
    double   _tdm;

    /* For the variation/std.dev., we must collect with a bias. */
    double   _td, _td2;
    double   _good;

    uint64_t _loss;
    uint64_t _n;

    int      _ts_unused;
    int      _report_good;
    int      _report_loss;
  } _self;

  struct matching
  {
    double   _td, _td2;
    double   _good;

    uint64_t _loss;
    uint64_t _n;
  } _match;

  uint64_t _pulses;

} pulser_item;

pulser_item _pulser_items[PULSER_ID_NUM];

uint64_t _pulser_expect_id_mask = 0;

uint64_t _pulser_last_ts;

int _pulser_ana_cnt = 0;
int _pulser_header_cnt = 0;
int _pulset_ana_all_expected = 0;

int _pulser_nominal_period = 100000000;

int _pulser_lines = 0; /* Number of lines printed last report. */

int _pulser_message_printed = 0;

void pulser_id_str(char *str, size_t size,
		   int id)
{
  if (id == PULSER_ID_CAEN)
    snprintf (str, size, "c.");
  else if (id == PULSER_ID_MESY)
    snprintf (str, size, "m.");
  else if (id >= PULSER_ID_MESY_MOD_START &&
	   id <  PULSER_ID_MESY_MOD_END)
    snprintf (str, size, "m%x", id - PULSER_ID_MESY_MOD_START);
  else if (id >= PULSER_ID_ASIC_START &&
	   id <  PULSER_ID_ASIC_END)
    snprintf (str, size, "a%x", id - PULSER_ID_ASIC_START);
  else
    snprintf (str, size, "%2d", id);
}

void pulser_usage(void)
{
  printf ("\n");
  printf ("--track-pulser=<OPTIONS>:\n");
  printf ("\n");
  printf ("\n");
  printf ("  period                   Nominal pulser period.\n");
  printf ("\n");
}

void pulser_setup(const char *cfg)
{
  const char *request;
  lwroc_parse_split_list parse_info;

  lwroc_parse_split_list_setup(&parse_info, cfg, ',');

  while ((request = lwroc_parse_split_list_next(&parse_info)) != NULL)
    {
      const char *post;

      if (LWROC_MATCH_C_ARG("help"))
	{
	  pulser_usage();
	  exit(0);
	}
      else if (LWROC_MATCH_C_PREFIX("period=",post)) {
	_pulser_nominal_period = atoi(post);
      } else
	BADCFG("Unrecognised track pulser option: %s", request);
    }
}

void pulser_expect(int id)
{
  _pulser_expect_id_mask |= (1 << id);
}

void pulser_analyse(void)
{
  int id;

  uint64_t good_id_mask = 0;

  _pulser_ana_cnt++;

  /* Analyse the pulsers relative to the reference. */

  for (id = 0; id < PULSER_ID_NUM; id++)
    {
      double td = _pulser_items[id]._ts - _pulser_items[id]._ts_prev +
	((int16_t) (_pulser_items[id]._fine -
		    _pulser_items[id]._fine_prev)) / 256.;

      if (id == 0 && 0)
	printf ("%2d %d %10" PRId64 " %5d %5d\n",
		id,
		_pulser_items[id]._self._ts_unused,
		_pulser_items[id]._ts - _pulser_items[id]._ts_prev,
		_pulser_items[id]._fine_prev,
		_pulser_items[id]._fine);

      /* Slowly sink, in case we get bad value. */
      _pulser_items[id]._self._good *= MW_AVG_FRAC_OLD;

      if (td > _pulser_nominal_period - _pulser_nominal_period / 100 &&
	  td < _pulser_nominal_period + _pulser_nominal_period / 100 &&
	  _pulser_items[id]._self._ts_unused)
	{
	  /* We had a good pulse, so keep the average floating. */
	  _pulser_items[id]._self._good += MW_AVG_FRAC_NEW;

	  if (!_pulser_items[id]._self._n)
	    {
	      _pulser_items[id]._self._tdm = td;

	      td -= _pulser_nominal_period - _pulser_nominal_period / 100;

	      _pulser_items[id]._self._td = td;
	      _pulser_items[id]._self._td2 = td * td;

	      _pulser_items[id]._self._good = 1; /* First pulse - good! */
	    }
	  else
	    {
	      _pulser_items[id]._self._tdm =
		MW_AVG_FRAC_OLD * _pulser_items[id]._self._tdm +
		MW_AVG_FRAC_NEW * td;

	      td -= _pulser_nominal_period - _pulser_nominal_period / 100;

	      _pulser_items[id]._self._td =
		MW_AVG_FRAC_OLD * _pulser_items[id]._self._td +
		MW_AVG_FRAC_NEW * td;
	      _pulser_items[id]._self._td2 =
		MW_AVG_FRAC_OLD * _pulser_items[id]._self._td2 +
		MW_AVG_FRAC_NEW * td * td;
	    }

	  _pulser_items[id]._self._n++;

	  _pulser_items[id]._self._ts_unused = 0;

	  if (!_pulser_items[id]._self._report_good &&
	      _pulser_items[id]._self._good > MW_AVG_GO_GOOD)
	    {
	      char str[32];

	      pulser_id_str(str, sizeof (str), id);
	      WARNING("Cycle #%d: "
		      "Pulser %s recovered (%d pulses lost).",
		      _pulser_ana_cnt,
		      str, _pulser_items[id]._self._report_loss);

	      _pulser_items[id]._self._report_good = 1;
	      _pulser_items[id]._self._report_loss = 0;
	    }

	  good_id_mask |= (1 << id);
	}
      else
	{
	  _pulser_items[id]._self._loss++;

	  if (_pulser_items[id]._self._report_good)
	    {
	      char str[32];

	      pulser_id_str(str, sizeof (str), id);
	      ERROR("Cycle #%d: "
		    "Pulser %s loss.",
		    _pulser_ana_cnt,
		    str);
	    }

	  _pulser_items[id]._self._report_good = 0;
	  _pulser_items[id]._self._report_loss++;
	}
    }

  if ((_pulser_expect_id_mask & good_id_mask) ==
      _pulser_expect_id_mask)
    _pulset_ana_all_expected++;

  /* id = 0 is reference */
  for (id = 1; id < PULSER_ID_NUM; id++)
    {
      /* Slowly sink, in case we get no value. */
      _pulser_items[id]._match._good *= MW_AVG_FRAC_OLD;

      if (_pulser_items[id]._ts >
	  _pulser_items[0]._ts - _pulser_nominal_period / 100 &&
	  _pulser_items[id]._ts <
	  _pulser_items[0]._ts + _pulser_nominal_period / 100)
	{
	  double td =
	    (int64_t) (_pulser_items[id]._ts -
		       _pulser_items[0]._ts) +
	    ((int16_t) (_pulser_items[id]._fine -
			_pulser_items[id]._fine_prev)) / 256.;

	  /* We had a good pulse, so keep the average floating. */
	  _pulser_items[id]._match._good += MW_AVG_FRAC_NEW;

	  if (!_pulser_items[id]._match._n)
	    {
	      _pulser_items[id]._match._td = td;
	      _pulser_items[id]._match._td2 = td * td;

	      _pulser_items[id]._match._good = 1; /* First pulse - good! */
	    }
	  else
	    {
	      _pulser_items[id]._match._td =
		MW_AVG_FRAC_OLD * _pulser_items[id]._match._td +
		MW_AVG_FRAC_NEW * td;
	      _pulser_items[id]._match._td2 =
		MW_AVG_FRAC_OLD * _pulser_items[id]._match._td2 +
		MW_AVG_FRAC_NEW * td * td;
	    }

	  _pulser_items[id]._match._n++;
	}
      else
	_pulser_items[id]._match._loss++;
    }
}

void pulser_report(void)
{
  int id;
  int i;

  /* Only overwrite previous lines if no message has been printed
   * since last time.
   */
  if (_message_printed == _pulser_message_printed)
    {
      for (i = 0; i < _pulser_lines; i++)
	printf ("%s", CT_OUT(UP1LINE));
    }

  _pulser_message_printed = _message_printed;

  _pulser_lines = 0;

  if (_pulser_header_cnt <= 0 || 1)
    {
      printf ("ID"
	      "  __Self-correlations (vs. prev)___"
	      "  __Matching (vs. CAEN)____________"
	      "%s\n", CT_OUT(CLR_EOL));
      printf ("  "
	      "      dt [ns]  s.dev  good    #loss"
	      "      dt [ns]  s.dev  good    #loss"
	      "%s\n", CT_OUT(CLR_EOL));

      _pulser_lines += 2;

      _pulser_header_cnt = 5;
    }
  _pulser_header_cnt--;

  for (id = 0; id < PULSER_ID_NUM; id++)
    {
      char str[32];
      int expected = 0; /* fixme */

      if (_pulser_items[id]._self._good < 0.01 &&
	  _pulser_items[id]._match._good < 0.01 &&
	  !expected)
	continue;

      pulser_id_str(str, sizeof (str), id);
      printf ("%s", str);

      printf ("  %11.1f %6.1f %5.2f %8" PRId64"",
	      _pulser_items[id]._self._tdm,
	      sqrt(_pulser_items[id]._self._td2 -
		   _pulser_items[id]._self._td *
		   _pulser_items[id]._self._td),
	      _pulser_items[id]._self._good,
	      _pulser_items[id]._self._loss);

      if (id != 0)
	printf ("  %11.1f %6.1f %5.2f %8" PRId64"",
		_pulser_items[id]._match._td,
		sqrt(_pulser_items[id]._match._td2 -
		     _pulser_items[id]._match._td *
		     _pulser_items[id]._match._td),
		_pulser_items[id]._match._good,
		_pulser_items[id]._match._loss);

      printf ("%s\n", CT_OUT(CLR_EOL));

      _pulser_lines++;
    }

  printf ("\n");
  _pulser_lines++;
}

void pulser_got(int id, uint64_t ts, uint16_t t_fine)
{
  /* If the timestamp gotten (for whatever pulser) is significantly
   * later than the previous timestamp of a pulser, then it cannot
   * really match.
   *
   * If pulsers are aligned, it means that by analysing the data we
   * got previously for all pulsers, whoever is present should be
   * aligned.  Whoever is missing, is lost.
   *
   * If pulsers are not aligned, we will accumulate loss counts.
   */

  if (ts > _pulser_last_ts + _pulser_nominal_period / 100)
    {
      pulser_analyse();

      if (_pulser_ana_cnt % 100 == 0)
	pulser_report();
    }

  /* Insert the new data gotten. */

  _pulser_items[id]._ts_prev = _pulser_items[id]._ts;
  _pulser_items[id]._ts = ts;

  _pulser_items[id]._fine_prev = _pulser_items[id]._fine;
  _pulser_items[id]._fine = t_fine;

  _pulser_items[id]._pulses++;

  _pulser_items[id]._self._ts_unused = 1;

  _pulser_last_ts = ts;

  /*
  printf ("t10: %d @ %016" PRIx64 " (d %010" PRIu64 ")\n",
	  id,
	  _pulser_items[id]._ts,
	  _pulser_items[id]._ts - _pulser_items[id]._ts_prev);
  */
}

void pulser_summary(void)
{
  int id;

  /* Report any final pulser info. */

  for (id = 0; id < PULSER_ID_NUM; id++)
    {
      if (_pulser_items[id]._self._report_loss &&
	  (_pulser_expect_id_mask & (1 << id)))
	{
	  char str[32];

	  pulser_id_str(str, sizeof (str), id);
	  WARNING("Cycle #%d: "
		  "Pulser %s, %d pulses lost at end.",
		  _pulser_ana_cnt,
		  str, _pulser_items[id]._self._report_loss);
	}
    }

  INFO("Cycle #%d: "
       "%d good.",
       _pulser_ana_cnt,
       _pulset_ana_all_expected);

}
