/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* Code that would normally be elsewhere, but that we for simplicity
 * just compile together.
 */

#define EXTERNAL_WRITER_NO_SHM 1
#define __LAND02_CODE__

#define EXT_WRITER_PREFIX "../../ucesb/hbook/"

#include "external_writer.cc"
#include "forked_child.cc"
#include "markconvbold.cc"
#include "colourtext.cc"
#include "error.cc"
