/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#include <ctype.h>
#include <stdlib.h>

#include "message.h"
#include "input_file.h"

int compare_file_name_numbers(const char *a, const char *b)
{
  /* Compare lexicographically.  Except when a number is reached. */

  for ( ; ; )
    {
      if (*a == '\0')
	return (*b == '\0') ? 0 : -1;
      if (*b == '\0')
	return 1;

      if (isdigit(*a) && isdigit(*b))
	{
	  char *end_a, *end_b;
	  long num_a, num_b;

	  num_a = strtol(a, &end_a, 10);
	  num_b = strtol(b, &end_b, 10);

	  if (num_a != num_b)
	    return (num_a < num_b) ? -1 : 1;

	  a = end_a;
	  b = end_b;
	}

      if (*a != *b)
	return (*a < *b) ? -1 : 1;

      a++;
      b++;
    }
}


int compare_input_option(const void *ptr_a, const void *ptr_b)
{
  input_option *a = *((input_option **) ptr_a);
  input_option *b = *((input_option **) ptr_b);

  /* Only if both are filenames, we compare by the actual name. */
  if (a->_type == INPUT_OPTION_TYPE_FILE &&
      b->_type == INPUT_OPTION_TYPE_FILE)
    return compare_file_name_numbers(a->_name, b->_name);

  /* Otherwise, keep order. */
  if (a < b)
    return -1;
  return (a > b);
}

void sort_input_files(void)
{
  input_option **ptr_array;
  pd_ll_item *iter;
  size_t n;
  size_t i;

  /* The list of input files is a linked list.
   * That makes allocation easy, but sorting troublesome.
   * Either we would have to implement a merge sort.
   * Or we create a temporary pointer array and do a quicksort.
   */

  n = 0;
  PD_LL_FOREACH(_input_options, iter)
    n++;

  ptr_array = (input_option **) malloc(n * sizeof (input_option *));

  if (!ptr_array)
    FATAL_MEMORY("sort input item ptr array");

  i = 0;
  PD_LL_FOREACH(_input_options, iter)
    {
      input_option *item =
	PD_LL_ITEM(iter, input_option, _options);

      ptr_array[i++] = item;
    }

  if (n > 1)
    INFO("Sorting %zd input file names.", n);

  /* Do the sorting. */

  qsort(ptr_array, n, sizeof (input_option *),
	compare_input_option);

  /* Put back into linked list. */

  PD_LL_INIT(&_input_options);

  for (i = 0; i < n; i++)
    {
      input_option *item = ptr_array[i];

      PD_LL_ADD_BEFORE(&_input_options, &item->_options);
    }
}
