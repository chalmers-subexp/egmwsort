/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#ifndef __LDF_RECORD_H__
#define __LDF_RECORD_H__

#include <stdint.h>

extern char *_ldf_record_buffer;

extern int _record_pixie_num_crate_slot[16][16];

/* Process data in one record. */
int ldf_read_record_items(char *p, uint32_t remain);

/* Read (and process) one record from file. */
int ldf_read_record(int fd);

/* All records have been read, process remaining. */
void ldf_records_done(void);

void ldf_records_summary(void);

#endif/*__LDF_RECORD_H__*/
