/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <stdio.h>

typedef struct egmw_config
{
  int _print_record;

  int _accum_sort;

  int _print_raw_data;
  int _print_hit_data;
  int _print_sort_hit_data;

  int _format_out_hits;

  int _fill_ext_data_raw;
  int _fill_ext_data_rteb;

  int _synthetic_data_check;

  int _track_pulser;
  const char * _track_pulser_cfg;

  unsigned int _rteb_trig_coinc_window;
  unsigned int _rteb_window;

  const char *_expect_subsystem_spec;

} egmw_config_t;

extern egmw_config_t _egmw_config;

extern int _stderr_isatty;

extern FILE *_dump_fid;

extern const char *_out_filename;
extern const char *_dump_filename;

#endif/*__CONFIG_H__*/
