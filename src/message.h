/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#if LWROC_MESSAGE

#define BUG      LWROC_BUG_XFMT
#define FATAL    LWROC_FATAL_XFMT
#define BADCFG   LWROC_BADCFG_XFMT
#define ERROR    LWROC_ERROR_XFMT
#define WARNING  LWROC_WARNING_XFMT
#define INFO     LWROC_INFO_XFMT

static void message_rate_limit_begin(void) { }
static void message_rate_limit_end(void)   { }

#else

#if NO_LWROC_MESSAGE

#define LWROC_BUG_FMT      BUG
#define LWROC_FATAL_FMT    FATAL
#define LWROC_BADCFG_FMT   BADCFG
#define LWROC_ERROR_FMT    ERROR
#define LWROC_WARNING_FMT  WARNING
#define LWROC_INFO_FMT     INFO

#define LWROC_BUG          BUG
#define LWROC_FATAL        FATAL
#define LWROC_BADCFG       BADCFG
#define LWROC_ERROR        ERROR
#define LWROC_WARNING      WARNING
#define LWROC_INFO         INFO

#endif

#define MSGLVL_SIGNAL    1
#define MSGLVL_BUG       2
#define MSGLVL_FATAL     3
#define MSGLVL_BADCFG    4
#define MSGLVL_ERROR     5
#define MSGLVL_WARNING   6
#define MSGLVL_INFO      7
#define MSGLVL_LOG       8
#define MSGLVL_DEBUG     9
#define MSGLVL_SPAM      10
#define MSGLVL_MAX       MSGLVL_SPAM

extern int _message_printed;

void message_rate_limit_begin(void);
void message_rate_limit_end(void);

void message_internal(int level,
		      const char *file, int line,
		      const char *fmt,...)
  __attribute__ ((__format__ (__printf__, 4, 5)));

#define BUG(...)							\
  do { message_internal(MSGLVL_BUG,				\
			__FILE__,__LINE__,__VA_ARGS__);			\
    exit(1); } while (0)

#define FATAL(...)							\
  do { message_internal(MSGLVL_FATAL,					\
			__FILE__,__LINE__,__VA_ARGS__);			\
    exit(1); } while (0)

#define BADCFG(...)							\
  do { message_internal(MSGLVL_BADCFG,					\
			__FILE__,__LINE__,__VA_ARGS__);			\
    exit(1); } while (0)

#define ERROR(...)							\
  do { message_internal(MSGLVL_ERROR,					\
			__FILE__,__LINE__,__VA_ARGS__); } while (0)

#define WARNING(...)							\
  do { message_internal(MSGLVL_WARNING,					\
			__FILE__,__LINE__,__VA_ARGS__); } while (0)

#define INFO(...)							\
  do { message_internal(MSGLVL_INFO,					\
			__FILE__,__LINE__,__VA_ARGS__); } while (0)

#endif

#define FATAL_MEMORY(purpose) do {			\
    FATAL("Memory allocation error: %s\n", purpose);	\
  } while (0)

#endif/*__MESSAGE_H__*/
