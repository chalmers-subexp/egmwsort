/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hit_out.h"
#include "ebye_record_out.h"
#include "ldf_record_out.h"
#include "message.h"

uint64_t _prev_ts_mid_hi = 0;

void hit_out_raw(accum_item_raw *raw, void *arg)
{
  int new_record;

  uint32_t *pout;

  size_t size = raw->_raw_words * sizeof (uint32_t);

  pout =
    (uint32_t *) ebye_request_output(size, &new_record, arg);

  memcpy(pout, raw->_raw, size);

  ebye_used_output(size, arg);
}

void hit_out_febex(uint32_t id, accum_item_febex *febex, void *arg)
{
  int new_record;

  uint32_t *pout, *u32;

  int j;

  uint64_t ts = febex->_base._ts;

  uint32_t ts_low =  ts        & 0x0fffffff;
  uint32_t ts_mid = (ts >> 28) & 0x000fffff;
  uint32_t ts_hi  = (ts >> 48) & 0x000fffff;

  uint64_t ts_mid_hi = (febex->_base._ts >> 28) & 0xffffffffffll;

  uint32_t sfp   = (id >> 8) & 0x03;
  uint32_t board = (id >> 4) & 0x0f;
  uint32_t ch    = (id     ) & 0x0f;

  /* Worst case: 4 data_idx items, plus 2 timestamps. */
  pout =
    (uint32_t *) ebye_request_output(6 * 2 * sizeof (uint32_t),
				     &new_record, arg);
  u32 = pout;

  /* Always emit both timestamps to make data look like the normal
   * format.
   *
   * It is a future todo to optimise this.  Requires all tools to
   * handle such data.
   */
  if (/*new_record ||
	ts_mid_hi != _prev_ts_mid_hi*/
      1)
    {
      u32[0] =
	0x80000000 |                 /* 31..30  Info type marker     */
	(7 << 20) |                  /* 23..20  info type            */
	ts_mid;                      /* 19.. 0 (timestamp 47..28)    */
      u32[1] =
	ts_low;                      /* 27.. 0  timestamp            */
      u32[2] =
	0x80000000 |                 /* 31..30  Info type marker     */
	(8 << 20) |                  /* 23..20  info type            */
	ts_hi;                       /* 15.. 0 (timestamp 63..48)    */
      u32[3] =
	ts_low;                      /* 27.. 0  timestamp            */

      u32 += 4;

      _prev_ts_mid_hi = ts_mid_hi;
    }

  for (j = 0; j < 4; j++)
    {
      if (!(febex->_mask & (1 << j)))
	continue;

      uint32_t pileup = (febex->_adc_data[j] >> 17) & 1;
      uint32_t clip   = (febex->_adc_data[j] >> 16) & 1;
      uint32_t data   = (febex->_adc_data[j]      ) & 0xffff;

      u32[0] =
	0xc0000000 |                 /* 31..30  FEBEX type marker    */
	(pileup << 29) |             /*     29  pileup               */
	(clip << 28) |               /*     28  clip                 */
	(sfp << 26) |                /* 27..26  channel hi:  sfp     */
	(board << 22) |              /* 25..22  channel mid: board   */
	(j << 20) |                  /* 21..20  channel mid: data id */
	(ch << 16) |                 /* 19..16  channel low: channel */
	data;
      u32[1] =
	ts_low;                      /* 27.. 0  timestamp            */

      u32 += 2;
    }

  ebye_used_output(((char *) u32) - ((char *) pout), arg);
}

void hit_out_asic(uint32_t id, accum_item_asic *asic, void *arg)
{
  int new_record;

  uint32_t *pout, *u32;

  uint64_t ts = asic->_base._ts;

  uint32_t ts_low =  ts        & 0x0fffffff;
  uint32_t ts_mid = (ts >> 28) & 0x000fffff;
  uint32_t ts_hi  = (ts >> 48) & 0x000fffff;

  uint64_t ts_mid_hi = (asic->_base._ts >> 28) & 0xffffffffffll;

  uint32_t module = (id >> 11) & 0x3f;
  uint32_t asic_i = (id >>  7) & 0x0f;
  uint32_t ch     = (id      ) & 0x7f;

  /* Worst case: 1 data_idx items, plus 2 timestamps. */
  pout =
    (uint32_t *) ebye_request_output(3 * 2 * sizeof (uint32_t),
				     &new_record, arg);
  u32 = pout;

  /* Always emit both timestamps to make data look like the normal
   * format.
   *
   * It is a future todo to optimise this.  Requires all tools to
   * handle such data.
   */
  if (/*new_record ||
	ts_mid_hi != _prev_ts_mid_hi*/
      1)
    {
      u32[0] =
	0x80000000 |                 /* 31..30  Info type marker     */
	(5 << 20) |                  /* 23..20  info type            */
	ts_hi;                       /* 15.. 0 (timestamp 63..48)    */
      u32[1] =
	(1 << 28) |                  /*     28  ASIC extra mark      */
	ts_low;                      /* 27.. 0  timestamp            */
      u32[2] =
	0x80000000 |                 /* 31..30  Info type marker     */
	(7 << 20) |                  /* 23..20  info type            */
	ts_mid;                      /* 19.. 0 (timestamp 47..28)    */
      u32[3] =
	(1 << 28) |                  /*     28  ASIC extra mark      */
	ts_low;                      /* 27.. 0  timestamp            */

      u32 += 4;

      _prev_ts_mid_hi = ts_mid_hi;
    }

  {
    uint32_t pileup = (asic->_adc_data >> 17) & 1;
    uint32_t data   = (asic->_adc_data      ) & 0xfff;

    u32[0] =
      (3 << 30) |                    /* 31..30  ASIC type marker     */
      (pileup << 29) |               /*     29  pileup               */
      (module << 23) |               /* 28..23  channel high: module */
      (asic_i << 19) |               /* 22..19  channel mid: asic    */
      (ch << 12) |                   /* 18..12  channel mid: ch      */
      data;                          /* 11.. 0  ADC value            */
    u32[1] =
      (1 << 28) |                    /*     28  ASIC extra mark      */
      ts_low;                        /* 27.. 0  timestamp            */

    u32 += 2;
  }

  ebye_used_output(((char *) u32) - ((char *) pout), arg);
}

void hit_out_asic_info(uint32_t id, accum_item_asic_info *asic_info,
		       void *arg)
{
  int new_record;

  uint32_t *pout, *u32;

  uint64_t ts = asic_info->_base._ts;

  uint32_t ts_low =  ts        & 0x0fffffff;
  uint32_t ts_mid = (ts >> 28) & 0x000fffff;
  uint32_t ts_hi  = (ts >> 48) & 0x000fffff;

  uint64_t ts_mid_hi = (asic_info->_base._ts >> 28) & 0xffffffffffll;

  uint32_t module = (id >>  4) & 0x3f;
  uint32_t code   = (id      ) & 0x0f;

  /* Worst case: 2 timestamps. */
  pout =
    (uint32_t *) ebye_request_output(2 * 2 * sizeof (uint32_t),
				     &new_record, arg);
  u32 = pout;

  /* Always emit both timestamps to make data look like the normal
   * format.
   *
   * It is a future todo to optimise this.  Requires all tools to
   * handle such data.
   */
  if (/*new_record ||
	ts_mid_hi != _prev_ts_mid_hi*/
      1)
    {
      u32[0] =
	0x80000000 |                 /* 31..30  Info type marker     */
	(5 << 20) |                  /* 23..20  info type            */
	ts_hi;                       /* 15.. 0 (timestamp 63..48)    */
      u32[1] =
	(1 << 28) |                  /*     28  ASIC extra mark      */
	ts_low;                      /* 27.. 0  timestamp            */
      u32[2] =
	0x80000000 |                 /* 31..30  Info type marker     */
	(module << 24) |             /* 29..24  module               */
	(code << 20) |               /* 23..20  info type            */
	ts_mid;                      /* 19.. 0 (timestamp 47..28)    */
      u32[3] =
	(1 << 28) |                  /*     28  ASIC extra mark      */
	ts_low;                      /* 27.. 0  timestamp            */

      u32 += 4;

      _prev_ts_mid_hi = ts_mid_hi;
    }

  ebye_used_output(((char *) u32) - ((char *) pout), arg);
}

void hit_out_caen(uint32_t id, accum_item_caen *caen, void *arg)
{
  int new_record;

  uint32_t *pout, *u32;

  int j;

  uint64_t ts = caen->_base._ts / 4; /* The time is stored in units of 4 ns. */

  uint32_t ts_low =  ts        & 0x0fffffff;
  uint32_t ts_mid = (ts >> 28) & 0x000fffff;
  uint32_t ts_hi  = (ts >> 48) & 0x000fffff;

  uint64_t ts_mid_hi = (caen->_base._ts >> 28) & 0xffffffffffll;

  uint32_t module = (id >> 6) & 0x1f;
  uint32_t ch     = (id     ) & 0x3f;

  /* Worst case: 4 data_idx items, plus 2 timestamps. */
  pout =
    (uint32_t *) ebye_request_output(6 * 2 * sizeof (uint32_t),
				     &new_record, arg);
  u32 = pout;

  /* Always emit both timestamps to make data look like the normal
   * format.
   *
   * It is a future todo to optimise this.  Requires all tools to
   * handle such data.
   */
  if (/*new_record ||
	ts_mid_hi != _prev_ts_mid_hi*/
      1)
    {
      u32[0] =
	0x80000000 |                 /* 31..30  Info type marker     */
	(module << 24) |             /* 29..24  channel high: module */
	(4 << 20) |                  /* 23..20  info type            */
	(ts_mid);                    /* 15.. 0 (timestamp 47..28)    */
      u32[1] =
	ts_low;                      /* 27.. 0  timestamp (same as below!) */

      (void) ts_hi;

      u32 += 2;

      _prev_ts_mid_hi = ts_mid_hi;
    }

  for (j = 0; j < 4; j++)
    {
      if (!(caen->_mask & (1 << j)))
	continue;

      uint32_t pileup = (caen->_adc_data[j] >> 17) & 1;
      uint32_t data   = (caen->_adc_data[j]      ) & 0xffff;

      u32[0] =
	0xc0000000 |                 /* 31..30  CAEN type marker     */
	(pileup << 29) |             /*     29  pileup               */
	(module << 24) |             /* 28..24  channel high: module */
	(j << 22) |                  /* 23..22  channel mid: data id */
	(ch << 16) |                 /* 21..16  channel low: channel */
	(data);                      /* 15.. 0  ADC value            */
      u32[1] =
	ts_low;                      /* 27.. 0  timestamp            */

      u32 += 2;
    }

  ebye_used_output(((char *) u32) - ((char *) pout), arg);
}

#ifdef NO_LDF_DATA
void hit_out_pixie(uint32_t id, accum_item_pixie *pixie, void *arg)
{
  (void) id;
  (void) pixie;
  (void) arg;
}
#else
void hit_out_pixie(uint32_t id, accum_item_pixie *pixie, void *arg)
{
  uint32_t *pout, *u32;

  uint64_t ts = pixie->_base._ts;

  /* Internal timestamp is in ns. */
  /* Pixie timestamp is in clock cycles, so 4 ns for 250 MHz modules. */
  uint32_t ts_hi_fine =  ts        & 0x00000003;
  uint32_t ts_low     = (ts >>  2) & 0xffffffff;
  uint32_t ts_hi      = (ts >> 34) & 0x0000ffff;

  uint32_t t_frac     = (ts_hi_fine << 12) | (pixie->_ts_frac & 0x0fff);

  uint32_t adc_data   = pixie->_adc_data & 0xffff;

  uint32_t crate = (id >> 8) & 0x0f;
  uint32_t slot  = (id >> 4) & 0x0f;
  uint32_t ch    = (id     ) & 0x0f;

  int vsn = (crate << 4) | slot;

  /* Generate the minimum data, fixed 4 words. */
  pout =
    (uint32_t *) ldf_request_output(vsn, 4 * sizeof (uint32_t), arg);
  u32 = pout;

  u32[0] =
    (0 << 31)  |                 /*     31  Finish code.         */
    (4 << 17)  |                 /* 30..17  Event length.        */
    (4 << 12)  |                 /* 16..12  Header length.       */
    (crate << 8) |               /* 11.. 8  Crate.               */
    (slot << 4) |                /*  7.. 4  Slot.                */
    (ch);                        /*  3.. 0  Channel.             */
  u32[1] =
    ts_low;                      /* 31.. 0  timestamp            */
  u32[2] =
    (0 << 31)  |                 /*     31  CFD forced trig.     */
    (0 << 30)  |                 /*     30  CFD trigger.         */
    (t_frac << 16) |             /* 31..16  Fractional time.     */
    ts_hi;                       /* 15.. 0 (timestamp 47..32)    */
  u32[3] =
    (0 << 31)  |                 /*     31  Trace out-of-range.  */
    (0 << 16)  |                 /* 30..16  Trace length.        */
    adc_data;                    /* 15.. 0  ADC value.           */

  u32 += 4;

  ldf_used_output(vsn, ((char *) u32) - ((char *) pout), arg);
}
#endif

#define RANGE_CHECK(var,maxval) do {				\
    if (var < 0 || var > maxval)				\
      ERROR("Argument %s=%d in '%s' outside range [0,%d].",	\
	    #var, var, __func__, maxval);			\
  } while (0)

void format_hit_asic(int mod, int asic_i, int ch,
		     int adc, uint64_t ref_t)
{
  accum_item_asic asic;
  uint32_t id;

  RANGE_CHECK(mod,    0x3f);
  RANGE_CHECK(asic_i, 0x0f);
  RANGE_CHECK(ch,     0x7f);

  id = (mod << 11) | (asic_i << 7) | ch;

  asic._base._ts = ref_t;

  asic._adc_data = adc;

  hit_out_asic(id, &asic, NULL);
}

void format_hit_caen(int mod, int ch,
		     int adc, int adc2, int t_fine, uint64_t ref_t)
{
  accum_item_caen caen;
  uint32_t id;

  RANGE_CHECK(mod,    0x1f);
  RANGE_CHECK(ch,     0x3f);

  id = (mod << 6) | ch;

  caen._base._ts = ref_t * 4;

  caen._mask = 0;

  caen._mask |= (1 << 0);  caen._adc_data[0] = adc;
  caen._mask |= (1 << 1);  caen._adc_data[1] = adc2;
  /* */                    caen._adc_data[2] = 0; /* avoid warning */
  caen._mask |= (1 << 3);  caen._adc_data[3] = t_fine;

  hit_out_caen(id, &caen, NULL);
}

uint8_t synth_check_caen(uint64_t ts, uint32_t adc_data, uint32_t pileup)
{
  uint64_t check_64;

  check_64 = ts + adc_data + (pileup << 16);

  check_64 = (check_64 >> 32) ^ check_64;
  check_64 = (check_64 >> 16) ^ check_64;
  check_64 = (check_64 >>  8) ^ check_64;
  check_64 = (check_64 >>  4) ^ check_64;
  check_64 = (check_64 >>  2) ^ check_64;

  return (uint8_t) (check_64 & 0x3);
}

void format_hit_pixie(int crate, int slot, int ch,
		      int adc, int t_fine, uint64_t ref_t)
{
  accum_item_pixie pixie;
  uint32_t id;

  RANGE_CHECK(crate,  0xf);
  RANGE_CHECK(slot,   0xf);
  RANGE_CHECK(ch,     0xf);

  id = (crate << 8) | (slot << 4) | ch;

  pixie._base._ts = ref_t * 4;

  pixie._adc_data = adc;
  pixie._ts_frac = t_fine;

  hit_out_pixie(id, &pixie, NULL);
}
