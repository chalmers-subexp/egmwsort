/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#include <byteswap.h>
#include <arpa/inet.h>

#include "ebye_xfer_header.h"

#include "message.h"
#include "ebye_record_out.h"
#include "ldf_record_out.h"
#include "hit_out.h"

#define MATCH_C_PREFIX(prefix,post)               \
  (strncmp(request,prefix,strlen(prefix)) == 0 && \
   *(post = request + strlen(prefix)) != '\0')
#define MATCH_C_ARG(name) (strcmp(request,name) == 0)

void ebye_gen_usage(char *cmdname)
{
  printf ("EBYE XFER block data generator\n");
  printf ("\n");
  printf ("Usage: %s <options>\n", cmdname);
  printf ("\n");
  printf ("  --ldf                    Generate LDF format data (instead of EBYE).\n");
  printf ("  --flush                  Flush buffers after each hit.\n");
  printf ("  --synth-ch=N             Channel to generate.\n");
  printf ("\n");
}

int main(int argc, char *argv[])
{
  int ldf = 0;
  int flush = 0;
  int i;

  int check_crate = 0;
  int check_mod = 1;
  int check_ch  = 10;
  uint8_t check_adc = 0;
  uint8_t check_adc2 = 0;
  uint8_t check_t_fine = 0;

  uint64_t ts = 0x456789123ll;

  /* colourtext_init(); */

  for (i = 1; i < argc; i++)
    {
      char *request = argv[i];
      char *post;

      if (MATCH_C_ARG("--help")) {
	ebye_gen_usage(argv[0]);
        exit(0);
      } else if (MATCH_C_ARG("--ldf")) {
	ldf = 1;
      } else if (MATCH_C_ARG("--flush")) {
	flush = 1;
      } else if (MATCH_C_PREFIX("--synth-ch=",post)) {
	check_ch = atoi(post);
      } else {
	BADCFG("Unrecognized or invalid option: '%s'.",
	       request);
      }
    }

  if (!ldf)
    ebye_init_output(STDOUT_FILENO, 0 /* xfer headers */);
  else
    ldf_init_output(STDOUT_FILENO);

  for ( ; ; )
    {
      uint32_t adc    = 0x123;
      uint32_t adc2   = 0;
      uint32_t t_fine = 0;

      ts++;

      uint32_t write_adc    = (adc    << 2) | check_adc;
      uint32_t write_adc2   = (adc2   << 2) | check_adc2;
      uint32_t write_t_fine = (t_fine << 2) | check_t_fine;

      if (!ldf)
	format_hit_caen(check_mod, check_ch,
			write_adc, write_adc2, write_t_fine, ts);
      else
	format_hit_pixie(check_crate, check_mod, check_ch,
			 write_adc, write_t_fine, ts);

      check_adc    = synth_check_caen(ts, write_adc, 0);
      check_adc2   = synth_check_caen(ts, write_adc2, 0);
      check_t_fine = synth_check_caen(ts, write_t_fine, 0);

      /* Flushing means to write large buffers with just one hit
       * each...
       */
      if (flush)
	{
	  if (!ldf)
	    ebye_flush_output();
	  else
	    ldf_flush_output();
	}
    }

  return 0;
}

/********************************************************************/
