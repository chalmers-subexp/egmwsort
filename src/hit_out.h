/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#ifndef __HIT_OUT_H__
#define __HIT_OUT_H__

#include <stdint.h>

#include "item.h"

void hit_out_raw(accum_item_raw *raw, void *arg);

void hit_out_febex(uint32_t id, accum_item_febex *febex, void *arg);

void hit_out_asic(uint32_t id, accum_item_asic *asic, void *arg);

void hit_out_asic_info(uint32_t id, accum_item_asic_info *asic_info,
		       void *arg);

void hit_out_caen(uint32_t id, accum_item_caen *caen, void *arg);

void hit_out_pixie(uint32_t id, accum_item_pixie *pixie, void *arg);

void format_hit_asic(int asic_i, int mod, int ch,
		     int adc, uint64_t ref_t);

void format_hit_caen(int mod, int ch,
		     int adc, int adc2, int t_fine, uint64_t ref_t);

uint8_t synth_check_caen(uint64_t ts, uint32_t adc_data, uint32_t pileup);

void format_hit_pixie(int crate, int slot, int ch,
		      int adc, int t_fine, uint64_t ref_t);

#endif/*__HIT_OUT_H__*/
