/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#include "ext_data.h"
#include <string.h>
#include <math.h>
#include <stdio.h>

/* The auto-generated fill functions. */
#include "gen/ext_data_writer.hh"

struct ext_data _ext_data;

int _ext_data_filled = 0;

external_writer *ew;

#define MATCH_C_PREFIX(prefix,post)               \
  (strncmp(request,prefix,strlen(prefix)) == 0 && \
   *(post = request + strlen(prefix)) != '\0')
#define MATCH_C_ARG(name) (strcmp(request,name) == 0)

const char *_ext_data_filename = NULL;
int _ext_data_type = 0;
bool _ext_data_generate_header = false;

void usage_ext_data(void)
{
  printf ("  --root=FILE.root         Output ROOT file.\n");
  printf ("  --struct=...             Struct server (?).\n");
  printf ("  --struct_hh=FILE.h       Header file for struct server data.\n");
}

int match_ext_data_arg(const char *request)
{
  const char *post;

  if (MATCH_C_PREFIX("--root=",post)) {
    _ext_data_filename = post;
    _ext_data_type = NTUPLE_TYPE_ROOT;
  } else if (MATCH_C_PREFIX("--cwn=",post)) {
    _ext_data_filename = post;
    _ext_data_type = NTUPLE_TYPE_CWN;
  } else if (MATCH_C_PREFIX("--struct=",post)) {
    _ext_data_filename = post;
    _ext_data_type = NTUPLE_TYPE_STRUCT;
  } else if (MATCH_C_PREFIX("--struct_hh=",post)) {
    _ext_data_filename = post;
    _ext_data_type = NTUPLE_TYPE_STRUCT_HH;
    _ext_data_generate_header = true;
  } else {
    return 0;
  }
  return 1;
}

void init_ext_data(const char *argv0)
{
  main_argv0 = argv0;

  if (!_ext_data_type)
    return;

  unsigned int opt = NTUPLE_OPT_WRITER_NO_SHM;

  ew = new external_writer();

  ew->init_x(_ext_data_type | NTUPLE_CASE_KEEP, opt,
	    _ext_data_filename,"Title",-1,
	    false,false,false, 0);

  ew->send_file_open(0);

  ew->send_book_ntuple_x(101,"h101","egmw_tree");

  send_offsets_ext_data(ew);

  /* TODO: Would be nicer with a clear function, to not have to write
   * all memory locations.
   */
  memset(&_ext_data, 0, sizeof (_ext_data));
}

void send_ext_data(void)
{
  if (!ew)
    return;

  send_fill_x_ext_data(ew, _ext_data);

  /* TODO: Would be nicer with a clear function, to not have to write
   * all memory locations.
   */
  memset(&_ext_data, 0, sizeof (_ext_data));

  _ext_data_filled = 0;
}

void close_ext_data(void)
{
  if (!ew)
    return;

  ew->close();

  delete ew;
}
