/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#ifndef __LDF_RECORD_HEADER_H__
#define __LDF_RECORD_HEADER_H__

#include <stdint.h>

/********************************************************************/

typedef struct ldf_record_header_t
{
  uint32_t _type;
  uint32_t _words;     /* Does not include this header. */
} ldf_record_header;

/********************************************************************/

#define LDF_BUFFER_TYPE_DIR   "DIR "
#define LDF_BUFFER_TYPE_HEAD  "HEAD"
#define LDF_BUFFER_TYPE_DATA  "DATA"
#define LDF_BUFFER_TYPE_EOF   "EOF "

/********************************************************************/

typedef struct ldf_chunk_header_t
{
  uint32_t _bytes;     /* Does not include this member.  (I.e. the two
			* following header members are included.)
			*/
  uint32_t _seq_total;
  uint32_t _seq_cur;
} ldf_chunk_header;

/********************************************************************/

typedef struct ldf_spill_vsn_header_t
{
  uint32_t _words;     /* Includes this header. */
  uint32_t _vsn;
} ldf_spill_vsn_header;

/********************************************************************/

#endif/*__LDF_RECORD_HEADER_H__*/
