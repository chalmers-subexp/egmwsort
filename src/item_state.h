/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */


#ifndef __ITEM_STATE_H__
#define __ITEM_STATE_H__

#ifndef BUG2023_HANDLING
# define BUG2023_HANDLING  0
#endif

#define PRINT_HIT_PREFIX         "hit "
#define PRINT_SORT_HIT_PREFIX    "s-hit "

/* First and second-level sorting. */

#define ACCUM_ID_TYPE_FEBEX      0x10000000
#define ACCUM_ID_TYPE_ASIC       0x20000000
#define ACCUM_ID_TYPE_ASIC_INFO  0x30000000
#define ACCUM_ID_TYPE_CAEN       0x40000000
#define ACCUM_ID_TYPE_PIXIE      0x50000000

/* Second-level sorting. */

#define ACCUM_ID_TYPE_ELUM       0x10000000
#define ACCUM_ID_TYPE_LUME       0x20000000
#define ACCUM_ID_TYPE_ISIA       0x30000000
#define ACCUM_ID_TYPE_CD         0x40000000
#define ACCUM_ID_TYPE_REC        0x50000000
#define ACCUM_ID_TYPE_ZD         0x60000000
#define ACCUM_ID_TYPE_MWPC       0x70000000

/* Second-level sorting. */

#define ACCUM_ID_TYPE_TRIG       0xe0000000

/* All levels. */

#define ACCUM_ID_TYPE_MASK       0xf0000000

/* */

#define NUM_ACCUM_IDS_FEBEX      (4  /*sfp*/    * 16 /*board*/ * 16 /*ch*/)
#define NUM_ACCUM_IDS_ASIC       (64 /*module*/ * 16 /*asic*/ * 128 /*ch*/)
#define NUM_ACCUM_IDS_ASIC_INFO  (64 /*module*/ * 16 /*code*/)
#define NUM_ACCUM_IDS_CAEN       (32 /*module*/ * 64 /*ch*/)
#define NUM_ACCUM_IDS_PIXIE      (16 /*crate*/  * 16 /*slot*/  * 16 /*ch*/)

#define NUM_BOARDS_FEBEX         (4  /*sfp*/    * 16 /*board*/)
#define NUM_MODULES_ASIC         (64 /*module*/)
#define NUM_MODULES_ASIC_MOD     (64 /*module*/ * 16 /*asic*/)
#define NUM_MODULES_ASIC_INFO    (64 /*module*/)
#define NUM_MODULES_CAEN         (32 /*module*/)
#define NUM_MODULES_CAEN_GRP     (2  /*module*/) /* 0-15 caen, 16-31 mesytec */
#define NUM_MODULES_PIXIE        (16 /*crate*/  * 16 /*slot*/)

/* These values tell how old values might be seen after seeing a newer
 * value in the previous record of the _same_ category.
 *
 * It is not allowing violation of the strict ordering per each individual
 * channel.
 *
 * It is the minimum amount of delay that has to be applied before the
 * accumulated data of a record can be time-sorted.
 *
 * The ASIC_MI is for mod/info.  Those seem to be much less out-of-order
 * when considered separately.
 */
#define TS_FAR_BACKWARDS_FEBEX   ( 4000 * 1000000ll) /* Worst seen: 2500 ms. */
#define TS_FAR_BACKWARDS_ASIC    (40000 * 1000000ll) /* Worst seen: 20 s. */
#define TS_FAR_BACKWARDS_ASIC_MI (         200000ll) /* Worst seen: 150 us. */
#define TS_FAR_BACKWARDS_CAEN    ( 1000 * 1000000ll) /* Worst seen: 600 ms. */

typedef struct item_ts_far_t
{
  uint64_t _ts;
  uint32_t _id;
  uint32_t _extra_id;
} item_ts_far;

typedef struct item_state_t
{
  /* From info code 7 and 8. (Mark 0 in timestamp word). Used for febex. */
  uint64_t _ts_7_mid;
  uint64_t _ts_8_hi;
  /* From info code 5, 4 and 7. (Mark 1 in timestamp word). Used for asic. */
  uint64_t _ts_5_1_hi;
  uint64_t _ts_7_1_mid;
  uint64_t _ts_x_1_mid; /* x is 4, 14 or 15. */
  /* From info code 4. (Mark 0 in timestamp word). Used for caen. */
  uint64_t _ts_4_mid;

  /* What info items did we have most recently? */
  uint32_t _recent_info;

  uint32_t _prev_type;

  /* Accumulating current item. */
  uint32_t _accum_id;
  union
  {
    lwroc_accum_item_base     _base;  /* For getting at the timestamp. */
    accum_item_febex_raw      _febex_raw;
    accum_item_asic_raw       _asic_raw;
    accum_item_asic_info_raw  _asic_info_raw;
    accum_item_caen_raw       _caen_raw;
  } _accum_item;
  int      _accum_is_ok;

  int      _raw_words;
  uint32_t _raw[MAX_RAW_WORDS];

  uint64_t _ts_sorted_until;

  uint64_t _ts_max_seen;
  int      _ts_max_seen_backwards;

  /* Debugging of timestamp order. */
  struct
  {
    item_ts_far _febex[NUM_BOARDS_FEBEX];
    item_ts_far _asic[NUM_MODULES_ASIC];
    item_ts_far _asic_mod[NUM_MODULES_ASIC_MOD];
    item_ts_far _asic_info[NUM_MODULES_ASIC_INFO];
    item_ts_far _caen[NUM_MODULES_CAEN];
    item_ts_far _caen_grp[NUM_MODULES_CAEN_GRP];
  } _ts_last;

} item_state;

extern item_state _item_state;

extern int _first_in_record;
extern uint64_t _old_first_ts[8];

extern uint64_t _synthetic_data_check_count;
extern uint64_t _synthetic_data_check_bad;

extern uint8_t _synthetic_caen_check[32 /* mod */][64 /*ch */ ][4 /* data_idx */];

#endif/*__ITEM_STATE_H__*/
