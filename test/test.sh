#!/bin/sh

set -e

mkdir -p test/out/

for FILE in R_short_5e R120_5_short
do
    echo "====================" 1>&2
    echo "== $FILE"             1>&2
    echo                        1>&2

    # Just test to see that program can start.
    bin/egmwsort test/$FILE.ebye

    bin/egmwsort --print-raw --print-record test/$FILE.ebye \
		 > test/out/$FILE.raw.txt 2> test/out/$FILE.raw.err.txt

    diff -u test/$FILE.raw.good test/out/$FILE.raw.txt
    diff -u test/$FILE.raw.err.good test/out/$FILE.raw.err.txt

    bin/egmwsort --print-hit --print-sort-hit test/$FILE.ebye \
		 > test/out/$FILE.hit.txt

    diff -u test/$FILE.hit.good test/out/$FILE.hit.txt

    bin/egmwsort test/$FILE.ebye    --output=test/out/$FILE.new
    bin/egmwsort test/$FILE.ebye    --output=test/out/$FILE.format \
		 --format-out-hits

    diff test/out/$FILE.new test/out/$FILE.format

    bin/egmwsort test/out/$FILE.new --output=test/out/$FILE.chk

    diff test/out/$FILE.new test/out/$FILE.chk

    bin/egmwsort test/$FILE.ebye --ext-data-raw --struct=- \
		 > test/out/$FILE.struct

    ../ucesb/hbook/struct_writer - --dump < test/out/$FILE.struct \
				 > test/out/$FILE.struct.txt

    diff -u test/$FILE.struct.txt.good test/out/$FILE.struct.txt

done

echo "====================" 1>&2
echo "== Generator"         1>&2
echo                        1>&2

# Send 1 resize + 4 blocks = 1 + 4*64 kiB.
# Second send skips the resize block (has wrong size).

(../egmwsort/bin/ebye_gen --flush              | dd bs=1k count=257 status=none ; \
 ../egmwsort/bin/ebye_gen --flush --synth-ch=7 | dd bs=1k count=256 skip=1 status=none ; ) \
    | bin/egmwsort - --print-hit --synthetic-check > test/out/test_ebye_gen.txt

diff -u test/test_ebye_gen.good test/out/test_ebye_gen.txt

echo "====================" 1>&2
