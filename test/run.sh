#!/bin/bash

time ../egmwsort/bin/egmwsort < R82_32 > R82_32.presort 2> R82_32.presort.txt

mkdir -p sorted.orig
mkdir -p sorted.presort

time ../MiniballSort/bin/mb_sort \
  -s settings_20230918.dat \
  -c calibration_20230920.dat \
  -r reaction_144Cs_7Li_prelim.dat \
  -d sorted.orig \
  -i R82_32 -f \
    > R82_32.mb_sort.txt

time ../MiniballSort/bin/mb_sort \
  -s settings_20230918.dat \
  -c calibration_20230920.dat \
  -r reaction_144Cs_7Li_prelim.dat \
  -d sorted.presort \
  -i R82_32.presort -f \
    > R82_32.presort.mb_sort.txt
