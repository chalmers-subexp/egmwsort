
TTree *t = new TTree();
t->ReadFile("mb_dump_d3.txt","ch/i:t/l:s/i");
// t->Draw("ch:t","s>=844728&&s<=844732");
t->Draw("ch:t>>h0(1000,9684001700000,9684002500000,512,0,512)");

t->Draw("ch:t>>h1(1000,9684001700000,9684002500000,512,0,512)","s==844728","same");
t->Draw("ch:t>>h2(1000,9684001700000,9684002500000,512,0,512)","s==844729","same");
t->Draw("ch:t>>h3(1000,9684001700000,9684002500000,512,0,512)","s==844730","same");
t->Draw("ch:t>>h4(1000,9684001700000,9684002500000,512,0,512)","s==844731","same");
t->Draw("ch:t>>h5(1000,9684001700000,9684002500000,512,0,512)","s==844732","same");
t->Draw("ch:t>>h6(1000,9684001700000,9684002500000,512,0,512)","s==844733","same");
t->Draw("ch:t>>h7(1000,9684001700000,9684002500000,512,0,512)","s==844734","same");
t->Draw("ch:t>>h8(1000,9684001700000,9684002500000,512,0,512)","s==844735","same");
t->Draw("ch:t>>h9(1000,9684001700000,9684002500000,512,0,512)","s==844736","same");

h0->GetXaxis()->SetTitle("Timestamp");
h0->GetYaxis()->SetTitle("board*16 + channel");
h0->SetStats(0);        h0->SetMarkerStyle(2); h0->SetMarkerSize(.5);
h1->SetMarkerColor( 2); h1->SetMarkerStyle(2); h1->SetMarkerSize(.5);
h2->SetMarkerColor(3 ); h2->SetMarkerStyle(2); h2->SetMarkerSize(.5);
h3->SetMarkerColor(4 ); h3->SetMarkerStyle(2); h3->SetMarkerSize(.5);
h4->SetMarkerColor(5 ); h4->SetMarkerStyle(2); h4->SetMarkerSize(.5);
h5->SetMarkerColor(6 ); h5->SetMarkerStyle(2); h5->SetMarkerSize(.5);
h6->SetMarkerColor(7 ); h6->SetMarkerStyle(2); h6->SetMarkerSize(.5);
h7->SetMarkerColor(8 ); h7->SetMarkerStyle(2); h7->SetMarkerSize(.5);
h8->SetMarkerColor(9 ); h8->SetMarkerStyle(2); h8->SetMarkerSize(.5);
h9->SetMarkerColor(11); h9->SetMarkerStyle(2); h9->SetMarkerSize(.5);

h0->Draw();
h1->Draw("same");
h2->Draw("same");
h3->Draw("same");
h4->Draw("same");
h5->Draw("same");
h6->Draw("same");
h7->Draw("same");
h8->Draw("same");
h9->Draw("same");

