Miniball (FEBEX) / ISS (ASIC/CAEN) unpacker
===========================================

[TOC]

Usage
=====

```
bin/egmwsort <options> INPUT...
```

Command-line options
====================

Input options
-------------

- `INPUT`\
  Input filename(s), or - for stdin.
- `--expect=SPEC`\
  Subsystems expected, e.g. 'c01a012', or 'help'.
- `--synthetic-check`\
  Check synthetic data (anti-loss/dup).

Event-building options
----------------------

- `--trig-coinc=N`\
  Trigger coincidence window (ns).
- `--eb-window=N`\
  Event building window (ns).

Data printing options
---------------------

- `--print-raw`\
  Print raw data items.
- `--print-hit`\
  Print hit data items (combined raw).
- `--print-sort-hit`\
  Print hit data items (when time-sorted).
- `--print-record`\
  Print record info.

Monitoring options
------------------

- `--track-pulser`\
  Analyse pulser correlations.

Output options
--------------

- `--output=FILENAME`\
  Output filename, or - for stdout.
- `--format-out-hits`\
  Regenerate output hits from unpacked (test code).
- `--dump=FILENAME`\
  Debug dump filename.
- `--ext-data-raw`\
  Ext data (root/struct) raw instead of event-built.
- `--root=FILE.root`\
  Output ROOT file.
- `--struct=...`\
  Struct server (?).
- `--struct_hh=FILE.h`\
  Header file for struct server data.


Examples
========

Unpack a file to `.root`
------------------------

```
cat file.ebye | bin/egmwsort --root=out.root --ext-data
```

Monitor pulser syncronisation
-----------------------------

```
bin/egmwsort --expect=c01a012 --track-pulser
```


Overview
========

- `egmwsort.c`\
  Program `main()`, command-line parsing and main processing loop.
- `ebye_record.c`\
  `EBYEDATA` buffer reading and parsing.  Loop over payload data items.
- `item.c`\
  Payload item storage (general).
- `item_read.c`\
  Reading of payload items.
- `item_process.c`\



Call graph
==========

```
C main()
|
| [Parsing command line in main()]
|
| [Loop over input items (files/pipes).]
| |
| | [Open input]
| +-C open(item)
| |
| | [Loop over records.]
| +-C read_record()
|   |
|   | [Parse record header]
|   |
|   +-C read_record_items()
|   | |
|   | | [Loop over data items.]
|   | +-C item_read()
|   |   |
|   |   | [Identify item and read.]
|   |   +-C item_read_info()/
|   |       item_read_febex()/
|   |       item_read_caen()/
|   |       item_read_asic()/
|   |
|   | [Process items (sort/event-build).]
|   +-C item_process_items()
|
|
| [Finalisation.]
+-C records_done()
|
+-C flush_output()
|
+-C records_summary()
|
+-C item_summary()
```