/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* This file is part of egmwsort.
 *
 * Copyright (C) 2024  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#define LWROC_MESSAGE        1
#include "../src/message.h"

/* The purpose of this file is to sort EBYE hits, by using the egmwsort
 * hit handling and sorting.
 */

#include "lwroc_filter.h"
#include "lwroc_message.h"
#include "lwroc_mon_block.h"
#include "lwroc_filter_loop.h"
#include "ebye/lwroc_ebye_util.h"
#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

#include "lwroc_accum_sort.c"

/* Limit item.c operations. */
#define NO_EXT_DATA          1
#define NO_BUG2023_HANDLING  0
#define NO_LDF_DATA          1
#include "../src/config.c"
#include "../src/item.c"
#include "../src/item_ebye_read.c"
#include "../src/item_print.c"
#include "../src/item_process.c"
#include "../src/item_rteb.c"
#include "../src/item_hb.c"
#include "../src/hit_out.c"
#include "../src/ebye_record.c"
#include "../src/ebye_record_stats.c"
#include "../src/io_util.c"

/* Dummy function: */
void pulser_got(int id, uint64_t ts, uint16_t t_fine)
{
  (void) id;
  (void) ts;
  (void) t_fine;
}

void pulser_expect(int id)
{
  (void) id;
}

/* For debug-printing of offsets in item.c . */
char *_record_buffer = NULL;

#include <string.h>
#include <assert.h>

/*****************************************************************************/

/* The data flows through the accumulate-sort functionality per channel.
 *
 * This allows data from different channels to be slightly out-of order,
 * as well as data from different modules to be much out-of-order.
 *
 * According to which strategy do we expect data to be in-order, i.e.
 * when can we emit some old data?
 */

/*****************************************************************************/

void filter_ebyesort_setup(void);
void filter_ebyesort_loop(lwroc_pipe_buffer_consumer *pipe_buf,
			  const lwroc_thread_block *thread_block,
			  lwroc_data_pipe_handle *data_handle,
			  volatile int *terminate);
void filter_ebyesort_max_ev_len(uint32_t *max_len,
				uint32_t *add_len);

void lwroc_user_filter_pre_setup_functions(void)
{
  _lwroc_user_filter_functions->setup = filter_ebyesort_setup;
  _lwroc_user_filter_functions->loop = filter_ebyesort_loop;
  _lwroc_user_filter_functions->name = "filter_ebyesort";
  _lwroc_user_filter_functions->max_ev_len =
    filter_ebyesort_max_ev_len;
}

/*****************************************************************************/

#include "gen/lwroc_message_source_serializer.h"
#include "gen/lwroc_monitor_dams_source_serializer.h"
#include "gen/lwroc_monitor_dams_block_serializer.h"
#include "gen/lwroc_monitor_dam_source_serializer.h"
#include "gen/lwroc_monitor_dam_block_serializer.h"

void filter_ebyesort_monitor_fetch(void *ptr);

typedef struct lwroc_monitor_dams_dam_block_t
{
  lwroc_monitor_dams_block  _dams;
  lwroc_monitor_dam_block   _dam[1 /* actually more */];
} lwroc_monitor_dams_dam_block;

lwroc_monitor_dams_dam_block *_lwroc_mon_dams_dam;
lwroc_mon_block              *_lwroc_mon_dams_dam_handle = NULL;
uint32_t                      _lwroc_mon_dam_num = 0;

char *lwroc_mon_dams_dam_serialise(char *wire, void *block)
{
  lwroc_monitor_dams_dam_block *dams_dam_block =
    (lwroc_monitor_dams_dam_block *) block;
  uint32_t i;

  wire = lwroc_monitor_dams_block_serialize(wire, &dams_dam_block->_dams);

  for (i = 0; i < _lwroc_mon_dam_num; i++)
    wire =
      lwroc_monitor_dam_block_serialize(wire, &dams_dam_block->_dam[i]);

  return wire;
}

#if 0
void filter_ebyesort_accum_report_ins_backw(uint32_t id,
					    lwroc_accum_item_base *item,
					    uint64_t last_ts)
{
  LWROC_WARNING_FMT("Insert timestamp backwards for ID %d "
		    "(%016" PRIx64 " - %016" PRIx64 " = %" PRId64 ").",
		    id,
		    item->_ts, last_ts,
		    (int64_t) (item->_ts - last_ts));
}

void filter_ebyesort_accum_report_pick_backw(uint32_t id,
					     lwroc_accum_item_base *item,
					     uint64_t last_ts)
{
  LWROC_WARNING_FMT("Picked timestamp backwards for ID %d "
		    "(%016" PRIx64 " - %016" PRIx64 " = %" PRId64 ").",
		    id,
		    item->_ts, last_ts,
		    (int64_t) (item->_ts - last_ts));
}

void filter_ebyesort_accum_report_resorted(uint32_t id, size_t n)
{
  LWROC_WARNING_FMT("Resorted for ID %d, %" MYPRIzd " items.",
		    id, n);
}
#endif

void filter_ebyesort_setup(void)
{
  /* Monitor information. */

  /* TODO: for now, we have statically allocated one for each 16 possible.
   * Needs to change if we have many more...
   */
  _lwroc_mon_dam_num = 64;

  {
    lwroc_message_source dams_src_source;
    lwroc_message_source_sersize sz_dams_src_source;
    lwroc_monitor_dams_source dams_src;
    char *wire;
    size_t sz_mon_dams;
    uint32_t mon_idx = 0;

    dams_src._dam_blocks = _lwroc_mon_dam_num;

    lwroc_mon_source_set(&dams_src_source);

    /* The size ends where the dams writer blocks end. */
    sz_mon_dams = offsetof(lwroc_monitor_dams_dam_block,
			   _dam[_lwroc_mon_dam_num]);

    _lwroc_mon_dams_dam = malloc (sz_mon_dams);

    if (!_lwroc_mon_dams_dam)
      LWROC_FATAL("Failure allocating memory for DAMs monitor.");

    memset (_lwroc_mon_dams_dam, 0, sz_mon_dams);
    for ( ; mon_idx < _lwroc_mon_dam_num; )
      {
	lwroc_monitor_dam_block *mon =
	  &_lwroc_mon_dams_dam->_dam[mon_idx++];
	/*
	mon->_src_index_mask = 0;
	mon->_ts_ref_off_sig_k = (uint64_t) -2;
	*/
	mon->_id = 0;
      }

    _lwroc_mon_dams_dam_handle =
      lwroc_reg_mon_block(0, &_lwroc_mon_dams_dam->_dams, sz_mon_dams,
			  lwroc_monitor_dams_block_serialized_size() +
			  _lwroc_mon_dam_num *
			  lwroc_monitor_dam_block_serialized_size(),
			  lwroc_mon_dams_dam_serialise,
			  lwroc_message_source_serialized_size(&dams_src_source,
							       &sz_dams_src_source) +
			  lwroc_monitor_dams_source_serialized_size(),
			  filter_ebyesort_monitor_fetch,
			  NULL);

    wire = _lwroc_mon_dams_dam_handle->_ser_source;

    wire = lwroc_message_source_serialize(wire, &dams_src_source,
					  &sz_dams_src_source);
    wire = lwroc_monitor_dams_source_serialize(wire, &dams_src);

    assert (wire == (_lwroc_mon_dams_dam_handle->_ser_source +
		     _lwroc_mon_dams_dam_handle->_ser_source_size));
  }

  item_init_state();
}

/*****************************************************************************/

void filter_ebyesort_max_ev_len(uint32_t *max_len,
				uint32_t *add_len)
{
  /* Maximum generated event size: */

  *max_len = 12 * sizeof(uint32_t);
  (void) add_len;
}

/*****************************************************************************/

typedef struct mod_statistics_t
{
  uint64_t _ht_hits;
  uint64_t _hits;

  uint64_t _hits_missed;

} mod_statistics;

/*****************************************************************************/
#if 0
heimtime_state _ht_state[64];
#endif
mod_statistics _stats[64];

void filter_ebyesort_monitor_fetch(void *ptr)
{
  lwroc_monitor_dams_dam_block *dams_dam_mon =
    (lwroc_monitor_dams_dam_block *) ptr;
  uint32_t i;

  for (i = 0; i < _lwroc_mon_dam_num; i++)
    {
      lwroc_monitor_dam_block *mon =
	&(dams_dam_mon->_dam[i]);

      if (_stats[i]._hits)
	mon->_id = i+1;
      /*
      mon->_time._sec  = (uint64_t) now.tv_sec;
      mon->_time._nsec = (uint32_t) now.tv_usec * 1000;
      */
      mon->_hits        = _stats[i]._hits;
      mon->_hits_missed = _stats[i]._hits_missed;
    }
}

/*****************************************************************************/

typedef struct accum_item_dam_t
{
  lwroc_accum_item_base _base;

  uint16_t              _adc;
  uint16_t              _ov_pu;

} accum_item_dam;

/* Used by item.c when emitting data. */
void *ebye_request_output(size_t n, int *new_record, void *arg)
{
  lwroc_data_pipe_handle *data_handle = (lwroc_data_pipe_handle *) arg;
  char *dest;

  /* The output buffer is not divided into records yet, so if
   * anything, assume the worst, i.e. new record.
   */
  *new_record = 1;

  /* Get space in the destination buffer. */
  dest = lwroc_request_event_space(data_handle, n);

  return dest;
}

void ebye_used_output(size_t n, void *arg)
{
  lwroc_data_pipe_handle *data_handle = (lwroc_data_pipe_handle *) arg;

  /* Data has been written to the destination (space gotten by
   * request_output()).  Will not be modified any further.
   */
  lwroc_used_event_space(data_handle, n, 0 /* no flush */);
}

/*****************************************************************************/

void filter_ebyesort_loop(lwroc_pipe_buffer_consumer *pipe_buf,
			  const lwroc_thread_block *thread_block,
			  lwroc_data_pipe_handle *data_handle,
			  volatile int *terminate)
{
  /* int update_seq = 0; */

#if 0
  memset (&_ht_state, 0, sizeof (_ht_state));
#endif
  memset (&_stats, 0, sizeof (_stats));

  for ( ; ; )
    {
      uint32_t avail;
      char *ptr;

      /****************************************************************/
      /* Get next event from the source.                              */

      avail = lwroc_pipe_buffer_avail_read(pipe_buf, thread_block,
					   &ptr,
					   NULL, 0, 0);

      if (!avail) /* We would block. */
	{
	  struct timeval timeout;
	  if (*terminate)
	    break; /* No more data, and asked to quit. */
	  /* Try again in 0.1 s. */
	  timeout.tv_sec = 0;
	  timeout.tv_usec = 100000;
	  lwroc_thread_block_get_token_timeout(thread_block, &timeout);
	  goto update_monitor;
	}

      /* End of source fetching.                                      */
      /****************************************************************/

      _record_buffer = ptr;

      ebye_read_record_items(ptr, avail);

      lwroc_pipe_buffer_did_read(pipe_buf, avail);

      item_process_items();

    update_monitor:
      LWROC_FILTER_MON_CHECK(LWROC_FILTER_USER, 0 /* flush */);

      LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_dams_dam_handle,
				 &(_lwroc_mon_dams_dam->_dams), 0);
    }

  item_process_remaining();
}
