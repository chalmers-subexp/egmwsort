/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

void text_to_root(const char *file, const char *outfile, const char *kind)
{
  printf ("TTree::ReadFile(\"%s\", \"%s\") -> %s\n",
	  file, kind, outfile);

  TFile *f = new TFile(outfile,"RECREATE");
  TTree *t = new TTree("h102","h102");

  if (strcmp(kind, "cd01") == 0)
    t->ReadFile(file,
		"eventno/I:"
		"CD01udn/I:"
		"CD01udi/I:"
		"CD01ude1/F:"
		"CD01udt1/F:"
		"CD01vdn/I:"
		"CD01vdi/I:"
		"CD01vde1/F:"
		"CD01vdt1/F");
  else if (strcmp(kind, "cd02") == 0)
    t->ReadFile(file,
		"eventno/I:"
		"CD02udn/I:"
		"CD02udi/I:"
		"CD02ude1/F:"
		"CD02udt1/F:"
		"CD02vdn/I:"
		"CD02vdi/I:"
		"CD02vde1/F:"
		"CD02vdt1/F");
  else if (strcmp(kind, "elum") == 0)
    t->ReadFile(file,
		"eventno/I:"
		"ELUMvdn/I:"
		"ELUMvdi/I:"
		"ELUMvdt1/F:"
		"ELUMvde1/F");
  else if (strcmp(kind, "gcba") == 0)
    t->ReadFile(file,
		"eventno/I:"
		"GCBAdn/I:"
		"GCBAdi/I:"
		"GCBAdt1/F:"
		"GCBAde1/F");
  else if (strcmp(kind, "hsia") == 0)
    t->ReadFile(file,
		"eventno/I:"
		"HSIAdn/I:"
		"HSIAdi/I:"
		"HSIAdt1/F:"
		"HSIAdt2/F:"
		"HSIAdt3/F:"
		"HSIAde1/F:"
		"HSIAde2/F:"
		"HSIAde3/F");
  else if (strcmp(kind, "lume01") == 0)
    t->ReadFile(file,
		"eventno/I:"
		"LUME01dn/I:"
		"LUME01dt1/F:"
		"LUME01dt2/F:"
		"LUME01dt3/F:"
		"LUME01de1/F:"
		"LUME01de2/F:"
		"LUME01de3/F");
  else if (strcmp(kind, "lume02") == 0)
    t->ReadFile(file,
		"eventno/I:"
		"LUME02dn/I:"
		"LUME02dt1/F:"
		"LUME02dt2/F:"
		"LUME02dt3/F:"
		"LUME02de1/F:"
		"LUME02de2/F:"
		"LUME02de3/F");
  else if (strcmp(kind, "isia") == 0)
    t->ReadFile(file,
		"eventno/I:"
		"ISIAudn/I:"
		"ISIAudi/I:"
		"ISIAudk/I:"
		"ISIAude1/F:"
		"ISIAudt1/F:"
		"ISIAvdn/I:"
		"ISIAvdi/I:"
		"ISIAvdk/I:"
		"ISIAvde1/F:"
		"ISIAvdt1/F");
  else if (strcmp(kind, "rec") == 0)
    t->ReadFile(file,
		"eventno/I:"
		"REC01vdn/I:"
		"REC01vdi/I:"
		"REC01vde1/F:"
		"REC01vdt1/F:"
		"REC02vdn/I:"
		"REC02vdi/I:"
		"REC02vde1/F:"
		"REC02vdt1/F");
  else
    {
      fprintf (stderr, "Unknown kind: %s\n", kind);
      exit(1);
    }

  t->Write();
  f->Close();
}
