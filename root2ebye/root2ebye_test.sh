#!/bin/sh

set -e

mkdir -p test/out/

for FILE in cd01 cd02 elum gcba hsia isia lume01 lume02 rec \
		 hsia_seed1 hsia_seed2
do
    echo "===================="
    echo "== $FILE"
    echo

    FILETXT=`echo $FILE | sed -e "s/_seed.*//"`

    echo $FILETXT

    root -l -b -q "text_to_root.C(\"test/$FILETXT.txt\",\"test/out/$FILE.root\",\"$FILETXT\")"

    echo "root2ebye ..."

    GO_SEED=
    case $FILE in
	*_seed*)
	    GO_SEED=--go-seed=`echo $FILE | sed -e "s/.*_seed//"`
	    ;;
    esac

    echo $GO_SEED

    bin/root2ebye test/out/$FILE.root test/out/$FILE.ebye $GO_SEED

    echo "egmwsort ..."

    ../bin/egmwsort test/out/$FILE.ebye --print-hit > test/out/$FILE.out

    echo "diff ..."

    diff -u test/$FILE.good test/out/$FILE.out

done

echo "===================="
