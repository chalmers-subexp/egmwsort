/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __GAIN_OFFSET_HH__
#define __GAIN_OFFSET_HH__

#include <math.h>

class gain_offset
{
public:
  double _gain;        /* [MeV/ch] */
  double _raw_offset;  /* [ch]     */

  int    _max_raw;     /* [ch]     */

  /* calibrated_value = (raw_value - raw_offset) * _gain
   * raw_value = calibrated_value / _gain + raw_offset
   *
   * raw_offset is the raw value at zero energy
   *
   * E.g. for an amplitude: _gain [MeV/ch]  _offset [ch]
   * E.g. for a time:       _gain [ns/ch]   _offset [ch]
   */

public:
  double raw2cal(int raw)
  {
    double cal;

    cal = (raw - _raw_offset) * _gain;

    return cal;
  }

  double cal2raw(double cal)
  {
    double raw;

    raw = cal / _gain + _raw_offset;

    return raw;
  }

  bool cal2raw(int *int_raw, double cal)
  {
    double raw;

    raw = cal2raw(cal);

    if (!isfinite(raw))
      {
	/* NAN or INF. */

	if (isinf(raw) == 1)
	  {
	    /* Positive INF, give maximum discretized value. */
	    *int_raw = _max_raw;
	    return true;
	  }
	return false;
      }

    if (raw >= _max_raw)
      {
	/* Over range, give maximum discretized value. */
	*int_raw = _max_raw;
	return true;
      }
    if (raw <= 0)
      {
	/* Under range (negative), no value at all. */
	return false;
      }

    /* Inside range, convert. */
    *int_raw = (int) raw;
    return true;
  }
};

#endif//__GAIN_OFFSET_HH__
