/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 * Copyright (C) 2025  Björn Johansson  <bjoj@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __root2ebye_hh__
#define __root2ebye_hh__

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "gain_offset.hh"

// Header file for the classes stored in the TTree if any.

class root2ebye
{
public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Which detectors are available in tree?
  Int_t           hsia = 0;
  Int_t           lume[4] = { 0, 0, 0, 0 };
  Int_t           cd[2] = { 0, 0 };
  Int_t           isia = 0;
  Int_t           elum = 0;
  Int_t           gcba = 0;
  Int_t           rec[2] = { 0, 0 };

  Int_t           ids_u[4] = { 0, 0, 0, 0 };

  // Calibration parameters (gain-offset).
  gain_offset     go_CDu_e[2][24];
  gain_offset     go_CDv_e[2][32];
  gain_offset     go_ELUM_e[4];
  gain_offset     go_GCBA_e[36];
  gain_offset     go_HSIA_e[24][3];
  gain_offset     go_ISIA_p_e[12][128];
  gain_offset     go_ISIA_n_e[24][11];
  gain_offset     go_LUME_e[4][3];
  gain_offset     go_REC_e[2][4];

  gain_offset     go_IDS_Uu_e[4][16];
  gain_offset     go_IDS_Uv_e[4][16];

  // Fixed size dimensions of array or collections stored in the TTree if any.

  // Declaration of leaf types
  UInt_t          eventno;

  UInt_t          HSIAdn = 0;
  UInt_t          HSIAdi[100];
  Float_t         HSIAdt[3][100];
  Float_t         HSIAde[3][100];

  UInt_t          LUME_dn[4] = { 0, 0, 0, 0 };
  Float_t         LUME_dt[4][3][100];
  Float_t         LUME_de[4][3][100];

  UInt_t          CD_udn [2] = { 0, 0 };
  UInt_t          CD_udi [2][100];
  Float_t         CD_ude1[2][100];
  Float_t         CD_udt1[2][100];
  UInt_t          CD_vdn [2] = { 0, 0 };
  UInt_t          CD_vdi [2][100];
  Float_t         CD_vde1[2][100];
  Float_t         CD_vdt1[2][100];

  UInt_t          ISIAudn = 0;
  UInt_t          ISIAudi[100];
  UInt_t          ISIAudk[100];
  Float_t         ISIAude1[100];
  Float_t         ISIAudt1[100];
  UInt_t          ISIAvdn = 0;
  UInt_t          ISIAvdi[100];
  UInt_t          ISIAvdk[100];
  Float_t         ISIAvde1[100];
  Float_t         ISIAvdt1[100];

  UInt_t          ELUMvdn = 0;
  UInt_t          ELUMvdi [100];
  Float_t         ELUMvde1[100];
  Float_t         ELUMvdt1[100];

  UInt_t          GCBAdn = 0;
  UInt_t          GCBAdi [100];
  Float_t         GCBAde1[100];
  Float_t         GCBAdt1[100];

  UInt_t          REC_vdn [2] = { 0, 0 };
  UInt_t          REC_vdi [2][100];
  Float_t         REC_vde1[2][100];
  Float_t         REC_vdt1[2][100];

  UInt_t          IDS_U_udn [4] = { 0, 0, 0, 0 };
  UInt_t          IDS_U_udi [4][100];
  Float_t         IDS_U_ude1[4][100];
  Float_t         IDS_U_udt1[4][100];
  UInt_t          IDS_U_vdn [4] = { 0, 0, 0, 0 };
  UInt_t          IDS_U_vdi [4][100];
  Float_t         IDS_U_vde1[4][100];
  Float_t         IDS_U_vdt1[4][100];

  // List of branches
  TBranch        *b_eventno;

  TBranch        *b_HSIAdn;
  TBranch        *b_HSIAdi;
  TBranch        *b_HSIAdt[3];
  TBranch        *b_HSIAde[3];

  TBranch        *b_LUME_dn[4];
  TBranch        *b_LUME_dt[4][3];
  TBranch        *b_LUME_de[4][3];

  TBranch        *b_CD_udn [2];
  TBranch        *b_CD_udi [2];
  TBranch        *b_CD_ude1[2];
  TBranch        *b_CD_udt1[2];
  TBranch        *b_CD_vdn [2];
  TBranch        *b_CD_vdi [2];
  TBranch        *b_CD_vde1[2];
  TBranch        *b_CD_vdt1[2];

  TBranch        *b_ISIAudn;
  TBranch        *b_ISIAudi;
  TBranch        *b_ISIAudk;
  TBranch        *b_ISIAude1;
  TBranch        *b_ISIAudt1;
  TBranch        *b_ISIAvdn;
  TBranch        *b_ISIAvdi;
  TBranch        *b_ISIAvdk;
  TBranch        *b_ISIAvde1;
  TBranch        *b_ISIAvdt1;

  TBranch        *b_ELUMvdn;
  TBranch        *b_ELUMvdi;
  TBranch        *b_ELUMvde1;
  TBranch        *b_ELUMvdt1;

  TBranch        *b_GCBAdn;
  TBranch        *b_GCBAdi;
  TBranch        *b_GCBAde1;
  TBranch        *b_GCBAdt1;

  TBranch        *b_REC_vdn [2];
  TBranch        *b_REC_vdi [2];
  TBranch        *b_REC_vde1[2];
  TBranch        *b_REC_vdt1[2];

  TBranch        *b_IDS_U_udn [4];
  TBranch        *b_IDS_U_udi [4];
  TBranch        *b_IDS_U_ude1[4];
  TBranch        *b_IDS_U_udt1[4];
  TBranch        *b_IDS_U_vdn [4];
  TBranch        *b_IDS_U_vdi [4];
  TBranch        *b_IDS_U_vde1[4];
  TBranch        *b_IDS_U_vdt1[4];

  root2ebye(TTree *tree);
  virtual ~root2ebye();
  virtual Int_t    GetEntry(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop();
  virtual void     InitBranches();
  virtual void     LookForBranches(TTree *tree);
};

#endif//__root2ebye_hh__
