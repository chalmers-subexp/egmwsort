/* This file is part of EGMWSORT - a tool for data sorting.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 * Copyright (C) 2025  Björn Johansson  <bjoj@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <inttypes.h>

#include "root2ebye.hh"

extern "C" {
#include "../src/message.c"
#include "../src/inc_colourtext.c"
#include "../src/ebye_record_out.c"
#include "../src/ldf_record_out.c"
#include "../src/hit_out.c"
#include "../src/io_util.c"
}

#define countof(array) (sizeof(array)/sizeof((array)[0]))

int str_ends_with(const char *str, const char *suffix)
{
  if (!str || !suffix)
    return 0;

  size_t str_length = strlen(str);
  size_t suffix_length = strlen(suffix);

  if (suffix_length > str_length)
    return 0;

  return strcmp(str + str_length - suffix_length, suffix) == 0;
}

root2ebye::root2ebye(TTree *tree) : fChain(0)
{
  Init(tree);
}

root2ebye::~root2ebye()
{
  if (!fChain)
    return;
  delete fChain->GetCurrentFile();
}

Int_t root2ebye::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain)
    return 0;
  return fChain->GetEntry(entry);
}

#define CHSBA  fChain->SetBranchAddress
#define TSFMT  TString::Format

void root2ebye::Init(TTree *tree)
{
  // Set branch addresses and branch pointers
  if (!tree)
    return;

  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  CHSBA("eventno",  &eventno, &b_eventno);
}

void root2ebye::InitBranches()
{
  if(hsia)
    {
      CHSBA("HSIAdn", &HSIAdn,  &b_HSIAdn);
      CHSBA("HSIAdi",  HSIAdi,  &b_HSIAdi);
      for (int i = 0; i < 3; i++)
	{
	  CHSBA(TSFMT("HSIAdt%d",i+1), HSIAdt[i], &b_HSIAdt[i]);
	  CHSBA(TSFMT("HSIAde%d",i+1), HSIAde[i], &b_HSIAde[i]);
	}
    }
  for (int k = 0; k < 4; k++)
    if (lume[k])
      {
	CHSBA(TSFMT("LUME%02ddn",k+1), &LUME_dn[k],  &b_LUME_dn[k]);
	for (int i = 0; i < 3; i++)
	  {
	    CHSBA(TSFMT("LUME%02ddt%d",k+1,i+1), LUME_de[k][i], &b_LUME_de[k][i]);
	    CHSBA(TSFMT("LUME%02dde%d",k+1,i+1), LUME_de[k][i], &b_LUME_de[k][i]);
	  }
      }
  for (int k = 0; k < 2; k++)
    if(cd[k])
      {
	CHSBA(TSFMT("CD%02dudn",k+1), &CD_udn[k],  &b_CD_udn[k]);
	CHSBA(TSFMT("CD%02dudi",k+1),  CD_udi[k],  &b_CD_udi[k]);
	CHSBA(TSFMT("CD%02dude1",k+1), CD_ude1[k], &b_CD_ude1[k]);
	CHSBA(TSFMT("CD%02dudt1",k+1), CD_udt1[k], &b_CD_udt1[k]);
	CHSBA(TSFMT("CD%02dvdn",k+1), &CD_vdn[k],  &b_CD_vdn[k]);
	CHSBA(TSFMT("CD%02dvdi",k+1),  CD_vdi[k],  &b_CD_vdi[k]);
	CHSBA(TSFMT("CD%02dvde1",k+1), CD_vde1[k], &b_CD_vde1[k]);
	CHSBA(TSFMT("CD%02dvdt1",k+1), CD_vdt1[k], &b_CD_vdt1[k]);
      }
  if (isia)
    {
      CHSBA("ISIAudn", &ISIAudn,  &b_ISIAudn);
      CHSBA("ISIAudi",  ISIAudi,  &b_ISIAudi);
      CHSBA("ISIAudk",  ISIAudk,  &b_ISIAudk);
      CHSBA("ISIAude1", ISIAude1, &b_ISIAude1);
      CHSBA("ISIAudt1", ISIAudt1, &b_ISIAudt1);
      CHSBA("ISIAvdn", &ISIAvdn,  &b_ISIAvdn);
      CHSBA("ISIAvdi",  ISIAvdi,  &b_ISIAvdi);
      CHSBA("ISIAvdk",  ISIAvdk,  &b_ISIAvdk);
      CHSBA("ISIAvde1", ISIAvde1, &b_ISIAvde1);
      CHSBA("ISIAvdt1", ISIAvdt1, &b_ISIAvdt1);
    }
  if (elum)
    {
      CHSBA("ELUMvdn", &ELUMvdn,  &b_ELUMvdn);
      CHSBA("ELUMvdi",  ELUMvdi,  &b_ELUMvdi);
      CHSBA("ELUMvde1", ELUMvde1, &b_ELUMvde1);
      CHSBA("ELUMvdt1", ELUMvdt1, &b_ELUMvdt1);
    }
  if (gcba)
    {
      CHSBA("GCBAdn", &GCBAdn,  &b_GCBAdn);
      CHSBA("GCBAdi",  GCBAdi,  &b_GCBAdi);
      CHSBA("GCBAde1", GCBAde1, &b_GCBAde1);
      CHSBA("GCBAdt1", GCBAdt1, &b_GCBAdt1);
    }
  for (int k = 0; k < 2; k++)
    if (rec[k])
      {
	CHSBA(TSFMT("REC%02dvdn",k+1), &REC_vdn[k],  &b_REC_vdn[k]);
	CHSBA(TSFMT("REC%02dvdi",k+1),  REC_vdi[k],  &b_REC_vdi[k]);
	CHSBA(TSFMT("REC%02dvde1",k+1), REC_vde1[k], &b_REC_vde1[k]);
	CHSBA(TSFMT("REC%02dvdt1",k+1), REC_vdt1[k], &b_REC_vdt1[k]);
      }
  for (int k = 0; k < 4; k++)
    if(ids_u[k])
      {
	CHSBA(TSFMT("U%dudn",k+1), &IDS_U_udn[k],  &b_IDS_U_udn[k]);
	CHSBA(TSFMT("U%dudi",k+1),  IDS_U_udi[k],  &b_IDS_U_udi[k]);
	CHSBA(TSFMT("U%dude1",k+1), IDS_U_ude1[k], &b_IDS_U_ude1[k]);
	CHSBA(TSFMT("U%dudt1",k+1), IDS_U_udt1[k], &b_IDS_U_udt1[k]);
	CHSBA(TSFMT("U%dvdn",k+1), &IDS_U_vdn[k],  &b_IDS_U_vdn[k]);
	CHSBA(TSFMT("U%dvdi",k+1),  IDS_U_vdi[k],  &b_IDS_U_vdi[k]);
	CHSBA(TSFMT("U%dvde1",k+1), IDS_U_vde1[k], &b_IDS_U_vde1[k]);
	CHSBA(TSFMT("U%dvdt1",k+1), IDS_U_vdt1[k], &b_IDS_U_vdt1[k]);
      }
}

void root2ebye::LookForBranches(TTree *tree)
{
  TObjArray *branch_list = tree->GetListOfBranches();
  int num_branches = branch_list->GetEntries();

  for (int branch_num = 0; branch_num < num_branches; branch_num++)
    {
      const char *branch_name = branch_list[0][branch_num]->GetName();
      const char *digi_suffix = "dn";

      if (str_ends_with(branch_name,digi_suffix))
	{
	  if (strncmp(branch_name,"HSIA",4) == 0)
	    hsia = 1;
	  else if (strncmp(branch_name,"LUME01",6) == 0)
	    lume[0] = 1;
	  else if (strncmp(branch_name,"LUME02",6) == 0)
	    lume[1] = 1;
	  else if (strncmp(branch_name,"LUME03",6) == 0)
	    lume[2] = 1;
	  else if (strncmp(branch_name,"LUME04",6) == 0)
	    lume[3] = 1;
	  else if (strncmp(branch_name,"CD01",4) == 0)
	    cd[0] = 1;
	  else if (strncmp(branch_name,"CD02",4) == 0)
	    cd[1] = 1;
	  else if (strncmp(branch_name,"ISIA",4) == 0)
	    isia = 1;
	  else if (strncmp(branch_name,"ELUM",4) == 0)
	    elum = 1;
	  else if (strncmp(branch_name,"GCBA",4) == 0)
	    gcba = 1;
	  else if (strncmp(branch_name,"REC01",5) == 0)
	    rec[0] = 1;
	  else if (strncmp(branch_name,"REC02",5) == 0)
	    rec[1] = 1;
	  else if (strncmp(branch_name,"U1",2) == 0)
	    ids_u[0] = 1;
	  else if (strncmp(branch_name,"U2",2) == 0)
	    ids_u[1] = 1;
	  else if (strncmp(branch_name,"U3",2) == 0)
	    ids_u[2] = 1;
	  else if (strncmp(branch_name,"U4",2) == 0)
	    ids_u[3] = 1;
	}
    }

  printf ("Branches: "
	  "hsia: %d, "
	  "lume: %d,%d,%d,%d, "
	  "cd: %d,%d "
	  "isia: %d "
	  "elum: %d "
	  "gcba: %d "
	  "rec: %d,%d,\n"
	  "         "
	  "u: %d,%d,%d,%d\n",
	  hsia,
	  lume[0], lume[1], lume[2], lume[3],
	  cd[0], cd[1],
	  isia,
	  elum,
	  gcba,
	  rec[0], rec[1],
	  ids_u[0], ids_u[1], ids_u[2], ids_u[3]);
}

#define GET_GAIN_OFFSET_1D(go, array, i1)				\
  if ((i1) < 0 || (size_t) (i1) > countof(array))  {			\
    fprintf (stderr, "Index 1: %d outside range [0,%zd] for '%s'.\n",	\
	     i1, countof(array), #array);				\
    exit(1);								\
  }									\
  gain_offset &go = array[i1];

#define GET_GAIN_OFFSET_2D(go, array, i1, i2)				\
  if ((i1) < 0 || (size_t) (i1) > countof(array))  {			\
    fprintf (stderr, "Index 1: %d outside range [0,%zd] for '%s'.\n",	\
	     i1, countof(array), #array);				\
    exit(1);								\
  }									\
  if ((i2) < 0 || (size_t) (i2) > countof(array[0]))  {			\
    fprintf (stderr, "Index 2: %d outside range [0,%zd] for '%s'.\n",	\
	     i2, countof(array), #array);				\
    exit(1);								\
  }									\
  gain_offset &go = array[i1][i2];

#define MESYTEC_MODULE_OFFSET 16

/* ISSSort default module mappings:
 *
 * Note: crate adds 16 to module number in data format.
 *
 *          crate  mod    ch
 * RECOIL   0      0      0-7
 * ELUM     0      1      0-3
 * ZD       0      1      6-7
 * MWPC     0      1      8-11
 * CD       ?      ?      ?
 * LUME     ?      ?      ?
 * SCINT    1      0-2    0-31     ( = gamma)
 * HSIA                            ( = non-ISS)
 * ISIA                            ( = ASIC)
 *
 * IDS (pixie-16, 16 ch/mod):
 *
 * U        0      5-12   0-15     ( = dsssd)
 * INDIE    0      1-3    scattered
 * B        0      0      0-7,8-15 ( = sci. pads)
 * C        0      4      0-15     ( = clovers)
 */

void root2ebye::Loop()
{
  if (fChain == 0) return;

  int64_t nentries = fChain->GetEntriesFast();

  uint64_t total_hits_hsia = 0;
  uint64_t total_hits_lume[4] = { 0, 0, 0, 0 };
  uint64_t total_hits_cd_u[2] = { 0, 0 };
  uint64_t total_hits_cd_v[2] = { 0, 0 };
  uint64_t total_hits_isia_p = 0;
  uint64_t total_hits_isia_n = 0;
  uint64_t total_hits_elum = 0;
  uint64_t total_hits_gcba = 0;
  uint64_t total_hits_rec[2] = { 0, 0 };
  uint64_t total_hits_ids_u_u[4] = { 0, 0, 0, 0 };
  uint64_t total_hits_ids_u_v[4] = { 0, 0, 0, 0 };

  for (Long64_t j = 0; j < nentries; j++)
    {
      fChain->GetEntry(j);

      /* printf ("\n"); */

      uint64_t ref_t = ((uint64_t) eventno) * 0x10000 + 0x1000;

      for (unsigned int h = 0; h < HSIAdn; h++)
	{
	  for (int ch = 0; ch < 3; ch++)
	    {
	      int detector = HSIAdi[h];
	      float e      = HSIAde[ch][h];
	      float t      = HSIAdt[ch][h];

	      uint64_t timestamp = ref_t + t;

	      int module = 10 + ch;
	      int adc;

	      GET_GAIN_OFFSET_2D(go, go_HSIA_e, detector, ch);

	      if (!go.cal2raw(&adc, e))
		continue;

	      // printf("%d %d : %2d  %7.3f  %7.3f\n",
	      //	h, ch, HSIAdi[h], HSIAde[ch][h], go._gain);

	      format_hit_caen(module, detector,
			      adc, 0, 0, timestamp);
	    }
	  total_hits_hsia++;
	}

      for (int k = 0; k < 4; k++)
	for (unsigned int h = 0; h < LUME_dn[k]; h++)
	  {
	    for (int ch = 0; ch < 3; ch++)
	      {
		float e      = LUME_de[k][ch][h];
		float t      = LUME_dt[k][ch][h];

		uint64_t timestamp = ref_t + t;

		int module = MESYTEC_MODULE_OFFSET + 4; /* 20 */
		int mod_ch = ch + k*3;
		int adc;

		GET_GAIN_OFFSET_2D(go, go_LUME_e, k, ch);

		if (!go.cal2raw(&adc, e))
		  continue;

		format_hit_caen(module, mod_ch,
				adc, 0, 0, timestamp);
	      }

	    total_hits_lume[k]++;
	  }

      for (int k = 0; k < 2; k++)
	{
	  for (unsigned int h = 0; h < CD_udn[k]; h++)
	    {
	      int ch  = CD_udi[k][h] - 1;
	      float e = CD_ude1[k][h];
	      float t = CD_udt1[k][h];

	      uint64_t timestamp = ref_t + t;

	      int module = MESYTEC_MODULE_OFFSET + 2 * k + 0; /* 16, 18 */
	      int mod_ch = ch;
	      int adc;

	      GET_GAIN_OFFSET_2D(go, go_CDu_e, k, ch);

	      if (!go.cal2raw(&adc, e))
		continue;

	      format_hit_caen(module, mod_ch,
			      adc, 0, 0, timestamp);

	      total_hits_cd_u[k]++;
	    }

	  for (unsigned int h = 0; h < CD_vdn[k]; h++)
	    {
	      int ch  = CD_vdi[k][h] - 1;
	      float e = CD_vde1[k][h];
	      float t = CD_vdt1[k][h];

	      uint64_t timestamp = ref_t + t;

	      int module = MESYTEC_MODULE_OFFSET + 2 * k + 1; /* 17, 19 */
	      int mod_ch = ch;
	      int adc;

	      GET_GAIN_OFFSET_2D(go, go_CDv_e, k, ch);

	      if (!go.cal2raw(&adc, e))
		continue;

	      format_hit_caen(module, mod_ch,
			      adc, 0, 0, timestamp);

	      total_hits_cd_v[k]++;
	    }
	}

      // ISIA p-sides.
      for (unsigned int h = 0; h < ISIAudn; h++)
	{
	  int det = ISIAudk[h] - 1;
	  int ch  = ISIAudi[h] - 1;
	  float e = ISIAude1[h];
	  float t = ISIAudt1[h];

	  uint64_t timestamp = ref_t + t;

	  int num_sides = 6;
	  int side = det % num_sides;
	  int row  = det / num_sides;

	  int asic_from_row[4] = { 0, 2, 3, 5 };

	  int module = (side / 2) % 3; /* 0,1 -> 0 , 2,3 -> 1 , 4,5 -> 2 */
	  int asic   = asic_from_row[row % 4];
	  int mod_ch = ch;
	  int adc;

	  //printf("module %d, asic %d, channel %d\n",
	  //	 module, asic, mod_ch);

	  GET_GAIN_OFFSET_2D(go, go_ISIA_p_e, det/2, ch);

	  if (!go.cal2raw(&adc, e))
	    continue;

	  format_hit_asic(module, asic, mod_ch,
			  adc, timestamp);
	  total_hits_isia_p++;
	}

      // ISIA n-sides.
      for (unsigned int h = 0; h < ISIAvdn; h++)
	{
	  int det = ISIAvdk[h] - 1;
	  int ch  = ISIAvdi[h] - 1;
	  float e = ISIAvde1[h];
	  float t = ISIAvdt1[h];

	  uint64_t timestamp = ref_t + t;

	  int num_sides = 6;
	  int side = det % num_sides;
	  int row  = det / num_sides;

	  int module = (side / 2) % 3; /* 0,1 -> 0 , 2,3 -> 1 , 4,5 -> 2 */

	  int asic   = (row <= 1 ? 1 : 4); /* 0,1 -> 1 , 2,3 -> 4 */
	  int offset =
	    (side % 2 == 1 ? (row % 2 ? 88 : 27) : (row % 2 ? 105 : 10));
	  int mod_ch = ch;
	  int adc;

	  //printf("module %d, asic %d, channel %d\n",
	  //	 module, asic, mod_ch);

	  GET_GAIN_OFFSET_2D(go, go_ISIA_n_e, det, ch);

	  if (!go.cal2raw(&adc, e))
	    continue;

	  format_hit_asic(module, asic, mod_ch + offset,
			  adc, timestamp);

	  total_hits_isia_n++;
	}

      for (unsigned int h = 0; h < ELUMvdn; h++)
	{
	  int ch  = ELUMvdi[h]-1;
	  float e = ELUMvde1[h];
	  float t = ELUMvdt1[h];

	  uint64_t timestamp = ref_t + t;

	  int module = 1;
	  int mod_ch = ch;
	  int adc;

	  GET_GAIN_OFFSET_1D(go, go_ELUM_e, ch);

	  if (!go.cal2raw(&adc, e))
	    continue;

	  format_hit_caen(module, mod_ch,
			  adc, 0, 0, timestamp);

	  total_hits_elum++;
	}

      for (unsigned int h = 0; h < GCBAdn; h++)
	{
	  int ch  = GCBAdi[h]-1;
	  float e = GCBAde1[h];
	  float t = GCBAdt1[h];

	  uint64_t timestamp = ref_t + t;

	  int ch_per_module = 32;
	  int module  = MESYTEC_MODULE_OFFSET + ch / ch_per_module;
	  int mod_ch =                         ch % ch_per_module;
	  int adc;

	  GET_GAIN_OFFSET_1D(go, go_GCBA_e, ch);

	  if (!go.cal2raw(&adc, e))
	    continue;

	  format_hit_caen(module, mod_ch,
			  adc, 0, 0, timestamp);

	  total_hits_gcba++;
	}

      for (int k = 0; k < 2; k++)
	for (unsigned int h = 0; h < REC_vdn[k]; h++)
	  {
	    int ch  = REC_vdi[k][h] - 1;
	    float e = REC_vde1[k][h];
	    float t = REC_vdt1[k][h];

	    uint64_t timestamp = ref_t + t;

	    int module = 0;
	    int mod_ch = 2 * ch + k; // k=0=>even, k=1=>odd
	    int adc;

	    GET_GAIN_OFFSET_2D(go, go_REC_e, k, ch);

	    if (!go.cal2raw(&adc, e))
	      continue;

	    format_hit_caen(module, mod_ch,
			    adc, 0, 0, timestamp);

	    total_hits_rec[k]++;
	  }

      for (int k = 0; k < 4; k++)
	{
	  for (unsigned int h = 0; h < IDS_U_udn[k]; h++)
	    {
	      int ch  = IDS_U_udi[k][h] - 1;
	      float e = IDS_U_ude1[k][h];
	      float t = IDS_U_udt1[k][h];

	      uint64_t timestamp = ref_t + t;

	      int crate = 0;
	      int slot = 5 + k * 2;
	      int slot_ch = ch;
	      int adc;

	      GET_GAIN_OFFSET_2D(go, go_IDS_Uu_e, k, ch);

	      if (!go.cal2raw(&adc, e))
		continue;

	      format_hit_pixie(crate, slot, slot_ch,
			       adc, 0, timestamp);

	      total_hits_ids_u_u[k]++;
	    }

	  for (unsigned int h = 0; h < IDS_U_vdn[k]; h++)
	    {
	      int ch  = IDS_U_vdi[k][h] - 1;
	      float e = IDS_U_vde1[k][h];
	      float t = IDS_U_vdt1[k][h];

	      uint64_t timestamp = ref_t + t;

	      int crate = 0;
	      int slot = 6 + k * 2;
	      int slot_ch = ch;
	      int adc;

	      GET_GAIN_OFFSET_2D(go, go_IDS_Uv_e, k, ch);

	      if (!go.cal2raw(&adc, e))
		continue;

	      format_hit_pixie(crate, slot, slot_ch,
			       adc, 0, timestamp);

	      total_hits_ids_u_v[k]++;
	    }
	}
    }

  printf ("Events: %" PRId64 "\n",
	  nentries);
  printf ("Hits: "
	  "hsia: %" PRIu64 ", "
	  "lume: %" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ", "
	  "cd: %" PRIu64 ",%" PRIu64 ", %" PRIu64 ",%" PRIu64 ", "
	  "isia: %" PRIu64 ",%" PRIu64 ", "
	  "elum: %" PRIu64 ", "
	  "gcba: %" PRIu64 ",\n"
	  "      "
	  "rec: %" PRIu64 ",%" PRIu64 "\n"
	  "      "
	  "u: %" PRIu64 ",%" PRIu64 ", %" PRIu64 ",%" PRIu64 ", "
	  "" "%" PRIu64 ",%" PRIu64 ", %" PRIu64 ",%" PRIu64 "\n",
	  total_hits_hsia,
	  total_hits_lume[0], total_hits_lume[1],
	  total_hits_lume[2], total_hits_lume[3],
	  total_hits_cd_u[0], total_hits_cd_v[0],
	  total_hits_cd_u[1], total_hits_cd_v[1],
	  total_hits_isia_p, total_hits_isia_n,
	  total_hits_elum,
	  total_hits_gcba,
	  total_hits_rec[0], total_hits_rec[1],
	  total_hits_ids_u_u[0], total_hits_ids_u_v[0],
	  total_hits_ids_u_u[1], total_hits_ids_u_v[1],
	  total_hits_ids_u_u[2], total_hits_ids_u_v[2],
	  total_hits_ids_u_u[3], total_hits_ids_u_v[3]);
}

uint64_t rxs64s(uint64_t *x) // xorshift64star, see wikipedia
{
  *x ^= *x >> 12;
  *x ^= *x << 25;
  *x ^= *x >> 27;
  return *x * 2685821657736338717ull;
}

void init_gain_offset(gain_offset &go, double inv_gain, double offset,
		      int max_raw,
		      int seed0, const char *seed1, size_t seed2)
{
  uint64_t rstate = (uint64_t) seed0;
  const char *p;

  rxs64s(&rstate);
  rstate += seed2;

  for (p = seed1; *p; p++)
    {
      rxs64s(&rstate);
      rstate += *p;
    }

  double mul_gain =
    1. + 0.1 * (rxs64s(&rstate) & 0xffffff) / (double) 0xffffff;
  double add_offset = (rxs64s(&rstate) & 0xff);

  if (!seed0)
    {
      mul_gain = 1;
      add_offset = 0;
    }

  go._gain = 1. / (inv_gain * mul_gain);
  go._raw_offset = offset + add_offset;
  go._max_raw = max_raw;
}

#define GET_VARIABLE_NAME(name) #name

#define INIT_GAIN_OFFSET_1D(array, inv_gain, offset, range, flag) do {	\
    for (size_t i1 = 0; i1 < countof(array); i1++)			\
      {									\
	init_gain_offset(array[i1],					\
			 inv_gain, offset, range,			\
			 go_seed, #array, i1);				\
	if (flag)							\
	  printf("%s.%ld Gain: %f Offset: %f\n",			\
		 GET_VARIABLE_NAME(array), i1,				\
		 array[i1]._gain, array[i1]._raw_offset);		\
      }									\
  } while (0)

#define INIT_GAIN_OFFSET_2D(array, inv_gain, offset, range, flag) do {	\
    for (size_t i1 = 0; i1 < countof(array); i1++)			\
      for (size_t i2 = 0; i2 < countof(array[0]); i2++)			\
	{								\
	  init_gain_offset(array[i1][i2],				\
			   inv_gain, offset, range,			\
			   go_seed, #array,				\
			   i1 * countof(array[0]) + i2);		\
	  if (flag)							\
	    printf("%s.%ld.%ld Gain: %f Offset: %f\n",			\
		   GET_VARIABLE_NAME(array), i1, i2,			\
		   array[i1][i2]._gain,					\
		   array[i1][i2]._raw_offset);				\
	}								\
  } while (0)

void main_usage(char *cmdname)
{
  printf ("Root to EBYE data writer.\n");

  printf ("\n");
  printf ("Usage: %s INPUT OUTPUT <options> ...\n", cmdname);
  printf ("\n");
  printf ("  INPUT                    Input filename (typically input.root).\n");
  printf ("  OUTPUT                   Output filename (typically output.ebye).\n");
  printf ("\n");
  printf ("  --ldf                    Generate LDF format data (instead of EBYE).\n");
  printf ("  --in-tree=NAME           Name of input tree (default: h102).\n");
  printf ("  --go-seed=N              Seed to randomized per-channel gains and offsets.\n");
  printf ("  --dump-gain-offset       Prints gain, offset pairs to stdout.\n");
  printf ("\n");
}

#define MATCH_C_PREFIX(prefix,post)			\
  (strncmp(request,prefix,strlen(prefix)) == 0 &&       \
   *(post = request + strlen(prefix)) != '\0')
#define MATCH_C_ARG(name) (strcmp(request,name) == 0)

int main(int argc, char *argv[])
{
  int ldf = 0;
  int i;

  FILE *fid_out;
  int go_seed = 0;
  int dump_flag = 0;

  const char *in_name      = NULL;
  const char *in_tree_name = "h102";
  const char *out_name     = NULL;

  colourtext_init();

  for (i = 1; i < argc; i++)
    {
      char *request = argv[i];
      char *post;

      if (MATCH_C_ARG("--help")) {
        main_usage(argv[0]);
        exit(0);
      } else if (MATCH_C_ARG("--ldf")) {
	ldf = 1;
      } else if (MATCH_C_PREFIX("--go-seed=",post)) {
	go_seed = atoi(post);
      } else if (MATCH_C_PREFIX("--in-tree=",post)) {
	in_tree_name = post;
      } else if (MATCH_C_ARG("--dump-gain-offset")) {
	dump_flag = 1;
      } else {
	if (in_name == NULL)
	  in_name = strdup(request);
	else if (out_name == NULL)
	  out_name = strdup(request);
	else
	  BADCFG("Input and output file names already given.");
      }
    }

  if (in_name == NULL ||
      out_name == NULL)
    {
      main_usage(argv[0]);
      exit(1);
    }

  TFile *f = TFile::Open(in_name);
  TTree *tree;

  if (!f)
    {
      fprintf (stderr, "Could not open input '%s'.\n", in_name);
      exit(1);
    }

  f->GetObject(in_tree_name, tree);

  if (!tree)
    {
      fprintf (stderr, "Could not find object '%s'.\n", in_tree_name);
      exit(1);
    }

  root2ebye conv(tree);

  conv.LookForBranches(tree);
  conv.InitBranches();

  fid_out = fopen(out_name, "w");
  if (!fid_out)
    {
      fprintf (stderr,"Failure opening output '%s'.\n", out_name);
      exit(1);
    }

  // Set default calibration parameters.
  INIT_GAIN_OFFSET_2D(conv.go_CDu_e,    1000.0, 0, 0xffff, dump_flag);
  INIT_GAIN_OFFSET_2D(conv.go_CDv_e,    1000.0, 0, 0xffff, dump_flag);
  INIT_GAIN_OFFSET_1D(conv.go_ELUM_e,   1000.0, 0, 0xffff, dump_flag);
  INIT_GAIN_OFFSET_1D(conv.go_GCBA_e,   1000.0, 0, 0xffff, dump_flag);
  INIT_GAIN_OFFSET_2D(conv.go_HSIA_e,   1000.0, 0, 0xffff, dump_flag);
  INIT_GAIN_OFFSET_2D(conv.go_ISIA_p_e, 50.0, 250,  0xfff, dump_flag);
  INIT_GAIN_OFFSET_2D(conv.go_ISIA_n_e, 50.0, 250,  0xfff, dump_flag);
  INIT_GAIN_OFFSET_2D(conv.go_LUME_e,   1000.0, 0, 0xffff, dump_flag);
  INIT_GAIN_OFFSET_2D(conv.go_REC_e,    1000.0, 0, 0xffff, dump_flag);

  INIT_GAIN_OFFSET_2D(conv.go_IDS_Uu_e, 1000.0, 0, 0xffff, dump_flag);
  INIT_GAIN_OFFSET_2D(conv.go_IDS_Uv_e, 1000.0, 0, 0xffff, dump_flag);

  // Prepare output.
  if (!ldf)
    ebye_init_output(fileno(fid_out), 1 /* record headers */);
  else
    ldf_init_output(fileno(fid_out));

  // Process all events.
  conv.Loop();

  // Final flush.
  if (!ldf)
    ebye_flush_output();
  else
    ldf_flush_output();

  return 0;
}
